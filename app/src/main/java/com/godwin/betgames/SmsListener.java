package com.godwin.betgames;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import com.godwin.betgames.views.VerifyActivity;


public class SmsListener extends BroadcastReceiver {

    private String msgBody;
    private SharedPreferences preferences;

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub


        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {

            Toast.makeText(context, "message received", Toast.LENGTH_SHORT).show();

            Bundle bundle = intent.getExtras();
            try {
                String msg_from;
                if (bundle != null) {
                    final Object[] pdus = (Object[]) bundle.get("pdus");
                    for (int i = 0; i < pdus.length; i++) {
                        SmsMessage smsMessage;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                            smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i], bundle.getString("format"));
                        else smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i],bundle.getString("format"));

                        msg_from = smsMessage.getDisplayOriginatingAddress();
                        msgBody = smsMessage.getMessageBody();
                        VerifyActivity verify = new VerifyActivity();
                        verify.handleMessage(msgBody);
                    }
                    Toast.makeText(context, "message is: " + msgBody, Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Log.d("Exception caught", e.getMessage());
            }
        }
    }
}

