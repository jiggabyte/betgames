package com.godwin.betgames;

public class Utils {
	
	//Email Validation pattern
	public static final String regEx = "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b";

	//Fragments Tags
	public static final String Main_Fragment = "fragment_main";
	public static final String Home_Fragment = "fragment_home";
	public static final String Challenger_Fragment = "fragment_challenger";
	public static final String Wallet_Fragment = "fragment_wallet";
	public static final String Account_Fragment = "fragment_accounts";
	public static final String Faq_Fragment = "fragment_faq";
	public static final String Contact_Fragment = "fragment_contact";
	public static final String Bets_Fragment = "activity_bet";
	public static final String MyBets_Fragment = "fragment_mybet";
	public static final String In_Fragment = "fragment_pending";
	public static final String Sent_Fragment = "fragment_sent";
	public static final String Share_Fragment = "fragment_share";
	public static final String Bet_Fragment = "fragment_bet";
	public static final String Web_Fragment = "fragment_web";
	public static final String Web2_Fragment = "fragment_web";
	
}
