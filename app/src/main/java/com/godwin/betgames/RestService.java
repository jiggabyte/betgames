package com.godwin.betgames;

import com.godwin.betgames.models.Bet;
import com.godwin.betgames.models.LoadTrans;
import com.godwin.betgames.models.Loadbet;
import com.godwin.betgames.models.User;

import org.json.JSONArray;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;


public interface RestService {

    //Call for Login
    @FormUrlEncoded
    @POST("/api/login")
    Call<User> userLogIn(@Field("usernom") String usernom, @Field("password") String password);

    //Call for Logout
    @FormUrlEncoded
    @POST("/api/logout")
    Call<User> userLogout(@Header("Authorization") String BearerString, @Field("uuid") String uuid);

    //Call for Details
    @FormUrlEncoded
    @POST("/api/details")
    Call<User> userDetails(@Header("Authorization") String BearerString, @Field("uuid") String uuid);

    //Call for Forgot
    @FormUrlEncoded
    @POST("/api/forgot")
    Call<User> userForgot(@Header("Authorization") String BearerString, @Field("uuid") String uuid);

    //Call for Registration
    @FormUrlEncoded
    @POST("/api/register")
    Call<User> userSignUp(@Field("name") String name, @Field("email") String email, @Field("uuid") String uuid, @Field("password") String password, @Field("c_password") String c_password, @Field("phone") String phone);

    //Send Bet
    @FormUrlEncoded
    @POST("/api/bet")
    Call<Bet> userBet(@Header("Authorization") String BearerString, @Field("uuid") String uuid, @Field("amt") String amt, @Field("game") String game, @Field("state") String state, @Field("created_at") String created_at);

    //Send BetX
    @FormUrlEncoded
    @POST("/api/betx")
    Call<Bet> userBetX(@Header("Authorization") String BearerString, @Field("uuid") String uuid , @Field("target") String target , @Field("amt") String amt, @Field("game") String game, @Field("state") String state, @Field("created_at") String created_at);

    //Load Bets
    @FormUrlEncoded
    @POST("/api/loadbet")
    Call<Loadbet> userLoadBet(@Header("Authorization") String BearerString, @Field("uuid") String uuid, @Field("offset") Integer offset, @Field("limit") Integer limit);

    //Load BetXs
    @FormUrlEncoded
    @POST("/api/loadbetx")
    Call<Loadbet> userLoadBetX(@Header("Authorization") String BearerString, @Field("uuid") String uuid, @Field("offset") Integer offset, @Field("limit") Integer limit);

    //Load BetYs
    @FormUrlEncoded
    @POST("/api/loadbety")
    Call<Loadbet> userLoadBetY(@Header("Authorization") String BearerString, @Field("uuid") String uuid, @Field("offset") Integer offset, @Field("limit") Integer limit);

    //Search Bets
    @FormUrlEncoded
    @POST("/api/searchbet")
    Call<Loadbet> userSearchBet(@Header("Authorization") String BearerString, @Field("uuid") String uuid, @Field("offset") Integer offset, @Field("limit") Integer limit, @Field("amt") String query);

    //Search BetXs   TODO create http endpoint
    @FormUrlEncoded
    @POST("/api/searchbetx")
    Call<Loadbet> userSearchBetX(@Header("Authorization") String BearerString, @Field("uuid") String uuid, @Field("offset") Integer offset, @Field("limit") Integer limit, @Field("amt") String query);

    //Search BetYs   TODO create http endpoint
    @FormUrlEncoded
    @POST("/api/searchbety")
    Call<Loadbet> userSearchBetY(@Header("Authorization") String BearerString, @Field("uuid") String uuid, @Field("offset") Integer offset, @Field("limit") Integer limit, @Field("amt") String query);

    //Search User
    @FormUrlEncoded
    @POST("/api/searchuser")
    Call<User> userSearch(@Header("Authorization") String BearerString, @Field("usernom") String usernom, @Field("uuid") String query);


    //Verify User
    @FormUrlEncoded
    @POST("/api/verifier")
    Call<User> userVerify(@Field("code") String code, @Field("uuid") String uuid);

    //Change User Password
    @FormUrlEncoded
    @POST("/api/change-password")
    Call<User> userChange(@Field("uuid") String uuid, @Field("passer") String passer, @Field("firmer") String firmer);

    //Resend Code
    @FormUrlEncoded
    @POST("/api/resend")
    Call<User> userResend(@Field("uuid") String uuid);

    //Register Device Token
    @FormUrlEncoded
    @POST("/api/device-token")
    Call<User> userSendToken(@Header("Authorization") String BearerString, @Field("uuid") String uuid, @Field("token") String token, @Field("devicer") String devicer);

    //Accept Bet Challenge
    @FormUrlEncoded
    @POST("/api/acceptbet")
    Call<User> userAccept(@Header("Authorization") String BearerString, @Field("uuid") String usernome, @Field("user_beta") String userbeta, @Field("bet_id") String betid);

    //Send Start Request
    @FormUrlEncoded
    @POST("/api/startgame")
    Call<User> userStart(@Header("Authorization") String BearerString, @Field("user_beta") String userBeta, @Field("user_acc") String userAcc, @Field("bet_id") String betId);

    //Send StartX Request
    @FormUrlEncoded
    @POST("/api/startgamex")
    Call<User> userStartX(@Header("Authorization") String BearerString, @Field("user_beta") String userBeta, @Field("user_acc") String userAcc, @Field("bet_id") String betId);

    //Send Cancel Request
    @FormUrlEncoded
    @POST("/api/cancelgame")
    Call<User> userCancel(@Header("Authorization") String BearerString, @Field("user_beta") String userBeta, @Field("user_acc") String userAcc, @Field("bet_id") String betId);

    //Send Replay Request
    @FormUrlEncoded
    @POST("/api/replaygame")
    Call<User> userReplay(@Header("Authorization") String BearerString, @Field("user_challer") String userBeta, @Field("user_nome") String userAcc, @Field("bet_game") String betGame, @Field("bet_id") String bet_id, @Field("game_id") String gameId);

    //Send Stop Request
    @FormUrlEncoded
    @POST("/api/stopgame")
    Call<User> userStop(@Header("Authorization") String BearerString, @Field("user_own") String userOwn, @Field("user_beta") String userBeta, @Field("user_acc") String userAcc, @Field("bet_id") String betId);

    //Remove Stop Request
    @FormUrlEncoded
    @POST("/api/removestop")
    Call<User> userRemStop(@Header("Authorization") String BearerString, @Field("user_own") String userOwn);


    //Send Confirm Request
    @FormUrlEncoded
    @POST("/api/confirm")
    Call<User> userConfirm(@Header("Authorization") String BearerString, @Field("user_beta") String userBeta, @Field("user_acc") String userAcc, @Field("bet_id") String betId);

    //Send Game Request
    @FormUrlEncoded
    @POST("/api/gameon")
    Call<User> userOn(@Header("Authorization") String BearerString,@Field("user_player") String userPlayer, @Field("user_beta") String userBeta, @Field("user_acc") String userAcc, @Field("bet_id") String betId);

    //Send Game Play
    @FormUrlEncoded
    @POST("/api/gameplay")
    Call<User> userPlay(@Header("Authorization") String BearerString, @Field("user_player") String userPlayer, @Field("player_one") String player_one, @Field("player_two") String player_two, @Field("card_string") String cardString, @Field("play_time") String playTime, @Field("game_id") String gameId);

    //Receive Game Play
    @FormUrlEncoded
    @POST("/api/gamerec")
    Call<User> userRec(@Header("Authorization") String BearerString, @Field("user_player") String userPlayer, @Field("game_id") String gameId);

    //Send Whot Play
    @FormUrlEncoded
    @POST("/api/whotter")
    Call<User> userWhot(@Header("Authorization") String BearerString, @Field("user_player") String userPlayer, @Field("player_one") String player_one, @Field("player_two") String player_two, @Field("whot_string") String whotString, @Field("game_id") String gameId, @Field("timer") String timer);

    //Send Market Play
    @FormUrlEncoded
    @POST("/api/marketer")
    Call<User> userMark(@Header("Authorization") String BearerString, @Field("user_player") String userPlayer, @Field("player_one") String player_one, @Field("player_two") String player_two, @Field("market_string") String marketString, @Field("game_id") String gameId, @Field("timer") String timer);

    //Check Game Over
    @FormUrlEncoded
    @POST("/api/gameover")
    Call<User> userOver(@Header("Authorization") String BearerString, @Field("user_player") String userPlayer, @Field("player_one") String player_one, @Field("player_two") String player_two, @Field("game_id") String gameId);

    //Check Game Over
    @FormUrlEncoded
    @POST("/api/gameoverx")
    Call<User> userOverX(@Header("Authorization") String BearerString, @Field("user_player") String userPlayer, @Field("player_one") String player_one, @Field("player_two") String player_two, @Field("game_id") String gameId);

    //Check Time Over
    @FormUrlEncoded
    @POST("/api/timer")
    Call<User> userTimer(@Header("Authorization") String BearerString, @Field("user_player") String userPlayer, @Field("player_one") String player_one, @Field("player_two") String player_two, @Field("game_id") String gameId);


    //Check Finish Over
    @FormUrlEncoded
    @POST("/api/finish")
    Call<User> userFinish(@Header("Authorization") String BearerString, @Field("user_player") String userPlayer, @Field("player_one") String player_one, @Field("player_two") String player_two, @Field("game_id") String gameId);


    //Check Reload Market
    @FormUrlEncoded
    @POST("/api/marketdeck")
    Call<User> userReload(@Header("Authorization") String BearerString, @Field("user_player") String userPlayer, @Field("player_one") String player_one, @Field("player_two") String player_two, @Field("game_id") String gameId);


    //Stop Game onBackPressed
    @FormUrlEncoded
    @POST("/api/onback")
    Call<User> userOnback(@Header("Authorization") String BearerString, @Field("user_player") String userPlayer, @Field("player_one") String player_one, @Field("player_two") String player_two, @Field("game_id") String gameId);

    //Load Balance
    @FormUrlEncoded
    @POST("/api/credit-balance")
    Call<User> userBalance(@Header("Authorization") String BearerString, @Field("uuid") String uuid);

    //Set Online User
    @FormUrlEncoded
    @POST("/api/setonline")
    Call<User> userSetOnline(@Header("Authorization") String BearerString, @Field("uuid") String uuid);


    //Set Offline User
    @FormUrlEncoded
    @POST("/api/setoffline")
    Call<User> userSetOffline(@Header("Authorization") String BearerString, @Field("uuid") String uuid);


    //Transactions
    @FormUrlEncoded
    @POST("/api/transaction")
    Call<LoadTrans> userTrans(@Header("Authorization") String BearerString, @Field("uuid") String uuid);

    //Send mail
    @FormUrlEncoded
    @POST("/api/send-mail")
    Call<User> userMailer(@Header("Authorization") String BearerString, @Field("name") String name, @Field("mail") String mail,  @Field("msg") String msg,  @Field("fone") String fone);


    //Transactions
    @FormUrlEncoded
    @POST("/api/referer")
    Call<User> userRef(@Header("Authorization") String BearerString, @Field("reffer") String reffer, @Field("reffee") String reffee);


    //Send Continue Play
    @FormUrlEncoded
    @POST("/api/continua")
    Call<User> userCont(@Header("Authorization") String BearerString, @Field("user_player") String userPlayer, @Field("player_one") String player_one, @Field("player_two") String player_two, @Field("game_id") String gameId);

    //Check Delay
    @FormUrlEncoded
    @POST("/api/check-delay")
    Call<User> userDelay(@Header("Authorization") String BearerString, @Field("user_player") String userPlayer, @Field("player_one") String player_one, @Field("player_two") String player_two, @Field("game_id") String gameId);



}