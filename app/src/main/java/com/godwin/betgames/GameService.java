package com.godwin.betgames;

import android.app.IntentService;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.godwin.betgames.models.User;

import java.util.concurrent.TimeUnit;

import androidx.annotation.Nullable;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GameService extends IntentService {
    Session session;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public GameService() {
        super("GameService");
    }


    @Override
    public void onCreate() {
        session = new Session(GameService.this);
        //Toast.makeText(this, "Service created!", Toast.LENGTH_LONG).show();

        PeriodicWorkRequest OnlineWorkRequest =
                new PeriodicWorkRequest.Builder(OnlineWorker.class, 1, TimeUnit.MINUTES)
                        .build();

        WorkManager
                .getInstance(GameService.this)
                .enqueue(OnlineWorkRequest);

        /**
         * RestService service = RestClient.getClient(session).create(RestService.class);
         *         retrofit2.Call<User> call = service.userSetOffline("Bearer " + session.getUserToken(), session.getUserID());
         *         call.enqueue(new Callback<User>() {
         *             @Override
         *             public void onResponse(retrofit2.Call<User> call, Response<User> response) {
         *
         *                 //Toast.makeText()
         *                 if (response.isSuccessful()) {
         *                     if (response.body() != null) {
         *
         *                     }
         *                 } else {
         *
         *                 }
         *             }
         *
         *             @Override
         *             public void onFailure(Call<User> call, Throwable t) {
         *
         *             }
         *         });
         */

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * This method is invoked on the worker thread with a request to process.
     * Only one Intent is processed at a time, but the processing happens on a
     * worker thread that runs independently from other application logic.
     * So, if this code takes a long time, it will hold up other requests to
     * the same IntentService, but it will not hold up anything else.
     * When all requests have been handled, the IntentService stops itself,
     * so you should not call {@link #stopSelf}.
     *
     * @param intent The value passed to {@link
     *               Context#startService(Intent)}.
     *               This may be null if the service is being restarted after
     *               its process has gone away; see
     *               {@link Service#onStartCommand}
     *               for details.
     */
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

    }

    @Override
    public ComponentName startService(Intent service) {
        return super.startService(service);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // super.onStartCommand(intent, flags, startId);

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
       // Toast.makeText(this, "Service stopped", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStart(Intent intent, int startid) {
     //   Toast.makeText(this, "Service started by user.", Toast.LENGTH_LONG).show();
    }

}
