package com.godwin.betgames.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Transactioner implements Serializable {

    @SerializedName("transaction") @Expose private ArrayList<Transaction> transaction;
    @SerializedName("count") @Expose private Integer counta;
    private final static long serialVersionUID = 48921359854632708L;


    public ArrayList<Transaction> getTransaction() {
        return transaction;
    }

    public void setTransaction(ArrayList<Transaction> transaction) {
        this.transaction = transaction;
    }

    public Integer getCounta() {
        return counta;
    }

    public void setCounta(Integer counta) {
        this.counta = counta;
    }


}