package com.godwin.betgames.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Successir implements Serializable {

    @SerializedName("winner") @Expose private String winner;
    @SerializedName("loser") @Expose private String loser;
    private final static long serialVersionUID = 489213543348202708L;


    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public String getLoser() {
        return loser;
    }

    public void setLoser(String loser) {
        this.loser = loser;
    }

}