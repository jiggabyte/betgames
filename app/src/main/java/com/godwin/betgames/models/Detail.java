package com.godwin.betgames.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Detail implements Serializable
{

    @SerializedName("id") @Expose private String id;
    @SerializedName("role_id") @Expose private String roleId;
    @SerializedName("name") @Expose private String name;
    @SerializedName("email") @Expose private String email;
    @SerializedName("uuid") @Expose private String uuid;
    @SerializedName("phone") @Expose private String phone;
    @SerializedName("avatar") @Expose private String avatar;
    @SerializedName("sms_code") @Expose private String smsCode;
    @SerializedName("email_verified_at") @Expose private String emailVerifiedAt;
    @SerializedName("settings") @Expose private Settings settings;
    @SerializedName("created_at") @Expose private String createdAt;
    @SerializedName("updated_at") @Expose private String updatedAt;
    @SerializedName("nota") @Expose private String nota;
    @SerializedName("count") @Expose private String count;
    private final static long serialVersionUID = 7943761571280001637L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getSmsCode() {
        return smsCode;
    }

    public void setSmsCode(String smsCode) {
        this.smsCode = smsCode;
    }

    public String getEmailVerifiedAt() {
        return emailVerifiedAt;
    }

    public void setEmailVerifiedAt(String emailVerifiedAt) {
        this.emailVerifiedAt = emailVerifiedAt;
    }

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("roleId", roleId).append("name", name).append("email", email).append("uuid", uuid).append("phone", phone).append("avatar", avatar).append("smsCode", smsCode).append("emailVerifiedAt", emailVerifiedAt).append("settings", settings).append("createdAt", createdAt).append("updatedAt", updatedAt).toString();
    }

}
