package com.godwin.betgames.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.ArrayList;

public class Success implements Serializable {

    @SerializedName("token") @Expose private String token;
    @SerializedName("uuid") @Expose private String uuid;
    @SerializedName("nota") @Expose private String nota;
    @SerializedName("note") @Expose private String note;
    @SerializedName("noti") @Expose private String noti;
    @SerializedName("noto") @Expose private String noto;
    @SerializedName("notu") @Expose private String notu;
    @SerializedName("noty") @Expose private String noty;
    @SerializedName("noth") @Expose private String noth;
    @SerializedName("notx") @Expose private int notx;
    @SerializedName("continua") @Expose private String continua;
    @SerializedName("winner") @Expose private String winner;
    @SerializedName("loser") @Expose private String loser;
    @SerializedName("updated_at") @Expose private String updated_at;
    @SerializedName("game_type") @Expose private String game_type;
    @SerializedName("updated") @Expose private String updated;
    @SerializedName("firebase") @Expose private String firebase;
    @SerializedName("credit") @Expose private String credit;
    @SerializedName("balance") @Expose private String balance;
    @SerializedName("error") @Expose private String error;
    @SerializedName("timer") @Expose private String timer;
    @SerializedName("wina") @Expose private String wina;
    @SerializedName("losa") @Expose private String losa;
    @SerializedName("decker") @Expose private ArrayList<String> decker;



    private final static long serialVersionUID = 6278062631837517413L;


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public String getNoti() {
        return noti;
    }

    public void setNoti(String noti) {
        this.noti = noti;
    }

    public String getNoto() {
        return noto;
    }

    public void setNoto(String noto) {
        this.noto = noto;
    }

    public String getNotu() {
        return notu;
    }

    public void setNotu(String notu) {
        this.notu = notu;
    }

    public String getNoty() {
        return noty;
    }

    public void setNoty(String noty) {
        this.noty = noty;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public String getLoser() {
        return loser;
    }

    public void setLoser(String loser) {
        this.loser = loser;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updatedAt) {
        this.updated_at = updated_at;
    }

    public String getGame_type() {
        return game_type;
    }

    public void setGame_type(String game_type) {
        this.game_type = game_type;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getFirebase() {
        return firebase;
    }

    public void setFirebase(String firebase) {
        this.firebase = firebase;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getNoth() {
        return noth;
    }

    public void setNoth(String noth) {
        this.noth = noth;
    }

    public int getNotx() {
        return notx;
    }

    public void setNotx(int notx) {
        this.notx = notx;
    }

    public String getContinua() {
        return continua;
    }

    public void setContinua(String continua) {
        this.continua = continua;
    }

    public String getTimer() {
        return timer;
    }

    public void setTimer(String timer) {
        this.timer = timer;
    }

    public String getWina() {
        return wina;
    }

    public void setWina(String wina) {
        this.wina = wina;
    }

    public String getLosa() {
        return losa;
    }

    public void setLosa(String losa) {
        this.losa = losa;
    }

    public ArrayList<String> getDecker() {
        return decker;
    }

    public void setDecker(ArrayList<String> decker) {
        this.decker = decker;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this).append("token", token).append("uuid", uuid).append("nota", nota).append("note", note).append("noti", noti).append("noto", noto).append("notu", notu).append("noty", noty).append("winner", winner).append("loser", loser).append("updated_at", updated_at).toString();
    }


}