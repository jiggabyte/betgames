package com.godwin.betgames.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Successer implements Serializable {

    @SerializedName("nota") @Expose private ArrayList<SuccessLoad> nota;
    @SerializedName("count") @Expose private Integer counta;
    private final static long serialVersionUID = 489213543348202708L;


    public ArrayList<SuccessLoad> getNota() {
        return nota;
    }

    public void setNota(ArrayList<SuccessLoad> nota) {
        this.nota = nota;
    }

    public Integer getCounta() {
        return counta;
    }

    public void setCounta(Integer counta) {
        this.counta = counta;
    }

}