package com.godwin.betgames.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable {
    @SerializedName("success") @Expose private Success success;
    @SerializedName("error") @Expose private String error;
    @SerializedName("detail") @Expose private Detail detail;
    @SerializedName("successor") @Expose private String successor;
    @SerializedName("gamer") @Expose private Gamer gamer;
    @SerializedName("decker") @Expose private ArrayList<String> decker;
    @SerializedName("deckobj") @Expose private JSONObject deckObj;
    @SerializedName("successir") @Expose private Successir successir;
    @SerializedName("transaction") @Expose private Transaction transaction;
    @SerializedName("errorList") @Expose private ErrorList errorList;

    private final static long serialVersionUID = 1500583362333088752L;


    public User (Detail detail, Success success, String error, String successor, Gamer gamer, ArrayList<String> decker, JSONObject deckObj, Successir successir, Transaction transaction) {
        this.detail = detail;
        this.success = success;
        this.error = error;
        this.successor = successor;
        this.gamer = gamer;
        this.decker = decker;
        this.deckObj = deckObj;
        this.successir = successir;
        this.transaction = transaction;
    }

    public ArrayList<String> getDecker() {
        return decker;
    }

    public void setDecker(ArrayList<String> decker) {
        this.decker = decker;
    }

    public JSONObject getDeckObj() {
        return deckObj;
    }

    public void setDeckObj(JSONObject deckObj) {
        this.deckObj = deckObj;
    }

    public Success getSuccess() {
        return success;
    }

    public void setSuccess(Success success) {
        this.success = success;
    }

    public String getSuccessor() {
        return successor;
    }

    public void setSuccessor(String successor) {
        this.successor = successor;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Detail getDetail() {
        return detail;
    }

    public void setDetail(Detail detail) {
        this.detail = detail;
    }

    public Gamer getGamer() {
        return gamer;
    }

    public void setGamer(Gamer gamer) {
        this.gamer = gamer;
    }

    public ErrorList getErrorList() {
        return errorList;
    }

    public void setErrorList( ErrorList errorList) {
        this.errorList = errorList;
    }

    public Successir getSuccessir() {
        return successir;
    }

    public void setSuccessir(Successir successir) {
        this.successir = successir;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
}