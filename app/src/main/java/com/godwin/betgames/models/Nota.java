package com.godwin.betgames.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.ArrayList;

import androidx.annotation.NonNull;

public class Nota implements Serializable {

    @SerializedName("backer") @Expose private String backer;
    @SerializedName("user_beta") @Expose private String user_beta;
    @SerializedName("user_acc") @Expose private String user_acc;
    @SerializedName("bet_id") @Expose private String bet_id;
    @SerializedName("nota") @Expose private String nota;
    @SerializedName("id") @Expose private int id;
    @SerializedName("player1") @Expose private String player1;
    @SerializedName("player2") @Expose private String player2;
    @SerializedName("game_type") @Expose private String game_type;
    @SerializedName("winner") @Expose private String winner;
    @SerializedName("loser") @Expose private String loser;
    @SerializedName("market_deck") @Expose private String market_deck;
    @SerializedName("market_size") @Expose private String market_size;
    @SerializedName("play_deck") @Expose private String play_deck;
    @SerializedName("player1_deck") @Expose private String player1_deck;
    @SerializedName("player2_deck") @Expose private String player2_deck;
    @SerializedName("player1_moves") @Expose private String player1_moves;
    @SerializedName("player2_moves") @Expose private String player2_moves;
    @SerializedName("turn") @Expose private String turn;
    @SerializedName("pend") @Expose private String pend;
    @SerializedName("timer") @Expose private String timer;
    @SerializedName("error") @Expose private String error;
    @SerializedName("created_at") @Expose private String created_at;
    @SerializedName("updated_at") @Expose private String updated_at;
    @SerializedName("gamer") @Expose private String gamer;
    @SerializedName("bet_amt") @Expose private String bet_amt;
    @SerializedName("bet_game") @Expose private String bet_game;

    private final static long serialVersionUID = 6278062632134567413L;


    public String getBacker() {
        return backer;
    }

    public void setBacker(String backer) {
        this.backer = backer;
    }

    public String getUser_beta() {
        return user_beta;
    }

    public void setUser_beta(String user_beta) {
        this.user_beta = user_beta;
    }

    public String getUser_acc() {
        return user_acc;
    }

    public void setUser_acc(String user_acc) {
        this.user_acc = user_acc;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public String getBet_id() {
        return bet_id;
    }

    public void setBet_id(String bet_id) {
        this.bet_id = bet_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlayer1() {
        return player1;
    }

    public void setPlayer1(String player1) {
        this.player1 = player1;
    }

    public String getPlayer2() {
        return player2;
    }

    public void setPlayer2(String player2) {
        this.player2 = player2;
    }

    public String getGame_type() {
        return game_type;
    }

    public void setGame_type(String game_type) {
        this.game_type = game_type;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public String getLoser() {
        return loser;
    }

    public void setLoser(String loser) {
        this.loser = loser;
    }

    public String getMarket_size() {
        return market_size;
    }

    public void setMarket_size(String market_size) {
        this.market_size = market_size;
    }

    public String getMarket_deck() {
        return market_deck;
    }

    public void setMarket_deck(String market_deck) {
        this.market_deck = market_deck;
    }

    public String getPlay_deck() {
        return play_deck;
    }

    public void setPlay_deck(String play_deck) {
        this.play_deck = play_deck;
    }

    public String getPlayer1_deck() {
        return player1_deck;
    }

    public void setPlayer1_deck(String player1_deck) {
        this.player1_deck = player1_deck;
    }

    public String getPlayer2_deck() {
        return player2_deck;
    }

    public void setPlayer2_deck(String player2_deck) {
        this.player2_deck = player2_deck;
    }

    public String getPlayer1_moves() {
        return player1_moves;
    }

    public void setPlayer1_moves(String player1_moves) {
        this.player1_moves = player1_moves;
    }

    public String getPlayer2_moves() {
        return player2_moves;
    }

    public void setPlayer2_moves(String player2_moves) {
        this.player2_moves = player2_moves;
    }

    public String getPend() {
        return pend;
    }

    public void setPend(String pend) {
        this.pend = pend;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTurn() {
        return turn;
    }

    public void setTurn(String turn) {
        this.turn = turn;
    }

    public String getTimer() {
        return timer;
    }

    public void setTimer(String timer) {
        this.timer = timer;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getGamer() {
        return gamer;
    }

    public void setGamer(String gamer) {
        this.gamer = gamer;
    }

    public String getBet_amt() {
        return bet_amt;
    }

    public void setBet_amt(String bet_amt) {
        this.bet_amt = bet_amt;
    }

    public String getBet_game() {
        return bet_game;
    }

    public void setBet_game(String bet_game) {
        this.bet_game = bet_game;
    }


    /**
     * Returns a string representation of the object. In general, the
     * {@code toString} method returns a string that
     * "textually represents" this object. The result should
     * be a concise but informative representation that is easy for a
     * person to read.
     * It is recommended that all subclasses override this method.
     * <p>
     * The {@code toString} method for class {@code Object}
     * returns a string consisting of the name of the class of which the
     * object is an instance, the at-sign character `{@code @}', and
     * the unsigned hexadecimal representation of the hash code of the
     * object. In other words, this method returns a string equal to the
     * value of:
     * <blockquote>
     * <pre>
     * getClass().getName() + '@' + Integer.toHexString(hashCode())
     * </pre></blockquote>
     *
     * @return a string representation of the object.
     */
    @NonNull
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("backer", backer).append("user_beta", user_beta).append("user_acc", user_acc).append("bet_id", bet_id).append("nota", nota).append("id", id).append("player1", player1).append("player2", player2).append("game_type", game_type).append("winner", winner).append("loser", loser).append("market_deck", market_deck).append("play_deck", play_deck).append("player1_deck", player1_deck).append("player2_deck", player2_deck).append("player1_moves", player1_moves).append("player2_moves", player2_moves).append("turn", turn).append("timer", timer).append("created_at", created_at).append("updated_at", updated_at).toString();
    }

}