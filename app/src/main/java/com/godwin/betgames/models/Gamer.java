package com.godwin.betgames.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Gamer implements Serializable
{

    @SerializedName("id") @Expose private String id;
    @SerializedName("player1") @Expose private String player1;
    @SerializedName("player2") @Expose private String player2;
    @SerializedName("game_type") @Expose private String game_type;
    @SerializedName("winner") @Expose private String winner;
    @SerializedName("loser") @Expose private String loser;
    @SerializedName("market_deck") @Expose private String market_deck;
    @SerializedName("market_size") @Expose private String market_size;
    @SerializedName("play_deck") @Expose private String play_deck;
    @SerializedName("player1_deck") @Expose private String player1_deck;
    @SerializedName("player2_deck") @Expose private String player2_deck;
    @SerializedName("player1_moves") @Expose private String player1_moves;
    @SerializedName("player2_moves") @Expose private String player2_moves;
    @SerializedName("turn") @Expose private String turn;
    @SerializedName("pend") @Expose private String pend;
    @SerializedName("timer") @Expose private String timer;
    @SerializedName("error") @Expose private String error;
    @SerializedName("created_at") @Expose private String createdAt;
    @SerializedName("updated_at") @Expose private String updatedAt;

    private final static long serialVersionUID = 7943761571280001637L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlayer1() {
        return player1;
    }

    public void setPlayer1(String player1) {
        this.player1 = player1;
    }

    public String getPlayer2() {
        return player2;
    }

    public void setPlayer2(String player2) {
        this.player2 = player2;
    }

    public String getGame_type() {
        return game_type;
    }

    public void setGame_type(String game_type) {
        this.game_type = game_type;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public String getLoser() {
        return loser;
    }

    public void setLoser(String loser) {
        this.loser = loser;
    }

    public String getMarket_size() {
        return market_size;
    }

    public void setMarket_size(String market_size) {
        this.market_size = market_size;
    }

    public String getMarket_deck() {
        return market_deck;
    }

    public void setMarket_deck(String market_deck) {
        this.market_deck = market_deck;
    }

    public String getPlay_deck() {
        return play_deck;
    }

    public void setPlay_deck(String play_deck) {
        this.play_deck = play_deck;
    }

    public String getPlayer1_deck() {
        return player1_deck;
    }

    public void setPlayer1_deck(String player1_deck) {
        this.player1_deck = player1_deck;
    }

    public String getPlayer2_deck() {
        return player2_deck;
    }

    public void setPlayer2_deck(String player2_deck) {
        this.player2_deck = player2_deck;
    }

    public String getPlayer1_moves() {
        return player1_moves;
    }

    public void setPlayer1_moves(String player1_moves) {
        this.player1_moves = player1_moves;
    }

    public String getPlayer2_moves() {
        return player2_moves;
    }

    public void setPlayer2_moves(String player2_moves) {
        this.player2_moves = player2_moves;
    }

    public String getPend() {
        return pend;
    }

    public void setPend(String pend) {
        this.pend = pend;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTurn() {
        return turn;
    }

    public void setTurn(String turn) {
        this.turn = turn;
    }

    public String getTimer() {
        return timer;
    }

    public void setTimer(String timer) {
        this.timer = timer;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("player1", player1).append("player2", player2).append("game_type", game_type).append("winner", winner).append("loser", loser).append("market_deck", market_deck).append("play_deck", play_deck).append("player1_deck", player1_deck).append("player2_deck", player2_deck).append("player1_moves", player1_moves).append("player2_moves", player2_moves).append("turn", turn).append("timer", timer).append("createdAt", createdAt).append("updatedAt", updatedAt).toString();
    }

}
