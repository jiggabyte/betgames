package com.godwin.betgames.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Decker implements Serializable {

    @SerializedName("0") @Expose private String x0;
    @SerializedName("1") @Expose private String x1;
    @SerializedName("2") @Expose private String x2;
    @SerializedName("3") @Expose private String x3;
    @SerializedName("4") @Expose private String x4;
    @SerializedName("5") @Expose private String x5;
    @SerializedName("6") @Expose private String x6;
    @SerializedName("7") @Expose private String x7;
    @SerializedName("8") @Expose private String x8;
    @SerializedName("9") @Expose private String x9;
    @SerializedName("10") @Expose private String x10;
    @SerializedName("11") @Expose private String x11;
    @SerializedName("12") @Expose private String x12;
    @SerializedName("13") @Expose private String x13;
    @SerializedName("14") @Expose private String x14;
    @SerializedName("15") @Expose private String x15;
    @SerializedName("16") @Expose private String x16;
    @SerializedName("17") @Expose private String x17;
    @SerializedName("18") @Expose private String x18;
    @SerializedName("19") @Expose private String x19;
    @SerializedName("20") @Expose private String x20;
    @SerializedName("21") @Expose private String x21;
    @SerializedName("22") @Expose private String x22;
    @SerializedName("23") @Expose private String x23;
    @SerializedName("24") @Expose private String x24;
    @SerializedName("25") @Expose private String x25;
    @SerializedName("26") @Expose private String x26;
    @SerializedName("27") @Expose private String x27;
    @SerializedName("28") @Expose private String x28;
    @SerializedName("29") @Expose private String x29;
    @SerializedName("30") @Expose private String x30;
    @SerializedName("31") @Expose private String x31;
    @SerializedName("32") @Expose private String x32;
    @SerializedName("33") @Expose private String x33;
    @SerializedName("34") @Expose private String x34;
    @SerializedName("35") @Expose private String x35;
    @SerializedName("36") @Expose private String x36;
    @SerializedName("37") @Expose private String x37;
    @SerializedName("38") @Expose private String x38;
    @SerializedName("39") @Expose private String x39;
    @SerializedName("40") @Expose private String x40;
    @SerializedName("41") @Expose private String x41;
    @SerializedName("42") @Expose private String x42;
    @SerializedName("43") @Expose private String x43;
    @SerializedName("44") @Expose private String x44;
    @SerializedName("45") @Expose private String x45;
    @SerializedName("46") @Expose private String x46;
    @SerializedName("47") @Expose private String x47;
    @SerializedName("48") @Expose private String x48;
    @SerializedName("49") @Expose private String x49;
    @SerializedName("50") @Expose private String x50;
    @SerializedName("51") @Expose private String x51;
    @SerializedName("52") @Expose private String x52;
    @SerializedName("53") @Expose private String x53;
    @SerializedName("54") @Expose private String x54;
    @SerializedName("55") @Expose private String x55;
    @SerializedName("56") @Expose private String x56;
    @SerializedName("57") @Expose private String x57;
    @SerializedName("58") @Expose private String x58;
    @SerializedName("59") @Expose private String x59;
    @SerializedName("60") @Expose private String x60;



    private final static long serialVersionUID = 6278062631831517413L;


    public String getX0() {
        return x0;
    }

    public void setX0(String x0) {
        this.x0 = x0;
    }


    public String getX1() {
        return x1;
    }

    public void setX1(String x1) {
        this.x1 = x1;
    }

    public String getX2() {
        return x2;
    }

    public void setX2(String x2) {
        this.x2 = x2;
    }

    public String getX3() {
        return x3;
    }

    public void setX3(String x3) {
        this.x3 = x3;
    }

    public String getX4() {
        return x4;
    }

    public void setX4(String x4) {
        this.x4 = x4;
    }

    public String getX5() {
        return x5;
    }

    public void setX5(String x5) {
        this.x5 = x5;
    }

    public String getX6() {
        return x6;
    }

    public void setX6(String x6) {
        this.x6 = x6;
    }

    public String getX7() {
        return x7;
    }

    public void setX7(String x7) {
        this.x7 = x7;
    }

    public String getX8() {
        return x8;
    }

    public void setX8(String x8) {
        this.x8 = x8;
    }

    public String getX9() {
        return x9;
    }

    public void setX9(String x9) {
        this.x9 = x9;
    }

    public String getX10() {
        return x10;
    }

    public void setX10(String x10) {
        this.x10 = x10;
    }

    public String getX11() {
        return x11;
    }

    public void setX11(String x11) {
        this.x11 = x11;
    }

    public String getX12() {
        return x12;
    }

    public void setX12(String x12) {
        this.x12 = x12;
    }

    public String getX13() {
        return x13;
    }

    public void setX13(String x13) {
        this.x13 = x13;
    }

    public String getX14() {
        return x14;
    }

    public void setX14(String x14) {
        this.x14 = x14;
    }

    public String getX15() {
        return x15;
    }

    public void setX15(String x15) {
        this.x15 = x15;
    }

    public String getX16() {
        return x16;
    }

    public void setX16(String x16) {
        this.x16 = x16;
    }

    public String getX17() {
        return x17;
    }

    public void setX17(String x17) {
        this.x17 = x17;
    }

    public String getX18() {
        return x18;
    }

    public void setX18(String x18) {
        this.x18 = x18;
    }

    public String getX19() {
        return x19;
    }

    public void setX19(String x19) {
        this.x19 = x19;
    }

    public String getX20() {
        return x20;
    }

    public void setX20(String x20) {
        this.x20 = x20;
    }

    public String getX21() {
        return x21;
    }

    public void setX21(String x21) {
        this.x21 = x21;
    }

    public String getX22() {
        return x22;
    }

    public void setX22(String x22) {
        this.x22 = x22;
    }

    public String getX23() {
        return x23;
    }

    public void setX23(String x23) {
        this.x23 = x23;
    }

    public String getX24() {
        return x24;
    }

    public void setX24(String x24) {
        this.x24 = x24;
    }

    public String getX25() {
        return x25;
    }

    public void setX25(String x25) {
        this.x25 = x25;
    }

    public String getX26() {
        return x26;
    }

    public void setX26(String x26) {
        this.x26 = x26;
    }

    public String getX27() {
        return x27;
    }

    public void setX27(String x27) {
        this.x27 = x27;
    }

    public String getX28() {
        return x28;
    }

    public void setX28(String x28) {
        this.x28 = x28;
    }

    public String getX29() {
        return x29;
    }

    public void setX29(String x29) {
        this.x29 = x29;
    }

    public String getX30() {
        return x30;
    }

    public void setX30(String x30) {
        this.x30 = x30;
    }

    public String getX31() {
        return x31;
    }

    public void setX31(String x31) {
        this.x31 = x31;
    }

    public String getX32() {
        return x32;
    }

    public void setX32(String x32) {
        this.x32 = x32;
    }

    public String getX33() {
        return x33;
    }

    public void setX33(String x33) {
        this.x33 = x33;
    }

    public String getX34() {
        return x34;
    }

    public void setX34(String x34) {
        this.x34 = x34;
    }

    public String getX35() {
        return x35;
    }

    public void setX35(String x35) {
        this.x35 = x35;
    }

    public String getX36() {
        return x36;
    }

    public void setX36(String x36) {
        this.x36 = x36;
    }

    public String getX37() {
        return x37;
    }

    public void setX37(String x37) {
        this.x37 = x37;
    }

    public String getX38() {
        return x38;
    }

    public void setX38(String x38) {
        this.x38 = x38;
    }

    public String getX39() {
        return x39;
    }

    public void setX39(String x39) {
        this.x39 = x39;
    }

    public String getX40() {
        return x40;
    }

    public void setX40(String x40) {
        this.x40 = x40;
    }

    public String getX41() {
        return x41;
    }

    public void setX41(String x41) {
        this.x41 = x41;
    }

    public String getX42() {
        return x42;
    }

    public void setX42(String x42) {
        this.x42 = x42;
    }

    public String getX43() {
        return x43;
    }

    public void setX43(String x43) {
        this.x43 = x43;
    }

    public String getX44() {
        return x44;
    }

    public void setX44(String x44) {
        this.x44 = x44;
    }

    public String getX45() {
        return x45;
    }

    public void setX45(String x45) {
        this.x45 = x45;
    }

    public String getX46() {
        return x46;
    }

    public void setX46(String x46) {
        this.x46 = x46;
    }

    public String getX47() {
        return x47;
    }

    public void setX47(String x47) {
        this.x47 = x47;
    }

    public String getX48() {
        return x48;
    }

    public void setX48(String x48) {
        this.x48 = x48;
    }

    public String getX49() {
        return x49;
    }

    public void setX49(String x49) {
        this.x49 = x49;
    }

    public String getX50() {
        return x50;
    }

    public void setX50(String x50) {
        this.x50 = x50;
    }

    public String getX51() {
        return x51;
    }

    public void setX51(String x51) {
        this.x51 = x51;
    }

    public String getX52() {
        return x52;
    }

    public void setX52(String x52) {
        this.x52 = x52;
    }

    public String getX53() {
        return x53;
    }

    public void setX53(String x53) {
        this.x53 = x53;
    }

    public String getX54() {
        return x54;
    }

    public void setX54(String x54) {
        this.x54 = x54;
    }

    public String getX55() {
        return x55;
    }

    public void setX55(String x55) {
        this.x55 = x55;
    }

    public String getX56() {
        return x56;
    }

    public void setX56(String x56) {
        this.x56 = x56;
    }

    public String getX57() {
        return x57;
    }

    public void setX57(String x57) {
        this.x57 = x57;
    }

    public String getX58() {
        return x58;
    }

    public void setX58(String x58) {
        this.x58 = x58;
    }

    public String getX59() {
        return x59;
    }

    public void setX59(String x59) {
        this.x59 = x59;
    }

    public String getX60() {
        return x60;
    }

    public void setX60(String x60) {
        this.x60 = x60;
    }
}