package com.godwin.betgames.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Deck implements Serializable {

    @SerializedName("item0") @Expose private String item0;
    @SerializedName("item1") @Expose private String item1;
    @SerializedName("item2") @Expose private String item2;
    @SerializedName("item3") @Expose private String item3;
    private final static long serialVersionUID = 6278062631831517413L;


    public String getItem0() {
        return item0;
    }

    public void setItem0(String item0) {
        this.item0 = item0;
    }

    public String getItem1() {
        return item1;
    }

    public void setItem1(String item1) {
        this.item1 = item1;
    }

    public String getItem2() {
        return item2;
    }

    public void setItem2(String item2) {
        this.item2 = item2;
    }

    public String getItem3() {
        return item3;
    }

    public void setItem3(String item3) {
        this.item3 = item3;
    }

}