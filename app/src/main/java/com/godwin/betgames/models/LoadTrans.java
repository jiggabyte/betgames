package com.godwin.betgames.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoadTrans implements Serializable {
    @SerializedName("transactioner") @Expose private Transactioner transactioner;
    private final static long serialVersionUID = 782345634521051753L;


    public LoadTrans(Transactioner transactioner) {
        this.transactioner = transactioner;
    }

    public Transactioner getTransactioner() {
        return transactioner;
    }

    public void setTransactioner(Transactioner transactioner) {
        this.transactioner = transactioner;
    }
}