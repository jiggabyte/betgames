package com.godwin.betgames;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.godwin.betgames.models.User;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OnlineWorker extends Worker {
    boolean isConnected;
    Session session;
    public OnlineWorker(
            @NonNull Context context,
            @NonNull WorkerParameters params) {
        super(context, params);
        session = new Session(getApplicationContext());
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    @Override
    public Result doWork() {

        // Do the work here--in this case ...
       if(isConnected){
           RestService service = RestClient.getClient(session).create(RestService.class);
           retrofit2.Call<User> call = service.userSetOnline("Bearer " + session.getUserToken(), session.getUserID());
           call.enqueue(new Callback<User>() {
               @Override
               public void onResponse(retrofit2.Call<User> call, Response<User> response) {

                   //Toast.makeText()
                   if (response.isSuccessful()) {
                       if (response.body() != null) {

                       }
                   } else {

                   }
               }

               @Override
               public void onFailure(Call<User> call, Throwable t) {

               }
           });
           // Indicate whether the work finished successfully with the Result
           return Result.success();
       } else {

           // Indicate whether the work finished unsuccessfully with the Result
           return Result.failure();
       }


    }
}

