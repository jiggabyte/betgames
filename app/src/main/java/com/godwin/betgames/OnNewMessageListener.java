package com.godwin.betgames;

public interface OnNewMessageListener {
    void onNewMessageReceived(String activationCode);
}
