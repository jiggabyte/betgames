package com.godwin.betgames;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.godwin.betgames.views.ClassicWhotActivity;
import com.godwin.betgames.views.FinishWhotActivity;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;


public class Call extends BroadcastReceiver {

    public void onReceive(Context context, Intent intent) {

        try {
            // TELEPHONY MANAGER class object to register one listener
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            //Create Listener
            MyPhoneStateListener phoneListener = new MyPhoneStateListener();

            // Register listener for LISTEN_CALL_STATE
            telephonyManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);

        } catch (Exception e) {
            Log.e("Phone Receive Error", " " + e);
        }

    }

    private static class MyPhoneStateListener extends PhoneStateListener {

        public void onCallStateChanged(int state, String incomingNumber) {

            Log.d("MyPhoneListener",state+"   incoming no:"+incomingNumber);

            if (state == TelephonyManager.CALL_STATE_RINGING) {
                if(ClassicWhotActivity.active){
                    ClassicWhotActivity classicWhotActivity = new ClassicWhotActivity();
                    classicWhotActivity.gameSound("stop");
                } else if(FinishWhotActivity.active){
                    FinishWhotActivity finishWhotActivity = new FinishWhotActivity();
                    finishWhotActivity.gameSound("stop");
                }

            } else if(state == TelephonyManager.CALL_STATE_IDLE){
                if(ClassicWhotActivity.active){
                    ClassicWhotActivity classicWhotActivity = new ClassicWhotActivity();
                    classicWhotActivity.gameSound("play");
                } else if(FinishWhotActivity.active){
                    FinishWhotActivity finishWhotActivity = new FinishWhotActivity();
                    finishWhotActivity.gameSound("play");
                }
            } else if(state == TelephonyManager.CALL_STATE_OFFHOOK){
                if(ClassicWhotActivity.active){
                    ClassicWhotActivity classicWhotActivity = new ClassicWhotActivity();
                    classicWhotActivity.gameSound("stop");
                } else if(FinishWhotActivity.active){
                    FinishWhotActivity finishWhotActivity = new FinishWhotActivity();
                    finishWhotActivity.gameSound("stop");
                }
            }
        }
    }
}