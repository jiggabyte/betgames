package com.godwin.betgames;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Session {

    SharedPreferences prefs;

    public Session(Context cntx) {
        // TODO Auto-generated constructor stub
        prefs = PreferenceManager.getDefaultSharedPreferences(cntx);
    }

    public void setUserID(String username) {
        prefs.edit().putString("userID", username).apply();
    }

    public String getUserID() {
        return prefs.getString("userID",null);
    }

    public void setUserJSON(String userData) {
        prefs.edit().putString("userJSON", userData).apply();
    }

    public String getUserJSON() {
        return prefs.getString("userJSON",null);
    }

    public void setUserToken(String username) {
        prefs.edit().putString("userToken", username).apply();
    }

    public String getUserToken() {
        return prefs.getString("userToken", null);
    }

    public void setDeviceID(String username) {
        prefs.edit().putString("deviceID", username).apply();
    }

    public String getDeviceID() {
        return prefs.getString("deviceID",null);
    }

    public void setNotaRec(String nota_rec) {
        prefs.edit().putString("playNota", nota_rec).apply();
    }

    public String getNotaRec() {
        return prefs.getString("playNota",null);
    }

    public void setNoteRec(String note_rec) {
        prefs.edit().putString("playNote", note_rec).apply();
    }

    public String getNoteRec() {
        return prefs.getString("playNote",null);
    }

    public void setNotiRec(String noti_rec) {
        prefs.edit().putString("playNoti", noti_rec).apply();
    }

    public String getNotiRec() {
        return prefs.getString("playNoti",null);
    }

    public void setNotoRec(String noto_rec) {
        prefs.edit().putString("playNoto", noto_rec).apply();
    }

    public String getNotoRec() {
        return prefs.getString("playNoto",null);
    }

    public void setNotuRec(String notu_rec) {
        prefs.edit().putString("playNotu", notu_rec).apply();
    }

    public String getNotuRec() {
        return prefs.getString("playNotu",null);
    }


    public void setNotyRec(String noty_rec) {
        prefs.edit().putString("playNoty", noty_rec).apply();
    }

    public String getNotyRec() {
        return prefs.getString("playNoty",null);
    }


    public void setPlay(String gamePlay) {
        prefs.edit().putString("gamePlay", gamePlay).apply();
    }

    public String getPlay() {
        return prefs.getString("gamePlay",null);
    }


    public void setReqTitle(String regTitle) {
        prefs.edit().putString("reqTitle", regTitle).apply();
    }

    public String getReqTitle() {
        return prefs.getString("reqTitle",null);
    }

    public void setReqStop(String reqStop) {
        prefs.edit().putString("reqStop", reqStop).apply();
    }

    public String getReqStop() {
        return prefs.getString("reqStop",null);
    }

    public void setReqNota(String reqNota) {
        prefs.edit().putString("reqNota", reqNota).apply();
    }

    public String getReqNota() {
        return prefs.getString("reqNota",null);
    }

    public void setUserBeta(String userBeta) {
        prefs.edit().putString("userBeta", userBeta).apply();
    }

    public String getUserBeta() {
        return prefs.getString("userBeta", null);
    }

    public void setUserAcc(String userAcc) {
        prefs.edit().putString("userAcc", userAcc).apply();
    }

    public String getUserAcc() {
        return prefs.getString("userAcc", null);
    }

    public void setBetId(String betId) {
        prefs.edit().putString("betId", betId).apply();
    }

    public String getBetId() {
        return prefs.getString("betId",null);
    }

    public void setWinaRec(String wina_rec) {
        prefs.edit().putString("gameWina", wina_rec).apply();
    }

    public String getWinaRec() {
        return prefs.getString("gameWina",null);
    }

    public void setLosaRec(String losa_rec) {
        prefs.edit().putString("gameLosa", losa_rec).apply();
    }

    public String getLosaRec() {
        return prefs.getString("gameLosa",null);
    }

    public void setGameState(String game_state) {
        prefs.edit().putString("gameState", game_state).apply();
    }

    public String getGameState() {
        return prefs.getString("gameState",null);
    }

    public void setHomeState(String home_state) {
        prefs.edit().putString("homeState", home_state).apply();
    }

    public String getHomeState() {
        return prefs.getString("homeState",null);
    }

    public void setBackState(String back_state) {
        prefs.edit().putString("backState", back_state).apply();
    }

    public String getBackState() {
        return prefs.getString("backState",null);
    }


    public void setBackgroundState(String backer_state) {
        prefs.edit().putString("backerState", backer_state).apply();
    }

    public String getBackgroundState() {
        return prefs.getString("backerState",null);
    }


    public void setChallengeState(String chall_state) {
        prefs.edit().putString("challState", chall_state).apply();
    }

    public String getChallengeState() {
        return prefs.getString("challState",null);
    }

    public void setDataState(String data_state) {
        prefs.edit().putString("dataState", data_state).apply();
    }

    public String getDataState() {
        return prefs.getString("dataState",null);
    }

    public void setMoveState(String move_state) {
        prefs.edit().putString("moveState", move_state).apply();
    }

    public String getMoveState() {
        return prefs.getString("moveState",null);
    }

    public void setEndState(int end_state) {
        prefs.edit().putInt("endState", end_state).apply();
    }

    public int getEndState() {
        return prefs.getInt("endState",0);
    }

    public void setAppState(String app_state) {
        prefs.edit().putString("appState", app_state).apply();
    }

    public String getAppState() {
        return prefs.getString("appState",null);
    }

    public void setUpdated(String time_state) {
        prefs.edit().putString("timeState", time_state).apply();
    }

    public String getUpdated() {
        return prefs.getString("timeState",null);
    }

    public void setPhoneNo(String phoneNo) {
        prefs.edit().putString("phoneNo", phoneNo).apply();
    }

    public String getPhoneNo() {
        return prefs.getString("phoneNo",null);
    }

    public void setFirstRun(String firstRun) {
        prefs.edit().putString("firstRun", firstRun).apply();
    }

    public String getFirstRun() {
        return prefs.getString("firstRun",null);
    }

    public void setReferer(String referer) {
        prefs.edit().putString("referer", referer).apply();
    }

    public String getReferer() {
        return prefs.getString("referer",null);
    }

    public void setDisplayStop(String displayStop) {
        prefs.edit().putString("displayStop", displayStop).apply();
    }

    public String getDisplayStop() {
        return prefs.getString("displayStop",null);
    }



    public void setResendStop(String resendStop) {
        prefs.edit().putString("resendStop", resendStop).apply();
    }

    public String getResendStop() {
        return prefs.getString("resendStop",null);
    }


    public void setSplashState(String splash_state) {
        prefs.edit().putString("splashState", splash_state).apply();
    }

    public String getSplashState() {
        return prefs.getString("splashState",null);
    }



}