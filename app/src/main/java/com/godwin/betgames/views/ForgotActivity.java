package com.godwin.betgames.views;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.godwin.betgames.PreferenceHelper;
import com.godwin.betgames.R;
import com.godwin.betgames.RestClient;
import com.godwin.betgames.RestService;
import com.godwin.betgames.Session;
import com.godwin.betgames.Utils;
import com.godwin.betgames.models.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotActivity extends AppCompatActivity implements View.OnClickListener {
    private View view;

    private EditText emailId;
    private TextView submit, back;
    private FragmentManager fragmentManager;
    private ProgressDialog pDialog;

    //Shared Preference
    private PreferenceHelper preferenceHelper;
    //Session Handler
    private Session userSession;

    public ForgotActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_forgot);
        preferenceHelper = new PreferenceHelper(this);
        userSession = new Session(this);
        initViews();
        setListeners();
    }

    @Override
    public void onBackPressed() {
        /**
        new AlertDialog.Builder(this)
                .setTitle("Click OK to Exit")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        // FinishWhotActivity.super.onBackPressed();
                        //Toast.makeText(MainActivity.this, "Game Over, You Lost!", Toast.LENGTH_LONG).show();
                        ForgotActivity.this.finish();

                    }
                }).create().show();
         */

    }

    // Initialize the views
    private void initViews() {
        fragmentManager = getSupportFragmentManager();
        emailId = findViewById(R.id.user_id);
        submit = findViewById(R.id.forgot_button);
        back = findViewById(R.id.backToLoginBtn);

        // Setting text selector over textviews
        int[][] states = new int[][] {
                new int[] {-android.R.attr.state_pressed}, // unpressed
                new int[] { android.R.attr.state_pressed}  // pressed
        };

        int[] colors = new int[] {
                Color.WHITE,
                Color.BLACK
        };

        try {
            ColorStateList csl = new ColorStateList(states, colors);

            back.setTextColor(csl);
            submit.setTextColor(csl);

        } catch (Exception e) {
        }

    }

    // Set Listeners over buttons
    private void setListeners() {
        back.setOnClickListener(this);
        submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backToLoginBtn:

                // Replace Login Activity
                Intent intentLogin;
                intentLogin = new Intent(ForgotActivity.this, LoginActivity.class);
                intentLogin.putExtra("stopper","false");
                startActivity(intentLogin);
                break;

            case R.id.forgot_button:

                // Call Submit button_extra task
                submitButtonTask();
                break;

        }

    }

    private void submitButtonTask() {
        final String getEmailId = emailId.getText().toString();

        // Pattern for email id validation
        // Pattern p = Pattern.compile(Utils.regEx);

        // Match the pattern
        // Matcher m = p.matcher(getEmailId);

        // First check if email id is not null else show error toast
        if (getEmailId.equals("") || getEmailId.length() == 0) {

            displayer("Forgot Error!", "Please enter your Username.");

            // Check if email id is valid or not
            // else if (!m.find())

            // displayer("Forgot Error!","Your  Id is Invalid.");

      //  } else if (getEmailId.length() > 11 || getEmailId.length() < 10) {

        //    displayer("Forgot Error!", "Please enter your Phone No.");

            // Else submit email id and fetch password or do your stuff
        } else {
            RestService service = RestClient.getClient(userSession).create(RestService.class);
            Call<User> call = service.userForgot("Bearer "+userSession.getUserToken(),getEmailId);
            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {

                    //Toast.makeText()
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            Toast.makeText(ForgotActivity.this, "Code Sent!", Toast.LENGTH_SHORT).show();
                            userSession.setPhoneNo(getEmailId);
                            // Replace ForgotActivity
                            Intent intentfor;
                            intentfor = new Intent(ForgotActivity.this, VerifyActivity.class);
                            intentfor.putExtra("forgot", "true");
                            startActivity(intentfor);
                        }
                    } else {
                        Toast.makeText(ForgotActivity.this, "Code Not Sent!", Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    Toast.makeText(ForgotActivity.this, "Connecting Error!", Toast.LENGTH_SHORT).show();
                }
            });


        }

    }

    private void displayer(String titler, String messager){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(false);
        dialog.setTitle(titler);
        dialog.setMessage(messager);
        dialog.setPositiveButton("Ok", null);

        final AlertDialog alert = dialog.create();
        alert.show();
    }
}