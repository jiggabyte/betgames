package com.godwin.betgames.views;

import android.content.Context;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import com.godwin.betgames.Session;

public class WebAppInterface {
    Context mContext;
    Session session;
    com.godwin.betgames.views.PaymentActivity pay;

    /** Instantiate the interface and set the context */
   public WebAppInterface(Context c) {
        mContext = c;
        session = new Session(c);
        pay = new com.godwin.betgames.views.PaymentActivity();
    }

    /** Show a toast from the web page */
    @JavascriptInterface
    public void showToast(String toast) {
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
    }

    /** Return user logged in uuid */
    @JavascriptInterface
    public String getUserID() {
        return session.getUserID();
    }

    /** Return user logged in token  */
    @JavascriptInterface
    public String getUserLoggedInToken() {
        return session.getUserToken();
    }

    /** Return user logged in details*/
    @JavascriptInterface
    public String getUserDetails() {
        return session.getUserJSON();
    }


}