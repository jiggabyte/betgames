package com.godwin.betgames.views;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.godwin.betgames.MainActivity;
import com.godwin.betgames.R;
import com.godwin.betgames.Session;

public class SplashActivity extends AppCompatActivity {

    Session session;

    Handler handler = new Handler();
    String stopper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = new Session(this);
        Intent intents = getIntent();
        stopper = intents.getStringExtra("stopper");
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();

        if(session.getSplashState() != null) {
            Intent intentSplash = new Intent( SplashActivity.this, MainActivity.class);
            intentSplash.putExtra("stopper", stopper);
            startActivity(intentSplash);
            SplashActivity.this.finish();
        }

        session.setGameState(null);

        session.setNotaRec(null);
        session.setNoteRec(null);
        session.setNotiRec(null);
        session.setNotoRec(null);
        session.setNotuRec(null);
        session.setNotyRec(null);
        session.setWinaRec(null);
        session.setLosaRec(null);
        session.setPlay("false");
        session.setReqNota(null);
        session.setUserBeta(null);
        session.setUserAcc(null);
        session.setBetId(null);


        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 3s = 3000ms

                if(session.getUserID() != null) {
                    Intent intentMain = new Intent(SplashActivity.this, MainActivity.class);
                    intentMain.putExtra("stopper", stopper);
                    session.setSplashState("active");
                    startActivity(intentMain);
                    SplashActivity.this.finish();
                } else {
                    Intent intentLogin;
                    intentLogin = new Intent(SplashActivity.this, LoginActivity.class);
                    session.setSplashState("active");
                    startActivity(intentLogin);
                    SplashActivity.this.finish();
                }

            }
        }, 5000);
    }
}
