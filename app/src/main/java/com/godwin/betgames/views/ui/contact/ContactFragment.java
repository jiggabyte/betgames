package com.godwin.betgames.views.ui.contact;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.godwin.betgames.R;
import com.godwin.betgames.RestClient;
import com.godwin.betgames.RestService;
import com.godwin.betgames.Session;
import com.godwin.betgames.Utils;
import com.godwin.betgames.models.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactFragment extends Fragment {

    private EditText namer;
    private EditText mailer;
    private EditText fone;
    private EditText msg;

    private Button sendButton;

    Session session;

    private ProgressDialog pDialog;

    private FragmentManager fragmentManager;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_contact, container, false);

        session = new Session(getActivity());

        pDialog = new ProgressDialog(getActivity(),  R.style.AppTheme_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        namer = root.findViewById(R.id.senderName);
        mailer = root.findViewById(R.id.senderMail);
        fone = root.findViewById(R.id.senderFone);
        msg = root.findViewById(R.id.senderMsg);

        sendButton = root.findViewById(R.id.senderBtn);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkValidation();
            }
        });

        return root;
    }

    // Check Validation Method
    private void checkValidation() {

        // Get all editText texts
        String getFullName = namer.getText().toString();
        String getEmailId = mailer.getText().toString();
        String getMsg = msg.getText().toString();
        String getMobileNumber = fone.getText().toString();

        // Pattern match for email id
        Pattern p = Pattern.compile(Utils.regEx);
        Matcher m = p.matcher(getEmailId);

        // Check if all strings are null or not
        if (getFullName.equals("") || getFullName.length() == 0
                || getMsg.equals("") || getMsg.length() == 0
                || getMobileNumber.equals("") || getMobileNumber.length() == 0) {



            displayer("Error!", "Check required fields.");
        } else {

            senderMailer(getFullName,getEmailId,getMsg,getMobileNumber);
        }


    }

    public void senderMailer(String namer, String mailer, String msg, String fone){
        showpDialog();
        RestService service = RestClient.getClient(session).create(RestService.class);
        Call<User> call = service.userMailer("Bearer " + session.getUserToken(), namer, mailer, msg, fone);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        hidepDialog();
                        Toast.makeText(getActivity(), "Mail Sent! "+response.body().getSuccessor(), Toast.LENGTH_LONG).show();
                        // Replace account fragment with animation
                        fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction
                                .addToBackStack(null)
                                //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                                .replace(R.id.home_host, new ContactFragment(),
                                        Utils.Contact_Fragment).commit();
                    }
                } else {
                    hidepDialog();
                    Toast.makeText(getActivity(), "Mail Not Sent! "+response.toString(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                hidepDialog();
                Toast.makeText(getActivity(), "Mail Error! "+t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void displayer(String titler, String messager){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setCancelable(false);
        dialog.setTitle(titler);
        dialog.setMessage(messager);
        dialog.setPositiveButton("Ok", null);

        final AlertDialog alert = dialog.create();
        alert.show();
    }

    public void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    public void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}