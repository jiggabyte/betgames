package com.godwin.betgames.views.ui.challenger;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.godwin.betgames.R;
import com.godwin.betgames.RestClient;
import com.godwin.betgames.RestService;
import com.godwin.betgames.Session;
import com.godwin.betgames.Utils;
import com.godwin.betgames.models.Loadbet;
import com.godwin.betgames.models.Nota;
import com.godwin.betgames.models.SuccessLoad;
import com.godwin.betgames.models.User;
import com.godwin.betgames.views.LoginActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChallengerFragment extends Fragment implements View.OnClickListener{

    public ProgressDialog pDialog;
    private Session userSession;
    private Session session;
    private Integer offset;
    private Integer limit;
    private LinearLayout linearView;
    private String loadCounter;
    private boolean loaderPointer = false;
    private TextView loadEmptyTxt;
    private Button loadEmptyButton;
    private Button searchEmptyButton;
    private Button prev;
    private Button next;
    private int prevState = 0;
    private int nextState = 0;
    private int countState = 0;
    private int totalState = 0;
    private SearchView searcher;
    private Button searchButton;
    private boolean searchPointer = false;
    private String queryGlobal;
    private ScrollView scv;
    private FragmentManager fragmentManager;
    Handler handler;
    Runnable runnablec;

    ValueEventListener startxEventListener;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        userSession = new Session(getActivity());
        session = new Session(getActivity());
        Context context = getContext();

        View root = inflater.inflate(R.layout.fragment_challenger, container, false);

        handler = new Handler();

        searcher = root.findViewById(R.id.searchView);
        searchButton = root.findViewById(R.id.searchButton);
        prev = root.findViewById(R.id.prevButton);
        next = root.findViewById(R.id.nextButton);
        loadEmptyTxt = root.findViewById(R.id.noDataTXT);
        loadEmptyButton = root.findViewById(R.id.noDataButton);
        prev.setOnClickListener(this);
        next.setOnClickListener(this);
        searchButton.setOnClickListener(this);
        linearView = root.findViewById(R.id.list_box);
        scv = root.findViewById(R.id.scroll_box);
        loadEmptyButton.setOnClickListener(this);
        if(loadCounter == null){
            loadBetFromServer(0,10);
            searchPointer = false;
        } else {
            // Do nothing
            String hunter = "last";

        }

        userSession.setChallengeState(null);

        searcher.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
               // prevState = 0;
               // nextState = 10;
               // countState = 0;
               // totalState = 0;
                searchBetFromServer(0,10,query);
                queryGlobal = query;
               // searchPointer = true;

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });




        return root;
    }


    @Override
    public void onDestroy () {
        handler.removeCallbacks(runnablec);
        super.onDestroy ();

    }


    @Override
    public void onStop() {
        super.onStop();

    }

    public void displayStop(String titler, String messager, final Context cntx){

        session.setChallengeState(null);
        // session.setAppState(null);
        AlertDialog.Builder dialog = new AlertDialog.Builder(cntx, R.style.Theme_AppCompat_Light_Dialog);
        dialog.setCancelable(false);
        dialog.setTitle(titler);
        dialog.setMessage(messager);
        session.setPlay(null);
        session.setReqTitle(null);
        session.setReqStop(null);
        session.setReqNota(null);
        session.setUserBeta(null);
        session.setUserAcc(null);
        //  session.setBetId(null);



        dialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                Log.d("StopConfirm", "Yeta Clicked!");

                RestService service = RestClient.getClient(session).create(RestService.class);
                Call<com.godwin.betgames.models.User> call = service.userRemStop("Bearer " + session.getUserToken(), session.getUserID());
                call.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {

                        //Toast.makeText()
                        if (response.isSuccessful()) {
                            if (response.body() != null) {

                                ChallengerFragment challengerFrag = new ChallengerFragment();
                                if (challengerFrag.isAdded()) {
                                    challengerFrag.removeHandler();
                                }

                                // Replace account fragment with animation
                                fragmentManager = getActivity().getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction
                                        .addToBackStack(null)
                                        //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                                        .replace(R.id.home_host, new ChallengerFragment(),
                                                Utils.Challenger_Fragment).commit();
                            }
                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        Toast.makeText(cntx, "Request Cancelled Failed!", Toast.LENGTH_SHORT).show();
                    }
                });


            }
        });

        final AlertDialog alert = dialog.create();
        if (!getActivity().isFinishing()) {
            alert.show();
        } else {

        }


    }

    public void displayStoper(String titler, String messager, Context cntx){

        AlertDialog.Builder dialog = new AlertDialog.Builder(cntx);
        dialog.setCancelable(false);
        dialog.setTitle(titler);
        dialog.setMessage(messager);
        dialog.setNegativeButton(android.R.string.no,new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                Log.d("StopConfirm","Nota Clicked!");
                if(!getActivity().isFinishing()){
	                hidepDialog();
                }
            //    hidepDialog();
            }
        });
        dialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                Log.d("StopConfirm","Yeta Clicked!");
                if(!getActivity().isFinishing()){
	                hidepDialog();
                }
            //    hidepDialog();
            }
        });

        final AlertDialog alert = dialog.create();
        alert.show();

    }


    private void searchBetFromServer(Integer offseta, Integer limita, String query) {
        pDialog = new ProgressDialog(getContext(),  R.style.AppTheme_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Searching...");
        pDialog.setCancelable(false);

        offset = offseta;
        limit = limita;



        if(!getActivity().isFinishing()){
	        showpDialog();
        }

       // showpDialog();

        RestService serviceBet = RestClient.getClient(userSession).create(RestService.class);
        Call<Loadbet> call = serviceBet.userSearchBet("Bearer "+userSession.getUserToken(),userSession.getUserID(),offset,limit,query);
        call.enqueue(new Callback<Loadbet>() {
            @Override
            public void onResponse(Call<Loadbet> call, Response<Loadbet> response) {
                if(!getActivity().isFinishing()){
	                hidepDialog();
                }
              //  hidepDialog();

                if(loaderPointer){
                    loadEmptyTxt.setVisibility(View.INVISIBLE);
                    loadEmptyButton.setVisibility(View.INVISIBLE);
                    loaderPointer = false;
                }

                //Toast.makeText()
                if (response.isSuccessful()) {
                    for(int k=0;k<linearView.getChildCount();k++){
                        linearView.getChildAt(k).setVisibility(View.INVISIBLE);
                    }
                    prev.setVisibility(View.INVISIBLE);
                    next.setVisibility(View.INVISIBLE);

                    if (response.body() != null) {

                        String responser = response.body().getSuccess().getNota().toString();
                        Integer counta = response.body().getSuccess().getCounta();
                        if(counta > 10){
                            totalState = counta;
                            if(prevState <= 0){
                                prev.setEnabled(false);
                            } else {
                                prev.setEnabled(true);
                            }
                            if(totalState - countState <= 10){
                                next.setEnabled(false);
                            } else {
                                next.setEnabled(true);
                            }
                            prev.setVisibility(View.INVISIBLE);
                            next.setVisibility(View.INVISIBLE);
                        } else if(counta == 0){
                            displayer("Search", "No Match Found!");
                            prev.setVisibility(View.INVISIBLE);
                            next.setVisibility(View.INVISIBLE);

                                loadCounter = null;
                                loadBetFromServer(0,10);
                                searchPointer = false;
                        }
                        // Toast.makeText(getActivity(), "Responser "+responser, Toast.LENGTH_SHORT).show();
                        //  Log.d("onSuccessBet", "Test "+response.body().getSuccess().getNota());

                        ArrayList<SuccessLoad> responseMsg = response.body().getSuccess().getNota();
                        for(int i = 0;i < responseMsg.size();i++){
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            Gson gson = gsonBuilder.create();
                            String objecto = gson.toJson(responseMsg.get(i));
                            try {
                                JSONObject objector = new JSONObject(objecto.trim());

                                final String responseId = objector.getString("id");
                                String responseOnline = null;
                                if(objector.has("online_status") && objector.getString("online_status") != null) {
                                    responseOnline = objector.getString("online_status");
                                }
                                final String responseName = objector.getString("uuid");
                                String responseAmt;
                                if(Integer.parseInt(objector.getString("amt")) == 0){
                                    responseAmt = "Free Game!";
                                } else {
                                    responseAmt = "₦"+objector.getString("amt");
                                }
                                String responseGame = objector.getString("game");
                                linearView.getChildAt(i).setVisibility(View.VISIBLE);
                                CardView cardia = (CardView) linearView.getChildAt(i);
                                TextView textName = (TextView) cardia.getChildAt(0);
                                TextView textGame = (TextView) cardia.getChildAt(1);
                                TextView textAmt = (TextView) cardia.getChildAt(2);
                                TextView textOnline = (TextView) cardia.getChildAt(3);
                                final Button challenButton = (Button) cardia.getChildAt(4);
                                textName.setText(responseName);
                                textGame.setText(responseGame);
                                textAmt.setText(responseAmt);
                                if(responseOnline != null && responseOnline.equals("online")){
                                    textOnline.setText(responseOnline);
                                }
                                challenButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        // Get firebase database reference
                                        DatabaseReference userOnlinerStatus = FirebaseDatabase.getInstance().getReference("users/"+responseName+"/status");
                                        userOnlinerStatus.addListenerForSingleValueEvent( new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot snapshot) {
                                           //     if(snapshot.getValue().equals("online")){
                                                Activity activity = getActivity();
                                                if(activity == null) return;

                                                    if (userSession.getChallengeState() != null) {
                                                        Toast.makeText(getActivity(), "Please Wait!", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        String usernome = userSession.getUserID();
                                                        final String userbeta = responseName;
                                                        final String betId = responseId;

                                                        if (!getActivity().isFinishing()) {
                                                            showpDialog();
                                                        }

                                                     //   showpDialog();

                                                        RestService service = RestClient.getClient(userSession).create(RestService.class);
                                                        Call<User> call = service.userStart("Bearer " + userSession.getUserToken(), userbeta, usernome, betId);
                                                        call.enqueue(new Callback<User>() {
                                                            @Override
                                                            public void onResponse(Call<User> call, Response<User> response) {


                                                                //Toast.makeText()
                                                                if (response.isSuccessful()) {
                                                                    if (response.body() != null) {
                                                                        // String jsonResponse = response.body().toString();
                                                                        //saveInfo(jsonResponse);
                                                                        userSession.setChallengeState("active");
                                                                        Toast.makeText(getActivity(), "Request Sent!", Toast.LENGTH_SHORT).show();
                                                                        Log.d("Tokeniser: ", response.body().getSuccessor());
                                                                        //cardia.setVisibility(View.GONE);
                                                                        challenButton.setText(R.string.busyx);


                                                                        int delay = 80000; //milliseconds

                                                                        runnablec = new Runnable() {
                                                                            public void run() {
                                                                                if (userSession.getGameState() != null && "active".equals(userSession.getGameState())) {
                                                                                    if (!getActivity().isFinishing()) {
                                                                                        hidepDialog();
                                                                                    }
                                                                                    //   hidepDialog();
                                                                                    userSession.setChallengeState(null);
                                                                                    challenButton.setText(R.string.accept);
                                                                                } else {
                                                                                    //do something
                                                                                    if (!getActivity().isFinishing()) {
                                                                                        hidepDialog();
                                                                                    }
                                                                                 //   hidepDialog();
                                                                                    userSession.setChallengeState(null);
                                                                                    challenButton.setText(R.string.accept);
                                                                                }

                                                                            }
                                                                        };

                                                                        handler.postDelayed(runnablec, delay);


                                                                    } else {
                                                                        if (!getActivity().isFinishing()) {
                                                                            hidepDialog();
                                                                        }
                                                                      //  hidepDialog();
                                                                        userSession.setChallengeState(null);
                                                                        Toast.makeText(getActivity(), "Empty response", Toast.LENGTH_LONG).show();
                                                                    }

                                                                } else {
                                                                    if (!getActivity().isFinishing()) {
                                                                        hidepDialog();
                                                                    }
                                                                  //  hidepDialog();
                                                                    userSession.setChallengeState(null);
                                                                    Toast.makeText(getActivity(), "Request Not Sent, Check your Internet or Balance!", Toast.LENGTH_SHORT).show();
                                                                }
                                                            }

                                                            @Override
                                                            public void onFailure(Call<User> call, final Throwable t) {


                                                                // Get firebase database reference
                                                                FirebaseDatabase database = FirebaseDatabase.getInstance();
                                                                DatabaseReference gameDbRef = database.getReference("startx");

                                                                if (userSession.getUserID() == null) {


                                                                } else {

                                                                    // Read from the database


                                                                    startxEventListener = gameDbRef.child(userbeta).addValueEventListener(new ValueEventListener() {
                                                                        @Override
                                                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                                                            // This method is called once with the initial value and again
                                                                            // whenever data at this location is updated.
                                                                            Nota nota = dataSnapshot.getValue(Nota.class);

                                                                            Activity activity = getActivity();
                                                                            if(activity == null) return;

                                                                            if ("".equals(nota.getBacker())) {

                                                                            } else {

                                                                                if (nota.getNota() != null) {
                                                                                    String action = nota.getBet_id();

                                                                                    if (action.equals(betId)) {
                                                                                        if (!getActivity().isFinishing()) {
                                                                                            showpDialog();
                                                                                        }

                                                                                        //    showpDialog();

                                                                                    } else {
                                                                                        if (!getActivity().isFinishing()) {
                                                                                            hidepDialog();
                                                                                        }
                                                                                        // hidepDialog();
                                                                                        userSession.setChallengeState(null);
                                                                                        Log.d("onFailure", t.toString());
                                                                                        Toast.makeText(getActivity(), "Failure Communicating With Server", Toast.LENGTH_SHORT).show();

                                                                                    }


                                                                                }

                                                                            }


                                                                        }

                                                                        @Override
                                                                        public void onCancelled(DatabaseError error) {
                                                                            // Failed to read value
                                                                            if (!getActivity().isFinishing()) {
                                                                                hidepDialog();
                                                                            }
                                                                            //   hidepDialog();
                                                                            userSession.setChallengeState(null);
                                                                            Log.d("onFailure", t.toString());
                                                                            Toast.makeText(getActivity(), "Failure Communicating With Server", Toast.LENGTH_SHORT).show();

                                                                            Log.w("FirebaseDB", "Failed to read value.", error.toException());
                                                                        }
                                                                    });
                                                                }

                                                            }
                                                        });
                                                    }
                                           //     } else {
                                          //          Toast.makeText(getActivity(), ""+responseName+" is away!", Toast.LENGTH_SHORT).show();
                                        //        }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError error) {
                                                // Failed to read value
                                                Log.w("FirebaseDB", "Failed to read value.", error.toException());
                                            }

                                        });

                                    }
                                });
                            } catch (JSONException error){
                                error.printStackTrace();
                                userSession.setChallengeState(null);
                            }


                        }

                        // Log.d("onStringBet", "Testor "+responseMsg);
                        loadCounter = "on";

                    } else {
                        Toast.makeText(getActivity(),"Empty response",Toast.LENGTH_LONG).show();
                    }
                } else {
                    Log.d("onFailureBet", response.toString());
                    Toast.makeText(getActivity(), "Load Error!", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Loadbet> call, Throwable t) {
                if(!getActivity().isFinishing()){
	                hidepDialog();
                }
            //    hidepDialog();

                loadBetFromServer(0,10);
                Log.d("onFailureBet", t.toString());
                Toast.makeText(getActivity(), "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
            }
        });
        scv.fullScroll(ScrollView.FOCUS_UP);
    }

    private void loadBetFromServer(Integer offseta, Integer limita) {
        pDialog = new ProgressDialog(getContext(),  R.style.AppTheme_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        offset = offseta;
        limit = limita;

        if(!getActivity().isFinishing()){
	        showpDialog();
        }
     //   showpDialog();

        RestService serviceBet = RestClient.getClient(userSession).create(RestService.class);
        Call<Loadbet> call = serviceBet.userLoadBet("Bearer "+userSession.getUserToken(),userSession.getUserID(),offset,limit);
        call.enqueue(new Callback<Loadbet>() {
            @Override
            public void onResponse(Call<Loadbet> call, Response<Loadbet> response) {

                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        for(int k=0;k<linearView.getChildCount();k++){
                            linearView.getChildAt(k).setVisibility(View.INVISIBLE);
                        }
                        prev.setVisibility(View.INVISIBLE);
                        next.setVisibility(View.INVISIBLE);

                        if(!getActivity().isFinishing()){
	                        hidepDialog();
                        }

                     //   hidepDialog();

                        if(loaderPointer){
                            loadEmptyTxt.setVisibility(View.INVISIBLE);
                            loadEmptyButton.setVisibility(View.INVISIBLE);
                            loaderPointer = false;
                        }

                        if(response.body().getSuccess().getCounta() == 0){
                            loadEmptyTxt.setVisibility(View.VISIBLE);
                            loadEmptyButton.setVisibility(View.VISIBLE);
                        }


                        String responser = response.body().getSuccess().getNota().toString();
                        Integer counta = response.body().getSuccess().getCounta();
                        if(counta > 10){
                            totalState = counta;
                            if(prevState <= 0){
                                prev.setEnabled(false);
                            } else {
                                prev.setEnabled(true);
                            }
                            if(totalState - countState <= 10){
                                next.setEnabled(false);
                            } else {
                                next.setEnabled(true);
                            }
                            prev.setVisibility(View.VISIBLE);
                            next.setVisibility(View.VISIBLE);
                        }
                       // Toast.makeText(getActivity(), "Responser "+responser, Toast.LENGTH_SHORT).show();
                      //  Log.d("onSuccessBet", "Test "+response.body().getSuccess().getNota());

                            ArrayList<SuccessLoad> responseMsg = response.body().getSuccess().getNota();
                            for(int i = 0;i < responseMsg.size();i++){
                                GsonBuilder gsonBuilder = new GsonBuilder();
                                Gson gson = gsonBuilder.create();
                                String objecto = gson.toJson(responseMsg.get(i));

                                try {
                                    JSONObject objector = new JSONObject(objecto);

                                    final String responseId = objector.getString("id");
                                    String responseOnline = null;
                                    if(objector.has("online_status") && objector.getString("online_status") != null) {
                                        responseOnline = objector.getString("online_status");
                                    }
                                    final String responseName = objector.getString("uuid");
                                    String responseAmt;
                                    if(Integer.parseInt(objector.getString("amt")) == 0){
                                        responseAmt = "Free Game!";
                                    } else {
                                        responseAmt = "₦"+objector.getString("amt");
                                    }

                                    String responseGame = objector.getString("game");
                                    linearView.getChildAt(i).setVisibility(View.VISIBLE);
                                    CardView cardia = (CardView) linearView.getChildAt(i);
                                    TextView textName = (TextView) cardia.getChildAt(0);
                                    TextView textGame = (TextView) cardia.getChildAt(1);
                                    TextView textAmt = (TextView) cardia.getChildAt(2);
                                    TextView textOnline = (TextView) cardia.getChildAt(3);
                                    final Button challenButton = (Button) cardia.getChildAt(4);
                                    textName.setText(responseName);
                                    textGame.setText(responseGame);
                                    textAmt.setText(responseAmt);
                                    if(responseOnline != null && responseOnline.equals("online")){
                                        textOnline.setText(responseOnline);
                                    }
                                    challenButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            // Get firebase database reference
                                            DatabaseReference userOnlinerStatus = FirebaseDatabase.getInstance().getReference("users/"+responseName+"/status");
                                            userOnlinerStatus.addListenerForSingleValueEvent( new ValueEventListener() {
                                                 @Override
                                                 public void onDataChange(DataSnapshot snapshot) {
                                           //        if(snapshot.getValue().equals("online")){

                                                     Activity activity = getActivity();
                                                     if(activity == null) return;

                                                       if (userSession.getChallengeState() != null) {
                                                           Toast.makeText(getActivity(), "Please Wait!", Toast.LENGTH_SHORT).show();
                                                       } else {
                                                           String usernome = userSession.getUserID();
                                                           final String userbeta = responseName;
                                                           final String betId = responseId;


                                                           if (!getActivity().isFinishing()) {
                                                               showpDialog();
                                                           }

                                                         //  showpDialog();

                                                           RestService service = RestClient.getClient(userSession).create(RestService.class);
                                                           Call<User> call = service.userStart("Bearer " + userSession.getUserToken(), userbeta, usernome, betId);
                                                           call.enqueue(new Callback<User>() {
                                                               @Override
                                                               public void onResponse(Call<User> call, Response<User> response) {

                                                                   if (response.isSuccessful()) {
                                                                       if (response.body() != null) {

                                                                           // String jsonResponse = response.body().toString();
                                                                           //saveInfo(jsonResponse);
                                                                           userSession.setChallengeState("active");

                                                                           Toast.makeText(getActivity(), "Request Sent!", Toast.LENGTH_SHORT).show();
                                                                           Log.d("Tokeniser: ", response.body().getSuccessor());
                                                                           Log.d("onConfirm", "Success: " + response.body().getSuccessor());
                                                                           //cardia.setVisibility(View.GONE);
                                                                           challenButton.setText(R.string.busyx);

                                                                           int delay = 80000; //milliseconds

                                                                           runnablec = new Runnable() {
                                                                               public void run() {
                                                                                   if (userSession.getGameState() != null && "active".equals(userSession.getGameState())) {
                                                                                       if (!getActivity().isFinishing()) {
                                                                                           hidepDialog();
                                                                                       }
                                                                                       //   hidepDialog();
                                                                                       userSession.setChallengeState(null);
                                                                                       challenButton.setText(R.string.accept);
                                                                                   } else {
                                                                                       //do something
                                                                                       if (!getActivity().isFinishing()) {
                                                                                           hidepDialog();
                                                                                       }
                                                                                    //   hidepDialog();
                                                                                       userSession.setChallengeState(null);
                                                                                       challenButton.setText(R.string.accept);
                                                                                   }

                                                                               }
                                                                           };

                                                                           handler.postDelayed(runnablec, delay);


                                                                       } else {
                                                                           if (!getActivity().isFinishing()) {
                                                                               hidepDialog();
                                                                           }
                                                                         //  hidepDialog();
                                                                           userSession.setChallengeState(null);
                                                                           Toast.makeText(getActivity(), "Empty response", Toast.LENGTH_LONG).show();
                                                                       }
                                                                   } else {
                                                                       if (!getActivity().isFinishing()) {
                                                                           hidepDialog();
                                                                       }
                                                                     //  hidepDialog();
                                                                       userSession.setChallengeState(null);
                                                                       Toast.makeText(getActivity(), "Request Not Sent, Check your Internet or Balance!", Toast.LENGTH_SHORT).show();
                                                                   }
                                                               }

                                                               @Override
                                                               public void onFailure(Call<User> call, final Throwable t) {
                                                                   // Get firebase database reference
                                                                   FirebaseDatabase database = FirebaseDatabase.getInstance();
                                                                   DatabaseReference gameDbRef = database.getReference("startx");

                                                                   if (userSession.getUserID() == null) {


                                                                   } else {

                                                                       // Read from the database

                                                                       startxEventListener = gameDbRef.child(userbeta).addValueEventListener(new ValueEventListener() {
                                                                           @Override
                                                                           public void onDataChange(DataSnapshot dataSnapshot) {
                                                                               // This method is called once with the initial value and again
                                                                               // whenever data at this location is updated.
                                                                               Nota nota = dataSnapshot.getValue(Nota.class);

                                                                               Activity activity = getActivity();
                                                                               if(activity == null) return;

                                                                               if ("".equals(nota.getBacker())) {

                                                                               } else {

                                                                                   if (nota.getNota() != null) {
                                                                                       String action = nota.getBet_id();

                                                                                       if (action.equals(betId)) {
                                                                                           if (!getActivity().isFinishing()) {
                                                                                               showpDialog();
                                                                                           }

                                                                                           //   showpDialog();

                                                                                       } else {
                                                                                           if (!getActivity().isFinishing()) {
                                                                                               hidepDialog();
                                                                                           }
                                                                                           //   hidepDialog();
                                                                                           userSession.setChallengeState(null);
                                                                                           Log.d("onFailure", t.toString());
                                                                                           Toast.makeText(getActivity(), "Failure Communicating With Server", Toast.LENGTH_SHORT).show();

                                                                                       }


                                                                                   }

                                                                               }


                                                                           }

                                                                           @Override
                                                                           public void onCancelled(DatabaseError error) {
                                                                               // Failed to read value
                                                                               if (!getActivity().isFinishing()) {
                                                                                   hidepDialog();
                                                                               }
                                                                               //    hidepDialog();
                                                                               userSession.setChallengeState(null);
                                                                               Log.d("onFailure", t.toString());
                                                                               Toast.makeText(getActivity(), "Failure Communicating With Server", Toast.LENGTH_SHORT).show();

                                                                               Log.w("FirebaseDB", "Failed to read value.", error.toException());
                                                                           }
                                                                       });
                                                                   }
                                                               }
                                                           });
                                                       }
                                         //          } else {
                                                     //  Toast.makeText(getActivity(), ""+responseName+" is away!", Toast.LENGTH_SHORT).show();
                                              //     }
                                                 }

                                                @Override
                                                public void onCancelled(DatabaseError error) {
                                                    // Failed to read value
                                                    Log.w("FirebaseDB", "Failed to read value.", error.toException());
                                                }

                                           });

                                        }
                                    });
                                } catch (JSONException error){
                                       error.printStackTrace();
                                    userSession.setChallengeState(null);
                                }


                            }

                       // Log.d("onStringBet", "Testor "+responseMsg);
                            loadCounter = "on";

                    } else {
                        if(!getActivity().isFinishing()){
                           	hidepDialog();
                        }
                       // hidepDialog();
                        Toast.makeText(getActivity(),"Empty response",Toast.LENGTH_LONG).show();
                    }
                } else {
                    if(!getActivity().isFinishing()){
	                   hidepDialog();
                    }
                  //  hidepDialog();
                    Log.d("onFailureBet", response.toString());
                    Toast.makeText(getActivity(), "Load Error!", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Loadbet> call, Throwable t) {
                if(!getActivity().isFinishing()){
	                hidepDialog();
                }
              //  hidepDialog();
                loadEmptyTxt.setVisibility(View.VISIBLE);
                loadEmptyButton.setVisibility(View.VISIBLE);
                loaderPointer = true;
                Log.d("onFailureBet", t.toString());
                Toast.makeText(getActivity(), "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
            }
        });
        scv.fullScroll(ScrollView.FOCUS_UP);
    }

    public void removeHandler()
    {
        handler.removeCallbacks(runnablec);
    }

    private void displayer(String titler, String messager){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setCancelable(false);
        dialog.setTitle(titler);
        dialog.setMessage(messager);
        dialog.setPositiveButton("Ok", null);

        final AlertDialog alert = dialog.create();
        alert.show();
    }

    public void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
    public void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void loadPrev(){
        if(prevState >= 10) {
            prevState -= 10;
            countState -= 10;
            if(searchPointer){
                searchBetFromServer(countState,10,queryGlobal);
            } else {
                loadBetFromServer(countState,10);
            }

            Log.d("prevState", " "+prevState);
            Log.d("countState", " "+countState);
            Log.d("nextState", " "+nextState);
            Log.d("totalState", " "+totalState);

        } else {
            if(searchPointer){
                searchBetFromServer(countState, 10,queryGlobal);
            } else {
                loadBetFromServer(countState, prevState);
            }

            prevState = 0;
            countState = 0;

            Log.d("prevState", " "+prevState);
            Log.d("countState", " "+countState);
            Log.d("nextState", " "+nextState);
            Log.d("totalState", " "+totalState);

        }

    }

    private void loadMore(){
        if(totalState - countState >= 10){
            prevState += 10;
            countState += 10;
            if(searchPointer){
                searchBetFromServer(countState, 10,queryGlobal);
            } else {
                loadBetFromServer(countState,10);
            }

            Log.d("prevState", " "+prevState);
            Log.d("countState", " "+countState);
            Log.d("nextState", " "+nextState);
            Log.d("totalState", " "+totalState);


        } else {
            prevState = prevState + (totalState - countState);
            countState = countState + (totalState - countState);
            if(searchPointer) {
                searchBetFromServer(countState, 10, queryGlobal);
            } else {
                loadBetFromServer(countState, 10);
            }

            Log.d("prevState", " "+prevState);
            Log.d("countState", " "+countState);
            Log.d("nextState", " "+nextState);
            Log.d("totalState", " "+totalState);

        }

    }




    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.prevButton:
                loadPrev();
                break;

            case R.id.nextButton:
                loadMore();
                break;


            case R.id.noDataButton:

                if(userSession.getUserToken() != null){
                    // Replace account fragment with animation
                    fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction
                            .addToBackStack(fragmentManager.getClass().getSimpleName())
                            //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                            .replace(R.id.home_host, new ChallengerFragment(),
                                    Utils.Challenger_Fragment).commit();


                } else {
                    // Replace login activity
                    Intent intentLogin;
                    intentLogin = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intentLogin);
                    getActivity().finish();

                }

                break;


            case R.id.searchButton:

                if(searcher.getQuery().toString().equals("")){
                    displayer("Search Error","Enter Search Term!");
                } else {
                   // prevState = 0;
                  //  nextState = 10;
                  //  countState = 0;
                  //  totalState = 0;
                    searchBetFromServer(0,10,searcher.getQuery().toString());
                    //searchPointer = true;
                }
                break;
        }
    }
}