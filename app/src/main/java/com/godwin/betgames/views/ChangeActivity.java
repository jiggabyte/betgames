package com.godwin.betgames.views;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.godwin.betgames.MainActivity;
import com.godwin.betgames.PreferenceHelper;
import com.godwin.betgames.R;
import com.godwin.betgames.RestClient;
import com.godwin.betgames.RestService;
import com.godwin.betgames.Session;
import com.godwin.betgames.SmsListener;
import com.godwin.betgames.models.User;

import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangeActivity extends AppCompatActivity implements View.OnClickListener {

    private View view;

    private EditText passer,firmer;
    private Button changeButton;
    private TextView login;
    private FragmentManager fragmentManager;
    private ProgressDialog pDialog;

    SmsListener smsListener;

    //Shared Preference
    private PreferenceHelper preferenceHelper;
    //Session Handler
    private Session userSession;
    private static String uniqueID = null;
    private static final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";

    public ChangeActivity() {

    }


    String stopper = "false";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_changer);
        preferenceHelper = new PreferenceHelper(this);
        userSession = new Session(this);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        initViews();
        setListeners();

    }


    @Override
    public void onBackPressed() {
        /**
        new AlertDialog.Builder(this)
                .setTitle("Click OK to Exit")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        // FinishWhotActivity.super.onBackPressed();
                        //Toast.makeText(MainActivity.this, "Game Over, You Lost!", Toast.LENGTH_LONG).show();
                        VerifyActivity.this.finish();

                    }
                }).create().show();
         */
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    }

    // Initiate Views
    private void initViews() {
        fragmentManager = getSupportFragmentManager();

        passer = findViewById(R.id.user_pass);
        firmer = findViewById(R.id.user_firmer);
        changeButton = findViewById(R.id.changer);
        login = findViewById(R.id.loginBtn);

        // Setting text selector over textviews
        int[][] states = new int[][] {
                new int[] {-android.R.attr.state_pressed}, // unpressed
                new int[] { android.R.attr.state_pressed}  // pressed
        };

        int[] colors = new int[] {
                Color.WHITE,
                Color.BLACK
        };

        try {
            ColorStateList csl = new ColorStateList(states, colors);

            passer.setTextColor(csl);
        } catch (Exception e) {
        }
    }

    // Set Listeners
    private void setListeners() {
        changeButton.setOnClickListener(this);
        login.setOnClickListener(this);
    }




    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginBtn:
                // Replace login activity
                Intent intentLog;
                intentLog = new Intent(ChangeActivity.this, LoginActivity.class);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                startActivity(intentLog);

                break;

            case R.id.changer:

                // Change password
                checkValidation();
                break;
        }

    }

    // Check Validation before login
    private void checkValidation() {
        // Get field data
        String getCode = passer.getText().toString().trim();
        String getUuid = firmer.getText().toString().trim();

        // Check for both field is empty or not
        if (getCode.equals("") || getCode.length() == 0 || getUuid.equals("") || getUuid.length() == 0) {
            displayer("Error!","Check Fields.");

        }

        // Else do login and do your stuff
        else
            changeByServer();

    }


    private void displayer(String titler, String messager){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(false);
        dialog.setTitle(titler);
        dialog.setMessage(messager);
        dialog.setPositiveButton("Ok", null);

        final AlertDialog alert = dialog.create();
        alert.show();
    }

    private void changeByServer() {
        pDialog = new ProgressDialog(this,  R.style.AppTheme_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        showpDialog();

        final String passa = passer.getText().toString().trim();
        final String firma = firmer.getText().toString().trim();

        RestService service = RestClient.getClient(userSession).create(RestService.class);
        Call<User> call = service.userChange(userSession.getUserID(),passa,firma);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                hidepDialog();

                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        String jsonResponse = response.body().toString();
                        Toast.makeText(ChangeActivity.this, "Successful", Toast.LENGTH_SHORT).show();
                        String tokener = response.body().getSuccess().getToken();
                        String userone = response.body().getSuccess().getUuid();
                        final Session session = new Session(ChangeActivity.this);
                        session.setUserToken(tokener);
                        session.setUserID(userone);

                        RestService serviceDet = RestClient.getClient(userSession).create(RestService.class);
                        Call<User> callDetail = serviceDet.userDetails("Bearer "+tokener,userone);
                        callDetail.enqueue(new Callback<User>() {
                            @Override
                            public void onResponse(Call<User> call, Response<User> response) {
                                hidepDialog();
                                //Toast.makeText()
                                if (response.isSuccessful()) {
                                    if (response.body() != null) {
                                        String jsonResponse = response.body().getDetail().toString();
                                        saveUserInfo(jsonResponse);
                                        userSession.setUserID(response.body().getDetail().getUuid());
                                        Log.d("onDetails", response.body().getDetail().toString());
                                        FirebaseMessaging.getInstance().getToken()
                                                .addOnCompleteListener(new OnCompleteListener<String>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<String> task) {
                                                        if (!task.isSuccessful()) {
                                                            Log.w("FCM", "Fetching FCM registration token failed", task.getException());
                                                            return;
                                                        }

                                                        // Get new FCM registration token
                                                        String token = task.getResult();

                                                        // Log and toast
                                                        Log.d("FCM", token);
                                                        Toast.makeText(ChangeActivity.this, token, Toast.LENGTH_SHORT).show();


                                                        String newToken = task.getResult();
                                                        String userid = session.getUserID();
                                                        String deviceid = userDeviceID(session);

                                                        RestService service = RestClient.getClient(session).create(RestService.class);
                                                        Call<User> call = service.userSendToken("Bearer " + session.getUserToken(), userid, newToken, deviceid);
                                                        call.enqueue(new Callback<User>() {
                                                            @Override
                                                            public void onResponse(Call<User> call, Response<User> response) {

                                                                //Toast.makeText()
                                                                if (response.isSuccessful()) {
                                                                    if (response.body() != null) {
                                                                        Toast.makeText(ChangeActivity.this, "Setup Complete!", Toast.LENGTH_LONG).show();
                                                                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

                                                                        // Replace account fragment with animation
                                                                        Intent intentHome;
                                                                        intentHome = new Intent(ChangeActivity.this, MainActivity.class);
                                                                        startActivity(intentHome);
                                                                    }
                                                                } else {
                                                                    Toast.makeText(ChangeActivity.this, "Setup Incomplete!", Toast.LENGTH_LONG).show();
                                                                }
                                                            }

                                                            @Override
                                                            public void onFailure(Call<User> call, Throwable t) {
                                                                Toast.makeText(ChangeActivity.this, "Setup Error!", Toast.LENGTH_LONG).show();
                                                            }
                                                        });


                                                    }
                                                });
                                    } else {
                                        Toast.makeText(ChangeActivity.this,"Empty response",Toast.LENGTH_LONG).show();
                                    }
                                }  else {
                                    Toast.makeText(ChangeActivity.this, "Unable to fetch data", Toast.LENGTH_SHORT).show();
                                }
                            }
                            @Override
                            public void onFailure(Call<User> call, Throwable t) {
                                hidepDialog();
                                Log.d("onFailure", t.toString());
                                Toast.makeText(ChangeActivity.this, "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
                            }
                        });


                    } else {
                        Toast.makeText(ChangeActivity.this,"Empty response",Toast.LENGTH_LONG).show();
                    }
                } else if (!response.isSuccessful()){
                    Toast.makeText(ChangeActivity.this, "Invalid Username or Password ", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ChangeActivity.this, "Unable to Connect", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
                Toast.makeText(ChangeActivity.this, "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
            }


        });


    }

    public static String userDeviceID(Session session) {
        String uniqueUUID;
        if (session.getDeviceID() != null) {
            uniqueUUID = session.getDeviceID();
        } else {
            uniqueUUID = UUID.randomUUID().toString();
            session.setDeviceID(uniqueUUID);
        }
        return uniqueUUID;
    }


    public synchronized static String id(Context context) {
        if (uniqueID != null) {
            SharedPreferences sharedPrefs = context.getSharedPreferences(
                    PREF_UNIQUE_ID, Context.MODE_PRIVATE);
            uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null);
            if (uniqueID == null) {
                uniqueID = UUID.randomUUID().toString();
                Log.d("onUserUUID","uuid: "+UUID.randomUUID().toString());
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString(PREF_UNIQUE_ID, uniqueID);
                editor.commit();
            }
        }
        return uniqueID;
    }

    private void saveUserInfo(String response){
        userSession.setUserJSON(response);
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }



}
