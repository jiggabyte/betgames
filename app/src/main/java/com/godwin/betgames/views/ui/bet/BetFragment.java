package com.godwin.betgames.views.ui.bet;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.godwin.betgames.R;
import com.godwin.betgames.RestClient;
import com.godwin.betgames.RestService;
import com.godwin.betgames.Session;
import com.godwin.betgames.models.User;
import com.godwin.betgames.views.TabberAdapter;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BetFragment extends Fragment {

    TabLayout tabLayout;
    ViewPager viewPager;
    Session session;
    private ProgressDialog pDialog;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        session = new Session(getActivity());
        Context context = getContext();

        View root = inflater.inflate(R.layout.fragment_bet, container, false);

        pDialog = new ProgressDialog(getActivity(),  R.style.AppTheme_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        // stopService(new Intent(this, GameService.class));

        RestService service = RestClient.getClient(session).create(RestService.class);
        Call<User> call = service.userSetOnline("Bearer " + session.getUserToken(), session.getUserID());
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {

                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });

        tabLayout = root.findViewById(R.id.tabLayout);
        viewPager = root.findViewById(R.id.viewPager);
        // tabLayout.addTab(tabLayout.newTab().setText("My Bets"));
        tabLayout.addTab(tabLayout.newTab().setText("InBets"));
        tabLayout.addTab(tabLayout.newTab().setText("SentBets"));
        // tabLayout.addTab(tabLayout.newTab().setText("DoneBets"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final TabberAdapter adapter = new TabberAdapter(getActivity(),getChildFragmentManager(),
                tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
          //      viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
        });


        return root;
    }


    public void displayStop(String titler, String messager){

        session.setChallengeState(null);
        // session.setAppState(null);
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setCancelable(false);
        dialog.setTitle(titler);
        dialog.setMessage(messager);
        session.setPlay(null);
        session.setReqTitle(null);
        session.setReqStop(null);
        session.setReqNota(null);
        session.setUserBeta(null);
        session.setUserAcc(null);
        //  session.setBetId(null);

        dialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

                RestService service = RestClient.getClient(session).create(RestService.class);
                Call<User> call = service.userRemStop("Bearer " + session.getUserToken(), session.getUserID());
                call.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {

                        //Toast.makeText()
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                Log.d("StopConfirm", "Yeta Clicked!");
                                // Replace account fragment with
                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                FragmentTransaction ft = fragmentManager.beginTransaction();
                                ft.detach(BetFragment.this).attach(BetFragment.this).commit();
                            }
                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        Toast.makeText(getActivity(), "Request Cancelled Failed!", Toast.LENGTH_SHORT).show();
                    }
                });


            }
        });

        final AlertDialog alert = dialog.create();
        if(!getActivity().isFinishing()){
            alert.show();
        } else {

        }



    }

    public void displayer(String titler, String messager){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setCancelable(false);
        dialog.setTitle(titler);
        dialog.setMessage(messager);
        dialog.setPositiveButton("Ok", null);

        final AlertDialog alert = dialog.create();
        if(!getActivity().isFinishing()){
            alert.show();
        } else {

        }
    }


    public void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    public void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}