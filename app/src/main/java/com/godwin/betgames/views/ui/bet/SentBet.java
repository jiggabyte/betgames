package com.godwin.betgames.views.ui.bet;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.godwin.betgames.R;
import com.godwin.betgames.RestClient;
import com.godwin.betgames.RestService;
import com.godwin.betgames.Session;
import com.godwin.betgames.models.Loadbet;
import com.godwin.betgames.models.SuccessLoad;
import com.godwin.betgames.models.User;
import com.godwin.betgames.views.ui.challenger.ChallengerFragment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SentBet extends Fragment implements View.OnClickListener{

    private ProgressDialog pDialog;
    private Session userSession;
    private Integer offset;
    private Integer limit;
    private LinearLayout linearView;
    private String loadCounter;
    private Button prev;
    private Button next;
    private int prevState = 0;
    private int nextState = 0;
    private int countState = 0;
    private int totalState = 0;
    private SearchView searcher;
    private Button searchButton;
    private boolean searchPointer = false;
    private String queryGlobal;
    private ScrollView scv;

    private TextView loadEmptyTxt;
    private Button loadEmptyButton;

    private FragmentManager fragmentManager;

    private boolean loaderPointer = false;

    Button challengerButton;

    public SentBet() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        userSession = new Session(getActivity());
        Context context = getContext();

        View root = inflater.inflate(R.layout.fragment_sent, container, false);

        loadEmptyTxt = root.findViewById(R.id.noDataTXT);
        loadEmptyButton = root.findViewById(R.id.noDataButton);

        searcher = root.findViewById(R.id.searchView);
        searchButton = root.findViewById(R.id.searchButton);
        prev = root.findViewById(R.id.prevButton);
        next = root.findViewById(R.id.nextButton);
        prev.setOnClickListener(this);
        next.setOnClickListener(this);
        searchButton.setOnClickListener(this);
        linearView = root.findViewById(R.id.list_box);
        scv = root.findViewById(R.id.scroll_box);
        loadEmptyButton.setOnClickListener(this);
        if(loadCounter == null){
            loadBetFromServer(0,10);
            searchPointer = false;
        } else {
            // Do nothing
            String hunter = "last";

        }

        searcher.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // prevState = 0;
                // nextState = 10;
                // countState = 0;
                // totalState = 0;
                searchBetFromServer(0,10,query);
                queryGlobal = query;
                // searchPointer = true;

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });

        final Handler handler = new Handler();
        final int delay = 60000; //milliseconds

        handler.postDelayed(new Runnable(){
            public void run(){
                //do something


                handler.postDelayed(this, delay);
            }
        }, delay);

        return root;
    }


    public void displayConfirm(String titler, String messager, final String[] parax){
        if(userSession.getChallengeState() != null &&userSession.getChallengeState().equals("active") &&userSession.getAppState() != null &&userSession.getAppState().equals("true")){
            // Toast.makeText(getApplicationContext(), "Display No Confirm !!! "+session.getChallengeState(), Toast.LENGTH_SHORT).show();
        } else {
            if (userSession.getBackState().equals("active")) {



            } else {

               userSession.setChallengeState("active");


                AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity(), R.style.Theme_AppCompat_Light_Dialog);
                dialog.setCancelable(false);
                dialog.setTitle(titler);
                dialog.setMessage(messager+"\r\nOpponent: "+parax[2]+"\r\nBet Amount: ₦"+parax[4]);
               userSession.setPlay(null);
               userSession.setReqTitle(null);
               userSession.setReqStop(null);
               userSession.setReqNota(null);
               userSession.setUserBeta(null);
               userSession.setUserAcc(null);
               userSession.setBetId(parax[3]);

                dialog.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Log.d("ReceiverCancelled", "Nota Clicked!" + parax[1]);

                        RestService serviceStart = RestClient.getClient(userSession).create(RestService.class);
                        Call<User> callStop = serviceStart.userStop("Bearer " +userSession.getUserToken(), parax[1], parax[1], parax[2], parax[3]);
                        callStop.enqueue(new Callback<User>() {
                            @Override
                            public void onResponse(Call<User> call, Response<User> response) {

                                //Toast.makeText()
                                if (response.isSuccessful()) {
                                    if (response.body() != null) {
                                       userSession.setChallengeState(null);
                                       userSession.setAppState("true");

                                       userSession.setNotaRec(null);
                                       userSession.setNoteRec(null);
                                       userSession.setNotiRec(null);
                                       userSession.setNotoRec(null);
                                       userSession.setNotuRec(null);
                                       userSession.setNotyRec(null);
                                       userSession.setWinaRec(null);
                                       userSession.setLosaRec(null);
                                       userSession.setPlay("false");
                                       userSession.setReqNota(null);
                                       userSession.setUserBeta(null);
                                       userSession.setUserAcc(null);
                                       userSession.setBetId(null);

                                        String jsonResponse = response.body().getSuccessor();
                                        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                                        ChallengerFragment challengerFrag = new ChallengerFragment();
                                        if (challengerFrag.isAdded()) {
                                            challengerFrag.removeHandler();
                                        }

                                        Log.d("onStop", "Success: " + response.body().getSuccessor());


                                    } else {
                                       userSession.setChallengeState(null);
                                        Toast.makeText(getActivity(), "Empty response", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                   userSession.setChallengeState(null);
                                    Toast.makeText(getActivity(), "Unable to fetch data", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<User> call, Throwable t) {
                               userSession.setChallengeState(null);
                                Log.d("onFailure", t.toString());
                                Toast.makeText(getActivity(), "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
                dialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Log.d("ReceiverConfirm", "Nota Clicked!" + parax[2]);

                        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date now = new Date();
                        String strDate = sdfDate.format(now);

                        Toast.makeText(getActivity(), "Time Check: "+findDifference(parax[0],strDate)+" first :"+parax[0]+" second: "+strDate, Toast.LENGTH_SHORT).show();
                        if (findDifference(parax[0],strDate) >= 60) {

                            displayer("Game Challenge.", "The Game Challenge Invitation has Expired!");
                           userSession.setAppState(null);
                           userSession.setChallengeState(null);
                           userSession.setBetId(null);

                        } else {
                            showpDialog();

                            RestService serviceConfirm = RestClient.getClient(userSession).create(RestService.class);
                            Call<User> call = serviceConfirm.userConfirm("Bearer " +userSession.getUserToken(), parax[1], parax[2], parax[3]);
                            call.enqueue(new Callback<User>() {
                                @Override
                                public void onResponse(Call<User> call, Response<User> response) {

                                    if (response.isSuccessful()) {
                                        if (response.body() != null) {
                                            String jsonResponse = response.body().getSuccessor();

                                            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                                            ChallengerFragment challengerFrag = new ChallengerFragment();
                                            if (challengerFrag.isAdded()) {
                                                challengerFrag.removeHandler();
                                            }

                                            Log.d("onConfirm", "Success: " + response.body().getSuccessor());

                                        } else {
                                           userSession.setChallengeState(null);
                                           userSession.setAppState(null);
                                           userSession.setBetId(null);
                                            Toast.makeText(getActivity(), "Empty response", Toast.LENGTH_LONG).show();
                                        }
                                    } else {
                                       userSession.setChallengeState(null);
                                       userSession.setAppState(null);
                                       userSession.setBetId(null);
                                        hidepDialog();
                                        Toast.makeText(getActivity(), "Unable to fetch data", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<User> call, Throwable t) {
                                   userSession.setChallengeState(null);
                                   userSession.setAppState(null);
                                   userSession.setBetId(null);
                                    hidepDialog();
                                }
                            });
                        }


                    }
                });
                final AlertDialog alert = dialog.create();
                if (!getActivity().isFinishing()) {
                    alert.show();
                    playChallengeSound();
                } else {
                    // Toast.makeText(this, "Display: Error here !!!!", Toast.LENGTH_SHORT).show();
                }
            }
        }

    }


    public void playChallengeSound(){
        MediaPlayer mediaPlayer;
        mediaPlayer = MediaPlayer.create(getActivity(), R.raw.challenge);
        mediaPlayer.start();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.release();
            }
        });
    }


    // Function to print difference in
    // time start_date and end_date
    private long findDifference(String start_date,String end_date) {

        long result = 0;
        // SimpleDateFormat converts the
        // string format to date object
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        // Try Block
        try {

            // parse method is used to parse
            // the text from a string to
            // produce the date
            Date d1 = sdf.parse(start_date);
            Date d2 = sdf.parse(end_date);

            // Calucalte time difference
            // in milliseconds
            long difference_In_Time
                    = d2.getTime() - d1.getTime();

            // Calucalte time difference in
            // seconds, minutes, hours, years,
            // and days
            long difference_In_Seconds
                    = (difference_In_Time
                    / 1000);
            // % 60;

            result = difference_In_Seconds;
        }

        // Catch the Exception
        catch (ParseException e) {
            e.printStackTrace();
        }

        return result;
    }

    private void searchBetFromServer(Integer offseta, Integer limita, String query) {
        pDialog = new ProgressDialog(getContext(),  R.style.AppTheme_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Searching...");
        pDialog.setCancelable(false);
        offset = offseta;
        limit = limita;

        showpDialog();

        RestService serviceBet = RestClient.getClient(userSession).create(RestService.class);
        Call<Loadbet> call = serviceBet.userSearchBetY("Bearer "+userSession.getUserToken(),userSession.getUserID(),offset,limit,query);
        call.enqueue(new Callback<Loadbet>() {
            @Override
            public void onResponse(Call<Loadbet> call, Response<Loadbet> response) {
                hidepDialog();

                if(loaderPointer){
                    loadEmptyTxt.setVisibility(View.INVISIBLE);
                    loadEmptyButton.setVisibility(View.INVISIBLE);
                    loaderPointer = false;
                }


                //Toast.makeText()
                if (response.isSuccessful()) {
                    for(int k=0;k<linearView.getChildCount();k++){
                        linearView.getChildAt(k).setVisibility(View.INVISIBLE);
                    }
                    prev.setVisibility(View.INVISIBLE);
                    next.setVisibility(View.INVISIBLE);

                    if (response.body() != null) {

                        String responser = response.body().getSuccess().getNota().toString();
                        Integer counta = response.body().getSuccess().getCounta();
                        if(counta > 10){
                            totalState = counta;
                            if(prevState <= 0){
                                prev.setEnabled(false);
                            } else {
                                prev.setEnabled(true);
                            }
                            if(totalState - countState <= 10){
                                next.setEnabled(false);
                            } else {
                                next.setEnabled(true);
                            }
                            prev.setVisibility(View.INVISIBLE);
                            next.setVisibility(View.INVISIBLE);
                        } else if(counta == 0){

                            displayer("Search Error","Enter Search Term!");

                            prev.setVisibility(View.INVISIBLE);
                            next.setVisibility(View.INVISIBLE);

                            loadCounter = null;
                            loadBetFromServer(0,10);
                            searchPointer = false;
                        }
                        // Toast.makeText(getActivity(), "Responser "+responser, Toast.LENGTH_SHORT).show();
                        //  Log.d("onSuccessBet", "Test "+response.body().getSuccess().getNota());

                        ArrayList<SuccessLoad> responseMsg = response.body().getSuccess().getNota();
                        for(int i = 0;i < responseMsg.size();i++){
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            Gson gson = gsonBuilder.create();
                            String objecto = gson.toJson(responseMsg.get(i));
                            try {
                                JSONObject objector = new JSONObject(objecto);
                                final String responseId = objector.getString("id");
                                final String responseName = objector.getString("uuid");
                                String responseOpp = objector.getString("acceptor");
                                if(responseOpp == null || responseOpp.equals("")){
                                    responseOpp = "General";
                                }
                                String responseAmt;
                                if(Integer.parseInt(objector.getString("amt")) == 0){
                                    responseAmt = "Free Game!";
                                } else {
                                    responseAmt = "₦"+objector.getString("amt");
                                }
                                String responseGame = objector.getString("game");
                                linearView.getChildAt(i).setVisibility(View.VISIBLE);
                                final CardView cardia = (CardView) linearView.getChildAt(i);
                                TextView textName = (TextView) cardia.getChildAt(0);
                                TextView textGame = (TextView) cardia.getChildAt(1);
                                TextView textAmt = (TextView) cardia.getChildAt(2);
                                Button challenButton = (Button) cardia.getChildAt(3);
                                challengerButton = challenButton;
                                textName.setText(responseOpp);
                                textGame.setText(responseGame);
                                textAmt.setText(responseAmt);
                                Toast.makeText(getActivity(), ""+responseOpp, Toast.LENGTH_SHORT).show();
                                challenButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        String usernome = userSession.getUserID();
                                        String userbeta = responseName;
                                        String betId = responseId;

                                        showpDialog();

                                        RestService service = RestClient.getClient(userSession).create(RestService.class);
                                        Call<User> call = service.userCancel("Bearer "+userSession.getUserToken(),usernome,userbeta,betId);
                                        // Call<User> call = service.userAccept("Bearer "+userSession.getUserToken(),usernome,userbeta,betId);
                                        call.enqueue(new Callback<User>() {
                                            @Override
                                            public void onResponse(Call<User> call, Response<User> response) {

                                                //Toast.makeText()
                                                if (response.isSuccessful()) {
                                                    if (response.body() != null) {
                                                        // String jsonResponse = response.body().toString();
                                                        //saveInfo(jsonResponse);
                                                        Toast.makeText(getActivity(), "Challenge Cancelled!", Toast.LENGTH_SHORT).show();
                                                        Log.d("Tokeniser: ", response.body().getSuccessor());
                                                        cardia.setVisibility(View.GONE);

                                                        hidepDialog();


                                                    } else {
                                                        hidepDialog();
                                                        Toast.makeText(getActivity(),"Empty response",Toast.LENGTH_LONG).show();
                                                    }

                                                } else {
                                                    hidepDialog();
                                                    Toast.makeText(getActivity(), "Challenge Not Cancelled!", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                            @Override
                                            public void onFailure(Call<User> call, Throwable t) {
                                                hidepDialog();
                                                Log.d("onFailure", t.toString());
                                                Toast.makeText(getActivity(), "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
                                            }
                                        });

                                    }
                                });
                            } catch (JSONException error){
                                error.printStackTrace();
                            }


                        }

                        // Log.d("onStringBet", "Testor "+responseMsg);
                        loadCounter = "on";

                    } else {
                        Toast.makeText(getActivity(),"Empty response",Toast.LENGTH_LONG).show();
                    }
                } else {
                    Log.d("onFailureBet", response.toString());
                    Toast.makeText(getActivity(), "Load Error!", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Loadbet> call, Throwable t) {
                hidepDialog();
                loadEmptyTxt.setVisibility(View.VISIBLE);
                loadEmptyButton.setVisibility(View.VISIBLE);
                loaderPointer = true;

                Log.d("onFailureBet", t.toString());
                Toast.makeText(getActivity(), "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
            }
        });
        scv.fullScroll(ScrollView.FOCUS_UP);
    }

    private void loadBetFromServer(Integer offseta, Integer limita) {
        pDialog = new ProgressDialog(getContext(),  R.style.AppTheme_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        offset = offseta;
        limit = limita;

        showpDialog();

        RestService serviceBet = RestClient.getClient(userSession).create(RestService.class);
        Call<Loadbet> call = serviceBet.userLoadBetY("Bearer "+userSession.getUserToken(),userSession.getUserID(),offset,limit);
        call.enqueue(new Callback<Loadbet>() {
            @Override
            public void onResponse(Call<Loadbet> call, Response<Loadbet> response) {
                hidepDialog();

                if(loaderPointer){
                    loadEmptyTxt.setVisibility(View.INVISIBLE);
                    loadEmptyButton.setVisibility(View.INVISIBLE);
                    loaderPointer = false;
                }


                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if(response.body().getSuccess().getCounta() == 0){
                            loadEmptyTxt.setVisibility(View.VISIBLE);
                            loadEmptyButton.setVisibility(View.VISIBLE);
                        }
                        for(int k=0;k<linearView.getChildCount();k++){
                            linearView.getChildAt(k).setVisibility(View.INVISIBLE);
                        }
                        prev.setVisibility(View.INVISIBLE);
                        next.setVisibility(View.INVISIBLE);
                        String responser = response.body().getSuccess().getNota().toString();
                        Integer counta = response.body().getSuccess().getCounta();
                        if(counta > 10){
                            totalState = counta;
                            if(prevState <= 0){
                                prev.setEnabled(false);
                            } else {
                                prev.setEnabled(true);
                            }
                            if(totalState - countState <= 10){
                                next.setEnabled(false);
                            } else {
                                next.setEnabled(true);
                            }
                            prev.setVisibility(View.VISIBLE);
                            next.setVisibility(View.VISIBLE);
                        }
                        // Toast.makeText(getActivity(), "Responser "+responser, Toast.LENGTH_SHORT).show();
                        //  Log.d("onSuccessBet", "Test "+response.body().getSuccess().getNota());

                        ArrayList<SuccessLoad> responseMsg = response.body().getSuccess().getNota();
                        for(int i = 0;i < responseMsg.size();i++){
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            Gson gson = gsonBuilder.create();
                            String objecto = gson.toJson(responseMsg.get(i));
                            try {
                                JSONObject objector = new JSONObject(objecto);
                                final String responseId = objector.getString("id");
                                final String responseName = objector.getString("uuid");
                                String responseOpp = objector.getString("acceptor");
                                if(responseOpp.equals("")){
                                    responseOpp = "General";
                                }
                                String responseAmt;
                                if(Integer.parseInt(objector.getString("amt")) == 0){
                                    responseAmt = "Free Game!";
                                } else {
                                    responseAmt = "₦"+objector.getString("amt");
                                }
                                String responseGame = objector.getString("game");
                                linearView.getChildAt(i).setVisibility(View.VISIBLE);
                                final CardView cardia = (CardView) linearView.getChildAt(i);
                                TextView textName = (TextView) cardia.getChildAt(0);
                                TextView textGame = (TextView) cardia.getChildAt(1);
                                TextView textAmt = (TextView) cardia.getChildAt(2);
                                Button challenButton = (Button) cardia.getChildAt(3);
                                challengerButton = challenButton;
                                textName.setText(responseOpp);
                                textGame.setText(responseGame);
                                textAmt.setText(responseAmt);
                                challenButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        String usernome = userSession.getUserID();
                                        String userbeta = responseName;
                                        String betId = responseId;

                                        showpDialog();

                                        RestService service = RestClient.getClient(userSession).create(RestService.class);
                                        Call<User> call = service.userCancel("Bearer "+userSession.getUserToken(),usernome,userbeta,betId);
                                        call.enqueue(new Callback<User>() {
                                            @Override
                                            public void onResponse(Call<User> call, Response<User> response) {

                                                //Toast.makeText()
                                                if (response.isSuccessful()) {
                                                    if (response.body() != null) {
                                                        // String jsonResponse = response.body().toString();
                                                        //saveInfo(jsonResponse);
                                                        Toast.makeText(getActivity(), "Challenge Cancelled!", Toast.LENGTH_SHORT).show();
                                                        Log.d("Tokeniser: ", response.body().getSuccessor());
                                                        Log.d("onConfirm", "Success: "+response.body().getSuccessor());
                                                        cardia.setVisibility(View.GONE);

                                                        hidepDialog();

                                                    } else {
                                                        hidepDialog();
                                                        Toast.makeText(getActivity(),"Empty response",Toast.LENGTH_LONG).show();
                                                    }
                                                } else {
                                                    hidepDialog();
                                                    Toast.makeText(getActivity(), "Challenge Not Cancelled!", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                            @Override
                                            public void onFailure(Call<User> call, Throwable t) {
                                                hidepDialog();
                                                Log.d("onFailure", t.toString());
                                                Toast.makeText(getActivity(), "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
                                            }
                                        });

                                    }
                                });
                            } catch (JSONException error){
                                error.printStackTrace();
                                Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                            }


                        }

                        // Log.d("onStringBet", "Testor "+responseMsg);
                        loadCounter = "on";

                    } else {
                        hidepDialog();
                        Toast.makeText(getActivity(),"Empty response",Toast.LENGTH_LONG).show();
                    }
                } else {
                    hidepDialog();
                    Log.d("onFailureBet", response.toString());
                    Toast.makeText(getActivity(), "Load Error!", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Loadbet> call, Throwable t) {
                hidepDialog();

                loadEmptyTxt.setVisibility(View.VISIBLE);
                loadEmptyButton.setVisibility(View.VISIBLE);
                loaderPointer = true;

                Log.d("onFailureBet", t.toString());
                Toast.makeText(getActivity(), "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
            }
        });
        scv.fullScroll(ScrollView.FOCUS_UP);
    }

    private void displayer(String titler, String messager){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setCancelable(false);
        dialog.setTitle(titler);
        dialog.setMessage(messager);
        dialog.setPositiveButton("Ok", null);

        final AlertDialog alert = dialog.create();
        alert.show();
    }

    public void displayStop(String titler, String messager, Context context){

        userSession.setChallengeState(null);
        // session.setAppState(null);
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setCancelable(false);
        dialog.setTitle(titler);
        dialog.setMessage(messager);
        userSession.setPlay(null);
        userSession.setReqTitle(null);
        userSession.setReqStop(null);
        userSession.setReqNota(null);
        userSession.setUserBeta(null);
        userSession.setUserAcc(null);
        //  session.setBetId(null);

        dialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

                RestService service = RestClient.getClient(userSession).create(RestService.class);
                Call<User> call = service.userRemStop("Bearer " + userSession.getUserToken(), userSession.getUserID());
                call.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {

                        //Toast.makeText()
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                Log.d("StopConfirm", "Yeta Clicked!");
                                // Replace account fragment with
                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                FragmentTransaction ft = fragmentManager.beginTransaction();
                                ft.detach(SentBet.this).attach(SentBet.this).commit();
                            }
                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        Toast.makeText(getActivity(), "Request Cancelled Failed!", Toast.LENGTH_SHORT).show();
                    }
                });


            }
        });

        final AlertDialog alert = dialog.create();
        alert.show();


    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void loadPrev(){
        if(prevState >= 10) {
            prevState -= 10;
            countState -= 10;
            if(searchPointer){
                searchBetFromServer(countState,10,queryGlobal);
            } else {
                loadBetFromServer(countState,10);
            }

            Log.d("prevState", " "+prevState);
            Log.d("countState", " "+countState);
            Log.d("nextState", " "+nextState);
            Log.d("totalState", " "+totalState);

        } else {
            if(searchPointer){
                searchBetFromServer(countState, 10,queryGlobal);
            } else {
                loadBetFromServer(countState, prevState);
            }

            prevState = 0;
            countState = 0;

            Log.d("prevState", " "+prevState);
            Log.d("countState", " "+countState);
            Log.d("nextState", " "+nextState);
            Log.d("totalState", " "+totalState);

        }

    }

    private void loadMore(){
        if(totalState - countState >= 10){
            prevState += 10;
            countState += 10;
            if(searchPointer){
                searchBetFromServer(countState, 10,queryGlobal);
            } else {
                loadBetFromServer(countState,10);
            }

            Log.d("prevState", " "+prevState);
            Log.d("countState", " "+countState);
            Log.d("nextState", " "+nextState);
            Log.d("totalState", " "+totalState);


        } else {
            prevState = prevState + (totalState - countState);
            countState = countState + (totalState - countState);
            if(searchPointer) {
                searchBetFromServer(countState, 10, queryGlobal);
            } else {
                loadBetFromServer(countState, 10);
            }

            Log.d("prevState", " "+prevState);
            Log.d("countState", " "+countState);
            Log.d("nextState", " "+nextState);
            Log.d("totalState", " "+totalState);

        }

    }



    @Override
    public void setMenuVisibility(boolean isvisible) {
        super.setMenuVisibility(isvisible);
        if (isvisible){
            Log.d("Viewpager", "fragment is visible ");

        } else {
            Log.d("Viewpager", "fragment is not visible ");
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.prevButton:
                loadPrev();
                break;

            case R.id.nextButton:
                loadMore();
                break;

            case R.id.searchButton:

                if(searcher.getQuery().toString().equals("")){

                    displayer("Search","No Match Found!");

                } else {
                    // prevState = 0;
                    //  nextState = 10;
                    //  countState = 0;
                    //  totalState = 0;
                    searchBetFromServer(0,10,searcher.getQuery().toString());
                    //searchPointer = true;
                }
                break;

            case R.id.noDataButton:

                if(userSession.getUserToken() != null) {
                    // Replace fragment with
                    FragmentManager fragmentManager = getParentFragmentManager();
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.detach(SentBet.this).attach(SentBet.this).commit();
                    Toast.makeText(getActivity(), "Please Come Back Later!", Toast.LENGTH_SHORT).show();

                    break;
                }
        }
    }
}