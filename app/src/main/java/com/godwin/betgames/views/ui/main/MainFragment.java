package com.godwin.betgames.views.ui.main;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.godwin.betgames.R;
import com.godwin.betgames.RestClient;
import com.godwin.betgames.RestService;
import com.godwin.betgames.Session;
import com.godwin.betgames.Utils;
import com.godwin.betgames.models.User;
import com.godwin.betgames.views.BetgamesDBHelper;
import com.godwin.betgames.views.LoginActivity;
import com.godwin.betgames.views.ui.bet.BetFragment;
import com.godwin.betgames.views.ui.challenger.ChallengerFragment;
import com.godwin.betgames.views.ui.contact.ContactFragment;
import com.godwin.betgames.views.ui.faq.FaqFragment;
import com.godwin.betgames.views.ui.home.HomeFragment;
import com.godwin.betgames.views.ui.wallet.WalletFragment;
import com.godwin.betgames.views.ui.web.Web2Fragment;
import com.godwin.betgames.views.ui.web.WebFragment;
import com.google.firebase.auth.FirebaseAuth;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainFragment extends Fragment implements View.OnClickListener {


    private ImageView whot2;
    private ImageView whot;
    private Button backGamer;
    Context context;
    Spinner spinner;
    private EditText betAmt;
    private String betGame;
    private Button betBtn;
    Spinner spinnerx;
    private EditText betAmtx;
    private String betGamex;
    private Button betBtnx;
    private ProgressDialog pDialog;
    private Session session;
    private TextView challenger_text;
    private FragmentManager fragmentManager;
    private SearchView searcher;
    private Button searchButton;
    private TextView user_nom_text;
    private TextView xcloser;
    private EditText challengeOne;
    private Button challenger;
    private LinearLayout linearView;
    private String loadCounter;
    private CardView card;
    private String queryGlobal;

    private LinearLayout deposit;
    private LinearLayout withdraw;

    private LinearLayout placeBet;
    private LinearLayout myBets;
    private LinearLayout challengers;
    private LinearLayout wallet;
    private LinearLayout help;

    private LinearLayout share;
    private LinearLayout contact;

    private BetgamesDBHelper betgamedb;

    private TextView usernomer;
    private TextView balance;
    private TextView credit;

    private ImageView refresher;

    private TextView logout;

    private ImageView whatsButton;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_main, container, false);

        pDialog = new ProgressDialog(getActivity(),  R.style.AppTheme_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        // getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        logout = root.findViewById(R.id.action_logout);
        SpannableString content = new SpannableString("Logout");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        logout.setText(content);
        session = new Session(getActivity());

        usernomer = root.findViewById(R.id.user_nom);
        balance = root.findViewById(R.id.balance_amt);
        credit = root.findViewById(R.id.credit_amt);

        refresher = root.findViewById(R.id.refresh);

        whatsButton = root.findViewById(R.id.whatsapper);

        if(session.getUserID() != null){

            usernomer.setText(session.getUserID());

        }

        deposit = root.findViewById(R.id.deposit);
        withdraw = root.findViewById(R.id.withdraw);

        placeBet = root.findViewById(R.id.bets);
        myBets = root.findViewById(R.id.my_bets);
        challengers = root.findViewById(R.id.challengers);
        wallet = root.findViewById(R.id.wallet);
        help = root.findViewById(R.id.help);

        share = root.findViewById(R.id.share);
        contact = root.findViewById(R.id.contact);

        deposit.setOnClickListener(this);
        withdraw.setOnClickListener(this);

        placeBet.setOnClickListener(this);
        myBets.setOnClickListener(this);
        challengers.setOnClickListener(this);
        wallet.setOnClickListener(this);
        help.setOnClickListener(this);

        share.setOnClickListener(this);
        contact.setOnClickListener(this);

        logout.setOnClickListener(this);
        withdraw.setOnClickListener(this);

        RestService service = RestClient.getClient(session).create(RestService.class);
        Call<User> call = service.userBalance("Bearer " + session.getUserToken(), session.getUserID());
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        credit.setText("₦"+response.body().getSuccess().getCredit());
                        balance.setText("₦"+response.body().getSuccess().getBalance());
                    }
                } else {
                  //  Toast.makeText(getActivity(), "Failure!", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(getActivity(), "Network Error!", Toast.LENGTH_LONG).show();


            }
        });

      //  refresher.setVisibility(View.VISIBLE);
        refresher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Replace account fragment with animation
                RestService service = RestClient.getClient(session).create(RestService.class);
                Call<User> call = service.userBalance("Bearer " + session.getUserToken(), session.getUserID());
                call.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {

                        //Toast.makeText()
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                credit.setText("₦"+response.body().getSuccess().getCredit());
                                balance.setText("₦"+response.body().getSuccess().getBalance());
                            }
                        } else {
                            Toast.makeText(getActivity(), "Failure!", Toast.LENGTH_LONG).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        Toast.makeText(getActivity(), "Network Error!", Toast.LENGTH_LONG).show();


                    }
                });
            }
        });

        //  Set whatsapp action here;
        whatsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = "https://api.whatsapp.com/send?phone=+2348115219777";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });

        return root;
    }


    public void logoutByServer() {

        if(!getActivity().isFinishing())
        {
            showpDialog();
        } else {

        }

        final String uuida = session.getUserID();

        RestService service = RestClient.getClient(session).create(RestService.class);
        retrofit2.Call<User> call = service.userLogout(session.getUserToken(),uuida);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(retrofit2.Call<User> call, Response<User> response) {
                hidepDialog();

                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Toast.makeText(getActivity(), "Logged out", Toast.LENGTH_SHORT).show();
                        Session session = new Session(getActivity());

                        // logout fragment
                        session.setUserToken(null);
                        session.setUserID(null);
                        session.setUserJSON(null);

                        FirebaseAuth.getInstance().signOut();

                        // Replace login activity
                        Intent intentLogin;
                        intentLogin = new Intent(getActivity(), LoginActivity.class);
                        intentLogin.putExtra("stopper","false");
                        startActivity(intentLogin);
                        getActivity().finish();

                        // Replace account fragment with animation
                        Intent intentLog;
                        intentLog = new Intent(getActivity(), LoginActivity.class);
                        startActivity(intentLog);
                        getActivity().finish();
                    } else {
                        Toast.makeText(getActivity(),"Empty response",Toast.LENGTH_LONG).show();
                    }
                } else if (!response.isSuccessful()){
                    Toast.makeText(getActivity(), "Logout Error!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Unable to Logout", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });


    }


    public void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    public void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }




    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.bets:
                if(session.getUserToken() != null){
                    // Replace account fragment with animation
                    fragmentManager = getParentFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction
                            .addToBackStack(null)
                            //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                            .replace(R.id.home_host, new HomeFragment(),
                                    Utils.Home_Fragment).commit();
                    // findViewById(R.id.home_host).setClickable(false);
                    // findViewById(R.id.nav_home).setClickable(true);

                } else {
                    // Replace login activity
                    Intent intentLogin;
                    intentLogin = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intentLogin);
                    getActivity().finish();

                }
                break;

            case R.id.my_bets:
                if(session.getUserToken() != null){
                    // Replace account fragment with animation
                    fragmentManager = getParentFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction
                            .addToBackStack(null)
                            //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                            .replace(R.id.home_host, new BetFragment(),
                                    Utils.Bet_Fragment).commit();
                    // findViewById(R.id.home_host).setClickable(false);
                    // findViewById(R.id.nav_home).setClickable(true);

                } else {
                    // Replace login activity
                    Intent intentLogin;
                    intentLogin = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intentLogin);
                    getActivity().finish();

                }
                break;

            case R.id.deposit:
                if(session.getUserToken() != null){
                    // Replace account fragment with animation
                    fragmentManager = getParentFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction
                            .addToBackStack(null)
                            //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                            .replace(R.id.home_host, new WebFragment(),
                                    Utils.Web_Fragment).commit();
                    // findViewById(R.id.home_host).setClickable(false);
                    // findViewById(R.id.nav_home).setClickable(true);

                } else {
                    // Replace login activity
                    Intent intentLogin;
                    intentLogin = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intentLogin);
                    getActivity().finish();

                }
                break;

            case R.id.withdraw:
                if(session.getUserToken() != null){
                    // Replace account fragment with animation
                    fragmentManager = getParentFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction
                            .addToBackStack(null)
                            //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                            .replace(R.id.home_host, new Web2Fragment(),
                                    Utils.Web2_Fragment).commit();
                    // findViewById(R.id.home_host).setClickable(false);
                    // findViewById(R.id.nav_home).setClickable(true);

                } else {
                    // Replace login activity
                    Intent intentLogin;
                    intentLogin = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intentLogin);
                    getActivity().finish();

                }
                break;
            case R.id.share:
                if(session.getUserToken() != null){

                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.godwin.betgames&referrer="+session.getUserID());
                        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Download this game and get rewarded for your crave!");
                        startActivity(Intent.createChooser(intent, "Share"));


                } else {
                    // Replace login activity
                    Intent intentLogin;
                    intentLogin = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intentLogin);
                    getActivity().finish();

                }
                break;
            case R.id.challengers:
                if(session.getUserToken() != null){
                    // Replace account fragment with animation
                    fragmentManager = getParentFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction
                            .addToBackStack(null)
                            //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                            .replace(R.id.home_host, new ChallengerFragment(),
                                    Utils.Challenger_Fragment).commit();
                   // findViewById(R.id.home_host).setClickable(false);
                   // findViewById(R.id.challengers).setClickable(true);

                } else {
                    // Replace login activity
                    Intent intentLogin;
                    intentLogin = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intentLogin);
                    getActivity().finish();

                }
                break;
            case R.id.wallet:
                if(session.getUserToken() != null){
                    // Replace account fragment with animation
                    fragmentManager = getParentFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction
                            .addToBackStack(null)
                            //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                            .replace(R.id.home_host, new WalletFragment(),
                                    Utils.Wallet_Fragment).commit();


                } else {
                    // Replace login activity
                    Intent intentLogin;
                    intentLogin = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intentLogin);
                    getActivity().finish();

                }
                break;
            case R.id.help:
                if(session.getUserToken() != null){
                    // Replace account fragment with animation
                    fragmentManager = getParentFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction
                            .addToBackStack(null)
                            //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                            .replace(R.id.home_host, new FaqFragment(),
                                    Utils.Faq_Fragment).commit();


                } else {
                    // Replace login activity
                    Intent intentLogin;
                    intentLogin = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intentLogin);
                    getActivity().finish();

                }
                break;
            case R.id.contact:
                if(session.getUserToken() != null){
                    // Replace account fragment with animation
                    fragmentManager = getParentFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction
                            .addToBackStack(null)
                            //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                            .replace(R.id.home_host, new ContactFragment(),
                                    Utils.Contact_Fragment).commit();


                } else {
                    // Replace login activity
                    Intent intentLogin;
                    intentLogin = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intentLogin);
                    getActivity().finish();

                }
                break;
            case R.id.action_logout:

                logoutByServer();
                Toast.makeText(getActivity(), "Logging Out ...", Toast.LENGTH_SHORT).show();
                break;
            default:
                // return super.onOptionsItemSelected(item);

        }
    }
}