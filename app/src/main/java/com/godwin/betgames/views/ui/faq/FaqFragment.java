package com.godwin.betgames.views.ui.faq;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.godwin.betgames.R;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

public class FaqFragment extends Fragment {


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_faq, container, false);

        return root;
    }
}