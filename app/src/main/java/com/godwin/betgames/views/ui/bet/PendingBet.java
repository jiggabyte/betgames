package com.godwin.betgames.views.ui.bet;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.godwin.betgames.R;
import com.godwin.betgames.RestClient;
import com.godwin.betgames.RestService;
import com.godwin.betgames.Session;
import com.godwin.betgames.models.Loadbet;
import com.godwin.betgames.models.Nota;
import com.godwin.betgames.models.SuccessLoad;
import com.godwin.betgames.models.User;
import com.godwin.betgames.views.ui.challenger.ChallengerFragment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PendingBet extends Fragment implements View.OnClickListener{

    private ProgressDialog pDialog;
    private Session userSession;
    private Session session;
    private Integer offset;
    private Integer limit;
    private LinearLayout linearView;
    private String loadCounter;
    private Button prev;
    private Button next;
    private int prevState = 0;
    private int nextState = 0;
    private int countState = 0;
    private int totalState = 0;
    private SearchView searcher;
    private Button searchButton;
    private boolean searchPointer = false;
    private String queryGlobal;
    private ScrollView scv;

    private boolean loaderPointer = false;
    private TextView loadEmptyTxt;
    private Button loadEmptyButton;

    private FragmentManager fragmentManager;

    Button challengerButton;
    Context context;

    Handler handler;
    Runnable runnablec;

    ValueEventListener startxEventListener;

    public PendingBet() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        userSession = new Session(getActivity());
        session = new Session(getActivity());
        context = getContext();

        handler = new Handler();

        View root = inflater.inflate(R.layout.fragment_pending, container, false);

        loadEmptyTxt = root.findViewById(R.id.noDataTXT);
        loadEmptyButton = root.findViewById(R.id.noDataButton);

        searcher = root.findViewById(R.id.searchView);
        searchButton = root.findViewById(R.id.searchButton);
        prev = root.findViewById(R.id.prevButton);
        next = root.findViewById(R.id.nextButton);
        prev.setOnClickListener(this);
        next.setOnClickListener(this);
        searchButton.setOnClickListener(this);
        linearView = root.findViewById(R.id.list_box);
        scv = root.findViewById(R.id.scroll_box);
        loadEmptyButton.setOnClickListener(this);
        if(loadCounter == null){
            loadBetFromServer(0,10);
            searchPointer = false;
        } else {
            // Do nothing
            String hunter = "last";

        }

        searcher.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // prevState = 0;
                // nextState = 10;
                // countState = 0;
                // totalState = 0;
                Context context = getContext();
                searchBetFromServer(0,10,query,context);
                queryGlobal = query;
                // searchPointer = true;

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });

        final Handler handler = new Handler();
        final int delay = 80000; //milliseconds

        handler.postDelayed(new Runnable(){
            public void run(){
                //do something


                handler.postDelayed(this, delay);
            }
        }, delay);


        return root;
    }

    public void displayConfirm(String titler, String messager, final String[] parax){
        if(session.getChallengeState() != null && session.getChallengeState().equals("active") && session.getAppState() != null && session.getAppState().equals("true")){
            // Toast.makeText(getApplicationContext(), "Display No Confirm !!! "+session.getChallengeState(), Toast.LENGTH_SHORT).show();
        } else {
            if (session.getBackState().equals("active")) {



            } else {

                session.setChallengeState("active");


                AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity(), R.style.Theme_AppCompat_Light_Dialog);
                dialog.setCancelable(false);
                dialog.setTitle(titler);
                dialog.setMessage(messager+"\r\nOpponent: "+parax[2]+"\r\nBet Amount: ₦"+parax[4]);
                session.setPlay(null);
                session.setReqTitle(null);
                session.setReqStop(null);
                session.setReqNota(null);
                session.setUserBeta(null);
                session.setUserAcc(null);
                session.setBetId(parax[3]);

                dialog.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Log.d("ReceiverCancelled", "Nota Clicked!" + parax[1]);

                        RestService serviceStart = RestClient.getClient(session).create(RestService.class);
                        Call<User> callStop = serviceStart.userStop("Bearer " + session.getUserToken(), parax[1], parax[1], parax[2], parax[3]);
                        callStop.enqueue(new Callback<User>() {
                            @Override
                            public void onResponse(Call<User> call, Response<User> response) {

                                //Toast.makeText()
                                if (response.isSuccessful()) {
                                    if (response.body() != null) {
                                        session.setChallengeState(null);
                                        session.setAppState("true");

                                        session.setNotaRec(null);
                                        session.setNoteRec(null);
                                        session.setNotiRec(null);
                                        session.setNotoRec(null);
                                        session.setNotuRec(null);
                                        session.setNotyRec(null);
                                        session.setWinaRec(null);
                                        session.setLosaRec(null);
                                        session.setPlay("false");
                                        session.setReqNota(null);
                                        session.setUserBeta(null);
                                        session.setUserAcc(null);
                                        session.setBetId(null);

                                        String jsonResponse = response.body().getSuccessor();
                                        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                                        ChallengerFragment challengerFrag = new ChallengerFragment();
                                        if (challengerFrag.isAdded()) {
                                            challengerFrag.removeHandler();
                                        }

                                        Log.d("onStop", "Success: " + response.body().getSuccessor());


                                    } else {
                                        session.setChallengeState(null);
                                        Toast.makeText(getActivity(), "Empty response", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    session.setChallengeState(null);
                                    Toast.makeText(getActivity(), "Unable to fetch data", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<User> call, Throwable t) {
                                session.setChallengeState(null);
                                Log.d("onFailure", t.toString());
                                Toast.makeText(getActivity(), "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
                dialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Log.d("ReceiverConfirm", "Nota Clicked!" + parax[2]);

                        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date now = new Date();
                        String strDate = sdfDate.format(now);

                        Toast.makeText(getActivity(), "Time Check: "+findDifference(parax[0],strDate)+" first :"+parax[0]+" second: "+strDate, Toast.LENGTH_SHORT).show();
                        if (findDifference(parax[0],strDate) >= 60) {
                            displayer("Game Challenge.", "The Game Challenge Invitation has Expired!", context);
                            session.setAppState(null);
                            session.setChallengeState(null);
                            session.setBetId(null);

                        } else {
                            if (!getActivity().isFinishing()) {
                                 showpDialog();
                            }

                            RestService serviceConfirm = RestClient.getClient(session).create(RestService.class);
                            Call<User> call = serviceConfirm.userConfirm("Bearer " + session.getUserToken(), parax[1], parax[2], parax[3]);
                            call.enqueue(new Callback<User>() {
                                @Override
                                public void onResponse(Call<User> call, Response<User> response) {

                                    if (response.isSuccessful()) {
                                        if (response.body() != null) {
                                            String jsonResponse = response.body().getSuccessor();

                                            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                                            ChallengerFragment challengerFrag = new ChallengerFragment();
                                            if (challengerFrag.isAdded()) {
                                                challengerFrag.removeHandler();
                                            }

                                            Log.d("onConfirm", "Success: " + response.body().getSuccessor());

                                        } else {
                                            session.setChallengeState(null);
                                            session.setAppState(null);
                                            session.setBetId(null);
                                            Toast.makeText(getActivity(), "Empty response", Toast.LENGTH_LONG).show();
                                        }
                                    } else {
                                        session.setChallengeState(null);
                                        session.setAppState(null);
                                        session.setBetId(null);
                                        if (!getActivity().isFinishing()) {
         hidepDialog();
}
                                        Toast.makeText(getActivity(), "Unable to fetch data", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<User> call, Throwable t) {
                                    session.setChallengeState(null);
                                    session.setAppState(null);
                                    session.setBetId(null);
                                    if (!getActivity().isFinishing()) {
         hidepDialog();
}
                                }
                            });
                        }


                    }
                });
                final AlertDialog alert = dialog.create();
                if (!getActivity().isFinishing()) {
                    alert.show();
                    playChallengeSound();
                } else {
                    // Toast.makeText(this, "Display: Error here !!!!", Toast.LENGTH_SHORT).show();
                }
            }
        }

    }


    public void playChallengeSound(){
        MediaPlayer mediaPlayer;
        mediaPlayer = MediaPlayer.create(getActivity(), R.raw.challenge);
        mediaPlayer.start();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.release();
            }
        });
    }

    // Function to print difference in
    // time start_date and end_date
    private long findDifference(String start_date,String end_date) {

        long result = 0;
        // SimpleDateFormat converts the
        // string format to date object
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        // Try Block
        try {

            // parse method is used to parse
            // the text from a string to
            // produce the date
            Date d1 = sdf.parse(start_date);
            Date d2 = sdf.parse(end_date);

            // Calucalte time difference
            // in milliseconds
            long difference_In_Time
                    = d2.getTime() - d1.getTime();

            // Calucalte time difference in
            // seconds, minutes, hours, years,
            // and days
            long difference_In_Seconds
                    = (difference_In_Time
                    / 1000);
            // % 60;

            result = difference_In_Seconds;
        }

        // Catch the Exception
        catch (ParseException e) {
            e.printStackTrace();
        }

        return result;
    }

    private void searchBetFromServer(Integer offseta, Integer limita, String query, final Context context) {
        pDialog = new ProgressDialog(getContext(),  R.style.AppTheme_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Searching...");
        pDialog.setCancelable(false);
        offset = offseta;
        limit = limita;

        if (!getActivity().isFinishing()) {
         showpDialog();
}

        RestService serviceBet = RestClient.getClient(userSession).create(RestService.class);
        Call<Loadbet> call = serviceBet.userSearchBetX("Bearer "+userSession.getUserToken(),userSession.getUserID(),offset,limit,query);
        call.enqueue(new Callback<Loadbet>() {
            @Override
            public void onResponse(Call<Loadbet> call, Response<Loadbet> response) {
                if (!getActivity().isFinishing()) {
         hidepDialog();
}

                if(loaderPointer){
                    loadEmptyTxt.setVisibility(View.INVISIBLE);
                    loadEmptyButton.setVisibility(View.INVISIBLE);
                    loaderPointer = false;
                }

                //Toast.makeText()
                if (response.isSuccessful()) {
                    for(int k=0;k<linearView.getChildCount();k++){
                        linearView.getChildAt(k).setVisibility(View.INVISIBLE);
                    }
                    prev.setVisibility(View.INVISIBLE);
                    next.setVisibility(View.INVISIBLE);

                    if (response.body() != null) {



                        String responser = response.body().getSuccess().getNota().toString();
                        Integer counta = response.body().getSuccess().getCounta();
                        if(counta > 10){
                            totalState = counta;
                            if(prevState <= 0){
                                prev.setEnabled(false);
                            } else {
                                prev.setEnabled(true);
                            }
                            if(totalState - countState <= 10){
                                next.setEnabled(false);
                            } else {
                                next.setEnabled(true);
                            }
                            prev.setVisibility(View.INVISIBLE);
                            next.setVisibility(View.INVISIBLE);
                        } else if(counta == 0){


                            displayer("Search","No Match Found!", context);

                            prev.setVisibility(View.INVISIBLE);
                            next.setVisibility(View.INVISIBLE);

                            loadCounter = null;
                            loadBetFromServer(0,10);
                            searchPointer = false;
                        }
                        // Toast.makeText(getActivity(), "Responser "+responser, Toast.LENGTH_SHORT).show();
                        //  Log.d("onSuccessBet", "Test "+response.body().getSuccess().getNota());

                        ArrayList<SuccessLoad> responseMsg = response.body().getSuccess().getNota();
                        for(int i = 0;i < responseMsg.size();i++){
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            Gson gson = gsonBuilder.create();
                            String objecto = gson.toJson(responseMsg.get(i));
                            try {
                                JSONObject objector = new JSONObject(objecto);
                                final String responseId = objector.getString("id");
                                final String responseName = objector.getString("uuid");
                                String responseAmt;
                                if(Integer.parseInt(objector.getString("amt")) == 0){
                                    responseAmt = "Free Game!";
                                } else {
                                    responseAmt = "₦"+objector.getString("amt");
                                }
                                String responseGame = objector.getString("game");
                                linearView.getChildAt(i).setVisibility(View.VISIBLE);
                                CardView cardia = (CardView) linearView.getChildAt(i);
                                TextView textName = (TextView) cardia.getChildAt(0);
                                TextView textGame = (TextView) cardia.getChildAt(1);
                                TextView textAmt = (TextView) cardia.getChildAt(2);
                                final Button challenButton = (Button) cardia.getChildAt(3);
                                textName.setText(responseName);
                                textGame.setText(responseGame);
                                textAmt.setText(responseAmt);
                                challenButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        // Get firebase database reference
                                        DatabaseReference userOnlinerStatus = FirebaseDatabase.getInstance().getReference("users/"+responseName+"/status");
                                        userOnlinerStatus.addListenerForSingleValueEvent( new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot snapshot) {
                                         //       if(snapshot.getValue().equals("online")){
                                                    challengerButton = challenButton;

                                                Activity activity = getActivity();
                                                if(activity == null) return;

                                                    if(userSession.getChallengeState() != null){
                                                        Toast.makeText(getActivity(), "Please Wait!", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        String usernome = userSession.getUserID();
                                                        final String usertarget = responseName;
                                                        final String betId = responseId;

                                                        if (!getActivity().isFinishing()) {
         showpDialog();
}

                                                        RestService service = RestClient.getClient(userSession).create(RestService.class);
                                                        Call<User> call = service.userStartX("Bearer " + userSession.getUserToken(), usernome, usertarget, betId);
                                                        // Call<User> call = service.userAccept("Bearer "+userSession.getUserToken(),usernome,userbeta,betId);
                                                        call.enqueue(new Callback<User>() {
                                                            @Override
                                                            public void onResponse(Call<User> call, Response<User> response) {

                                                                //Toast.makeText()
                                                                if (response.isSuccessful()) {
                                                                    if (response.body() != null) {
                                                                        // String jsonResponse = response.body().toString();
                                                                        //saveInfo(jsonResponse);
                                                                        userSession.setChallengeState("active");

                                                                        Toast.makeText(getActivity(), "Challenge Accepted!", Toast.LENGTH_SHORT).show();
                                                                        Log.d("Tokeniser: ", response.body().getSuccessor());
                                                                        challenButton.setText(R.string.busyx);

                                                                        int delay = 80000; //milliseconds

                                                                        runnablec = new Runnable(){
                                                                            public void run(){
                                                                                if(userSession.getGameState() != null && "active".equals(userSession.getGameState())){
                                                                                    handler.removeCallbacks(runnablec);
                                                                                    if (!getActivity().isFinishing()) {
                                                                                        hidepDialog();
                                                                                    }
                                                                                    userSession.setChallengeState(null);
                                                                                    challenButton.setText(R.string.accept);

                                                                                } else {
                                                                                    //do something
                                                                                    if (!getActivity().isFinishing()) {
                                                                                        hidepDialog();
                                                                                    }
                                                                                    userSession.setChallengeState(null);
                                                                                    challenButton.setText(R.string.accept);
                                                                                }

                                                                            }
                                                                        };

                                                                        handler.postDelayed(runnablec, delay);


                                                                    } else {
                                                                        if (!getActivity().isFinishing()) {
         hidepDialog();
}
                                                                        Toast.makeText(getActivity(), "Empty response", Toast.LENGTH_LONG).show();
                                                                    }

                                                                } else {
                                                                    if (!getActivity().isFinishing()) {
         hidepDialog();
}
                                                                    Toast.makeText(getActivity(), "Challenge Not Accepted, Check your Internet or Balance!", Toast.LENGTH_SHORT).show();
                                                                }
                                                            }

                                                            @Override
                                                            public void onFailure(Call<User> call, final Throwable t) {
                                                                // Get firebase database reference
                                                                FirebaseDatabase database = FirebaseDatabase.getInstance();
                                                                DatabaseReference gameDbRef = database.getReference("startx");

                                                                if(userSession.getUserID() == null){


                                                                } else {

                                                                    // Read from the database

                                                                    startxEventListener = gameDbRef.child(usertarget).addValueEventListener(new ValueEventListener() {
                                                                        @Override
                                                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                                                            // This method is called once with the initial value and again
                                                                            // whenever data at this location is updated.
                                                                            Nota nota = dataSnapshot.getValue(Nota.class);

                                                                            Activity activity = getActivity();
                                                                            if(activity == null) return;

                                                                            if("".equals(nota.getBacker())) {

                                                                            } else {

                                                                                if (nota.getNota() != null) {
                                                                                    String action = nota.getBet_id();

                                                                                    if (action.equals(betId)) {
                                                                                        if (!getActivity().isFinishing()) {
                                                                                            showpDialog();
                                                                                        }

                                                                                    } else {
                                                                                        if (!getActivity().isFinishing()) {
                                                                                            hidepDialog();
                                                                                        }

                                                                                        Log.d("onFailure", t.toString());
                                                                                        Toast.makeText(getActivity(), "Failure Communicating With Server", Toast.LENGTH_SHORT).show();

                                                                                    }


                                                                                }

                                                                            }


                                                                        }

                                                                        @Override
                                                                        public void onCancelled(DatabaseError error) {
                                                                            // Failed to read value

                                                                            if (!getActivity().isFinishing()) {
                                                                                hidepDialog();
                                                                            }
                                                                            userSession.setChallengeState(null);
                                                                            Log.d("onFailure", t.toString());
                                                                            Toast.makeText(getActivity(), "Failure Communicating With Server", Toast.LENGTH_SHORT).show();

                                                                            Log.w("FirebaseDB", "Failed to read value.", error.toException());
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        });
                                                    }
                                            //    } else {
                                            //        Toast.makeText(getActivity(), ""+responseName+" is away!", Toast.LENGTH_SHORT).show();
                                            //    }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError error) {
                                                // Failed to read value
                                                Log.w("FirebaseDB", "Failed to read value.", error.toException());
                                            }

                                        });

                                    }
                                });
                            } catch (JSONException error){
                                error.printStackTrace();
                            }


                        }

                        // Log.d("onStringBet", "Testor "+responseMsg);
                        loadCounter = "on";

                    } else {
                        Toast.makeText(getActivity(),"Empty response",Toast.LENGTH_LONG).show();
                    }
                } else {
                    Log.d("onFailureBet", response.toString());
                    Toast.makeText(getActivity(), "Load Error!", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Loadbet> call, Throwable t) {
                if (!getActivity().isFinishing()) {
         hidepDialog();
}
                loadEmptyTxt.setVisibility(View.VISIBLE);
                loadEmptyButton.setVisibility(View.VISIBLE);
                loaderPointer = true;

                Log.d("onFailureBet", t.toString());
                Toast.makeText(getActivity(), "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
            }
        });

        scv.fullScroll(ScrollView.FOCUS_UP);
    }

    private void loadBetFromServer(Integer offseta, Integer limita) {
        pDialog  = new ProgressDialog(getContext(),  R.style.AppTheme_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        offset = offseta;
        limit = limita;

        if (!getActivity().isFinishing()) {
         showpDialog();
}

        RestService serviceBet = RestClient.getClient(userSession).create(RestService.class);
        Call<Loadbet> call = serviceBet.userLoadBetX("Bearer "+userSession.getUserToken(),userSession.getUserID(),offset,limit);
        call.enqueue(new Callback<Loadbet>() {
            @Override
            public void onResponse(Call<Loadbet> call, Response<Loadbet> response) {
                if (!getActivity().isFinishing()) {
         hidepDialog();
}

                if(loaderPointer){
                    loadEmptyTxt.setVisibility(View.INVISIBLE);
                    loadEmptyButton.setVisibility(View.INVISIBLE);
                    loaderPointer = false;
                }

                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        for(int k=0;k<linearView.getChildCount();k++){
                            linearView.getChildAt(k).setVisibility(View.INVISIBLE);
                        }
                        prev.setVisibility(View.INVISIBLE);
                        next.setVisibility(View.INVISIBLE);

                        if(response.body().getSuccess().getCounta() == 0){
                            loadEmptyTxt.setVisibility(View.VISIBLE);
                            loadEmptyButton.setVisibility(View.VISIBLE);
                        }


                        String responser = response.body().getSuccess().getNota().toString();
                        Integer counta = response.body().getSuccess().getCounta();
                        if(counta > 10){
                            totalState = counta;
                            if(prevState <= 0){
                                prev.setEnabled(false);
                            } else {
                                prev.setEnabled(true);
                            }
                            if(totalState - countState <= 10){
                                next.setEnabled(false);
                            } else {
                                next.setEnabled(true);
                            }
                            prev.setVisibility(View.VISIBLE);
                            next.setVisibility(View.VISIBLE);
                        }
                        // Toast.makeText(getActivity(), "Responser "+responser, Toast.LENGTH_SHORT).show();
                        //  Log.d("onSuccessBet", "Test "+response.body().getSuccess().getNota());

                        ArrayList<SuccessLoad> responseMsg = response.body().getSuccess().getNota();

                        if(responseMsg.size() == 0){


                        } else {

                            for (int i = 0; i < responseMsg.size(); i++) {
                                GsonBuilder gsonBuilder = new GsonBuilder();
                                Gson gson = gsonBuilder.create();
                                String objecto = gson.toJson(responseMsg.get(i));
                                try {
                                    JSONObject objector = new JSONObject(objecto);
                                    final String responseId = objector.getString("id");
                                    final String responseName = objector.getString("uuid");
                                    String responseAmt;
                                    if (Integer.parseInt(objector.getString("amt")) == 0) {
                                        responseAmt = "Free Game!";
                                    } else {
                                        responseAmt = "₦" + objector.getString("amt");
                                    }
                                    String responseGame = objector.getString("game");
                                    linearView.getChildAt(i).setVisibility(View.VISIBLE);
                                    CardView cardia = (CardView) linearView.getChildAt(i);
                                    TextView textName = (TextView) cardia.getChildAt(0);
                                    TextView textGame = (TextView) cardia.getChildAt(1);
                                    TextView textAmt = (TextView) cardia.getChildAt(2);
                                    final Button challenButton = (Button) cardia.getChildAt(3);
                                    textName.setText(responseName);
                                    textGame.setText(responseGame);
                                    textAmt.setText(responseAmt);
                                    challenButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            // Get firebase database reference
                                            DatabaseReference userOnlinerStatus = FirebaseDatabase.getInstance().getReference("users/"+responseName+"/status");
                                            userOnlinerStatus.addListenerForSingleValueEvent( new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot snapshot) {
                                             //       if(snapshot.getValue().equals("online")){
                                                        challengerButton = challenButton;

                                                    Activity activity = getActivity();
                                                    if(activity == null) return;

                                                        if (userSession.getChallengeState() != null) {
                                                            Toast.makeText(getActivity(), "Please Wait!", Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            String usernome = userSession.getUserID();
                                                            final String userbeta = responseName;
                                                            final String betId = responseId;

                                                            if (!getActivity().isFinishing()) {
         showpDialog();
}

                                                            RestService service = RestClient.getClient(userSession).create(RestService.class);
                                                            Call<User> call = service.userStartX("Bearer " + userSession.getUserToken(), userbeta, usernome, betId);
                                                            // Call<User> call = service.userAccept("Bearer "+userSession.getUserToken(),usernome,userbeta,betId);
                                                            call.enqueue(new Callback<User>() {
                                                                @Override
                                                                public void onResponse(Call<User> call, Response<User> response) {

                                                                    //Toast.makeText()
                                                                    if (response.isSuccessful()) {
                                                                        if (response.body() != null) {
                                                                            // String jsonResponse = response.body().toString();
                                                                            // saveInfo(jsonResponse);
                                                                            userSession.setChallengeState("active");
                                                                            Toast.makeText(getActivity(), "Challenge Accepted!", Toast.LENGTH_SHORT).show();
                                                                            Log.d("Tokeniser: ", response.body().getSuccessor());
                                                                            challenButton.setText(R.string.busyx);

                                                                            int delay = 80000; //milliseconds

                                                                            runnablec = new Runnable() {
                                                                                public void run() {
                                                                                    if (userSession.getGameState() != null && "active".equals(userSession.getGameState())) {
                                                                                        handler.removeCallbacks(runnablec);
                                                                                        if (!getActivity().isFinishing()) {
                                                                                            hidepDialog();
                                                                                        }
                                                                                        userSession.setChallengeState(null);
                                                                                        challenButton.setText(R.string.accept);
                                                                                    } else {
                                                                                        //do something
                                                                                        if (!getActivity().isFinishing()) {
                                                                                             hidepDialog();
                                                                                        }
                                                                                             userSession.setChallengeState(null);
                                                                                             challenButton.setText(R.string.accept);
                                                                                    }

                                                                                }
                                                                            };

                                                                            handler.postDelayed(runnablec, delay);


                                                                        } else {
                                                                            if (!getActivity().isFinishing()) {
         hidepDialog();
}
                                                                            Toast.makeText(getActivity(), "Empty response", Toast.LENGTH_LONG).show();
                                                                        }

                                                                    } else {
                                                                        if (!getActivity().isFinishing()) {
         hidepDialog();
}
                                                                        Toast.makeText(getActivity(), "Challenge Not Accepted, Check your Internet or Balance!", Toast.LENGTH_SHORT).show();
                                                                    }
                                                                }

                                                                @Override
                                                                public void onFailure(Call<User> call, final Throwable t) {
                                                                    // Get firebase database reference
                                                                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                                                                    DatabaseReference gameDbRef = database.getReference("startx");

                                                                    if(userSession.getUserID() == null){


                                                                    } else {

                                                                        // Read from the database

                                                                        startxEventListener = gameDbRef.child(userbeta).addValueEventListener(new ValueEventListener() {
                                                                            @Override
                                                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                                                // This method is called once with the initial value and again
                                                                                // whenever data at this location is updated.
                                                                                Nota nota = dataSnapshot.getValue(Nota.class);

                                                                                Activity activity = getActivity();
                                                                                if(activity == null) return;


                                                                                if("".equals(nota.getBacker())) {

                                                                                } else {

                                                                                    if (nota.getNota() != null) {
                                                                                        String action = nota.getBet_id();

                                                                                        if (action.equals(betId)) {
                                                                                            if (!getActivity().isFinishing()) {
                                                                                                showpDialog();
                                                                                            }

                                                                                        } else {
                                                                                            if (!getActivity().isFinishing()) {
                                                                                                hidepDialog();
                                                                                            }

                                                                                            Log.d("onFailure", t.toString());
                                                                                            Toast.makeText(getActivity(), "Failure Communicating With Server", Toast.LENGTH_SHORT).show();

                                                                                        }


                                                                                    }

                                                                                }


                                                                            }

                                                                            @Override
                                                                            public void onCancelled(DatabaseError error) {
                                                                                // Failed to read value
                                                                                if (!getActivity().isFinishing()) {
                                                                                    hidepDialog();
                                                                                }
                                                                                userSession.setChallengeState(null);
                                                                                Log.d("onFailure", t.toString());
                                                                                Toast.makeText(getActivity(), "Failure Communicating With Server", Toast.LENGTH_SHORT).show();

                                                                                Log.w("FirebaseDB", "Failed to read value.", error.toException());
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            });
                                                        }
                                               //     } else {
                                                 //       Toast.makeText(getActivity(), ""+responseName+" is away!", Toast.LENGTH_SHORT).show();
                                                 //   }
                                                }

                                                @Override
                                                public void onCancelled(DatabaseError error) {
                                                    // Failed to read value
                                                    Log.w("FirebaseDB", "Failed to read value.", error.toException());
                                                }

                                            });


                                        }
                                    });
                                } catch (JSONException error) {
                                    error.printStackTrace();
                                }


                            }

                        }

                        // Log.d("onStringBet", "Testor "+responseMsg);
                        loadCounter = "on";

                    } else {
                        if (!getActivity().isFinishing()) {
         hidepDialog();
}
                        Toast.makeText(getActivity(),"Empty response",Toast.LENGTH_LONG).show();
                    }
                } else {
                    if (!getActivity().isFinishing()) {
         hidepDialog();
}
                    Log.d("onFailureBet", response.toString());
                    Toast.makeText(getActivity(), "Load Error!", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Loadbet> call, Throwable t) {
                if (!getActivity().isFinishing()) {
         hidepDialog();
}
                loadEmptyTxt.setVisibility(View.VISIBLE);
                loadEmptyButton.setVisibility(View.VISIBLE);
                loaderPointer = true;

                Log.d("onFailureBet", t.toString());
                Toast.makeText(getActivity(), "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
            }
        });
        scv.fullScroll(ScrollView.FOCUS_UP);
    }



    public void displayer(String titler, String messager, Context context){
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setCancelable(false);
        dialog.setTitle(titler);
        dialog.setMessage(messager);
        dialog.setPositiveButton("Ok", null);

        final AlertDialog alert = dialog.create();
        alert.show();
    }

    public void displayStop(String titler, String messager, Context context){

            userSession.setChallengeState(null);
            // session.setAppState(null);
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setCancelable(false);
            dialog.setTitle(titler);
            dialog.setMessage(messager);
            userSession.setPlay(null);
            userSession.setReqTitle(null);
            userSession.setReqStop(null);
            userSession.setReqNota(null);
            userSession.setUserBeta(null);
            userSession.setUserAcc(null);
            //  session.setBetId(null);

            dialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {

                    RestService service = RestClient.getClient(userSession).create(RestService.class);
                    Call<User> call = service.userRemStop("Bearer " + userSession.getUserToken(), userSession.getUserID());
                    call.enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {

                            //Toast.makeText()
                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    Log.d("StopConfirm", "Yeta Clicked!");
                                    // Replace fragment with
                                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                    FragmentTransaction ft = fragmentManager.beginTransaction();
                                    ft.detach(PendingBet.this).attach(PendingBet.this).commit();
                                }
                            } else {

                            }
                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable t) {
                            Toast.makeText(getActivity(), "Request Cancelled Failed!", Toast.LENGTH_SHORT).show();
                        }
                    });


                }
            });

            final AlertDialog alert = dialog.create();
            alert.show();


    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void loadPrev(){
        if(prevState >= 10) {
            prevState -= 10;
            countState -= 10;
            if(searchPointer){
                Context context = getContext();
                searchBetFromServer(countState,10,queryGlobal, context);
            } else {
                loadBetFromServer(countState,10);
            }

            Log.d("prevState", " "+prevState);
            Log.d("countState", " "+countState);
            Log.d("nextState", " "+nextState);
            Log.d("totalState", " "+totalState);

        } else {
            if(searchPointer){
                Context context = getContext();
                searchBetFromServer(countState, 10,queryGlobal, context);
            } else {
                loadBetFromServer(countState, prevState);
            }

            prevState = 0;
            countState = 0;

            Log.d("prevState", " "+prevState);
            Log.d("countState", " "+countState);
            Log.d("nextState", " "+nextState);
            Log.d("totalState", " "+totalState);

        }

    }

    private void loadMore(){
        if(totalState - countState >= 10){
            prevState += 10;
            countState += 10;
            if(searchPointer){
                Context context = getContext();
                searchBetFromServer(countState, 10,queryGlobal, context);
            } else {
                loadBetFromServer(countState,10);
            }

            Log.d("prevState", " "+prevState);
            Log.d("countState", " "+countState);
            Log.d("nextState", " "+nextState);
            Log.d("totalState", " "+totalState);


        } else {
            prevState = prevState + (totalState - countState);
            countState = countState + (totalState - countState);
            if(searchPointer) {
                Context context = getContext();
                searchBetFromServer(countState, 10, queryGlobal, context);
            } else {
                loadBetFromServer(countState, 10);
            }

            Log.d("prevState", " "+prevState);
            Log.d("countState", " "+countState);
            Log.d("nextState", " "+nextState);
            Log.d("totalState", " "+totalState);

        }

    }



    @Override
    public void setMenuVisibility(boolean isvisible) {
        super.setMenuVisibility(isvisible);
        if (isvisible){
            Log.d("Viewpager", "fragment is visible ");

        }else {
            Log.d("Viewpager", "fragment is not visible ");
        }
    }



    @Override
    public void onDestroy () {
        handler.removeCallbacks(runnablec);
        super.onDestroy ();

    }


    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.prevButton:
                loadPrev();
                break;

            case R.id.nextButton:
                loadMore();
                break;

            case R.id.searchButton:

                if(searcher.getQuery().toString().equals("")){

                    displayer("Search Error","Enter Search Term!",context);

                } else {
                    // prevState = 0;
                    //  nextState = 10;
                    //  countState = 0;
                    //  totalState = 0;
                    Context context = getContext();
                    searchBetFromServer(0,10,searcher.getQuery().toString(), context);
                    //searchPointer = true;
                }
                break;

            case R.id.noDataButton:

                if(userSession.getUserToken() != null) {
                    // Replace fragment with
                    FragmentManager fragmentManager = getParentFragmentManager();
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.detach(PendingBet.this).attach(PendingBet.this).commit();
                    Toast.makeText(getActivity(), "Please Come Back Later!", Toast.LENGTH_SHORT).show();

                    break;
                }
        }
    }
}