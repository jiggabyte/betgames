package com.godwin.betgames.views;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.godwin.betgames.MainActivity;
import com.godwin.betgames.PreferenceHelper;
import com.godwin.betgames.R;
import com.godwin.betgames.RestClient;
import com.godwin.betgames.RestService;
import com.godwin.betgames.Session;
import com.godwin.betgames.models.User;
import com.godwin.betgames.Utils;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private View view;

    private EditText emailid, passwordx;
    private Button loginButton;
    private TextView forgotPassword, signUp, verifier;
    private CheckBox show_hide_password;
    private LinearLayout loginLayout;
    private Animation shakeAnimation;
    private FragmentManager fragmentManager;
    private ProgressDialog pDialog;
    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]

    private String mCustomToken;


    //Shared Preference
    private PreferenceHelper preferenceHelper;
    //Session Handler
    private Session userSession;
    private static String uniqueID = null;
    private static final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";

    public LoginActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_login);

        preferenceHelper = new PreferenceHelper(this);
        userSession = new Session(this);
        initViews();
        setListeners();
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
    }

    @Override
    public void onBackPressed() {
        /**
        new AlertDialog.Builder(this)
                .setTitle("Click OK to Exit")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        // FinishWhotActivity.super.onBackPressed();
                        //Toast.makeText(MainActivity.this, "Game Over, You Lost!", Toast.LENGTH_LONG).show();
                        LoginActivity.this.finish();

                    }
                }).create().show();
        */
    }

    // Initiate Views
    private void initViews() {
        fragmentManager = getSupportFragmentManager();

        emailid = findViewById(R.id.login_emailid);
        passwordx = findViewById(R.id.login_password);
        loginButton = findViewById(R.id.loginBtn);
        forgotPassword = findViewById(R.id.forgot_password);
        signUp = findViewById(R.id.createAccount);
        show_hide_password = findViewById(R.id.show_hide_password);
        loginLayout = findViewById(R.id.login_layout);
        verifier = findViewById(R.id.verifyAccount);

        // Load ShakeAnimation
        shakeAnimation = AnimationUtils.loadAnimation(this,
                R.anim.shake);

        // Setting text selector over textviews
        int[][] states = new int[][] {
                new int[] {-android.R.attr.state_pressed}, // unpressed
                new int[] { android.R.attr.state_pressed}  // pressed
        };

        int[] colors = new int[] {
                Color.WHITE,
                Color.BLACK
        };

        try {
            ColorStateList csl = new ColorStateList(states, colors);

            forgotPassword.setTextColor(csl);
            show_hide_password.setTextColor(csl);
            signUp.setTextColor(csl);
            verifier.setTextColor(csl);
        } catch (Exception e) {
        }
    }

    // Set Listeners
    private void setListeners() {
        loginButton.setOnClickListener(this);
        forgotPassword.setOnClickListener(this);
        signUp.setOnClickListener(this);
        verifier.setOnClickListener(this);

        // Set check listener over checkbox for showing and hiding password
        show_hide_password
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton button,
                                                 boolean isChecked) {

                        // If it is checked then show password else hide
                        // password
                        if (isChecked) {

                            show_hide_password.setText(R.string.hide_pwd);// change
                            // checkbox
                            // text

                            passwordx.setInputType(InputType.TYPE_CLASS_TEXT);
                            passwordx.setTransformationMethod(HideReturnsTransformationMethod
                                    .getInstance());// show password
                        } else {
                            show_hide_password.setText(R.string.show_pwd);// change
                            // checkbox
                            // text

                            passwordx.setInputType(InputType.TYPE_CLASS_TEXT
                                    | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                            passwordx.setTransformationMethod(PasswordTransformationMethod
                                    .getInstance());// hide password

                        }

                    }
                });
    }


    private void startSignIn(String logToken) {
        // Initiate sign in with custom token
        // [START sign_in_custom]
        mAuth.signInWithCustomToken(logToken)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            // Toast.makeText(LoginActivity.this, "Authentication Success!", Toast.LENGTH_SHORT).show();
                            // FirebaseUser user = mAuth.getCurrentUser();
                        } else {
                            // If sign in fails, display a message to the user.
                            // Toast.makeText(LoginActivity.this, "Authentication Failed!: "+task.getException(), Toast.LENGTH_SHORT).show();
                            // loginByServer();
                        }
                    }
                });
        // [END sign_in_custom]

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginBtn:
                checkValidation();
                break;

            case R.id.forgot_password:

                // Replace forgot activity
                Intent intentForgot;
                intentForgot = new Intent(LoginActivity.this, ForgotActivity.class);
                startActivity(intentForgot);
                break;
            case R.id.createAccount:

                // Replace signup activity
                Intent intentSign;
                intentSign = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(intentSign);
                break;

            case R.id.verifyAccount:

                // Replace verify activity
                Intent intentVer;
                intentVer = new Intent(LoginActivity.this, VerifyActivity.class);
                startActivity(intentVer);
                break;
        }

    }

    // Check Validation before login
    private void checkValidation() {
        // Get email id and password
        String getEmailId = emailid.getText().toString();
        String getPassword = passwordx.getText().toString();

        // Check patter for email id
        Pattern p = Pattern.compile(Utils.regEx);

        Matcher m = p.matcher(getEmailId);

        // Check for both field is empty or not
        if (getEmailId.equals("") || getEmailId.length() == 0
                || getPassword.equals("") || getPassword.length() == 0) {
            loginLayout.startAnimation(shakeAnimation);
            displayer("Login Error!","Enter both credentials.");

        }
        // Check if email id is valid or not
       // else if (!m.find())
        //    displayer("Login Error!","Your Email Id is Invalid.");
            // Else do login and do your stuff
        else
            loginByServer();

    }

    private void loginByServer() {
        pDialog = new ProgressDialog(LoginActivity.this,  R.style.AppTheme_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        showpDialog();

        final String email = emailid.getText().toString();
        final String password = passwordx.getText().toString();

        RestService service = RestClient.getClient(userSession).create(RestService.class);
        Call<User> call = service.userLogIn(email,password);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                hidepDialog();


                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                       // String jsonResponse = response.body().toString();
                       // saveInfo(jsonResponse);

                        String tokener = response.body().getSuccess().getToken();
                        String updater = response.body().getSuccess().getUpdated();
                        final String firebaser = response.body().getSuccess().getFirebase();

                        // Toast.makeText(LoginActivity.this, "Firebaser: "+firebaser, Toast.LENGTH_SHORT).show();

                        // Session session = new Session(LoginActivity.this);
                        userSession.setUserToken(null);
                        userSession.setUpdated(null);
                        userSession.setUserToken(tokener);
                        userSession.setUpdated(updater);

                        RestService serviceDet = RestClient.getClient(userSession).create(RestService.class);
                        Call<User> callDetail = serviceDet.userDetails("Bearer "+tokener,email);
                        callDetail.enqueue(new Callback<User>() {
                            @Override
                            public void onResponse(Call<User> call, Response<User> response) {
                                hidepDialog();

                                // Toast.makeText()
                                if (response.isSuccessful()) {
                                    if (response.body() != null) {
                                        String jsonResponse = response.body().getDetail().toString();
                                        saveUserInfo(jsonResponse);
                                        userSession.setUserID(null);
                                        userSession.setUserID(response.body().getDetail().getUuid());
                                        Log.d("onDetails", "Successar: "+response.body().getDetail().toString());

                                        FirebaseMessaging.getInstance().getToken()
                                                .addOnCompleteListener(new OnCompleteListener<String>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<String> task) {
                                                        if (!task.isSuccessful()) {
                                                            Log.w("FCM", "Fetching FCM registration token failed", task.getException());
                                                            return;
                                                        }

                                                        // Get new FCM registration token
                                                        String token = task.getResult();

                                                        // Log and toast
                                                        Log.d("FCM", token);

                                                        String newToken = task.getResult();
                                                        String userid = userSession.getUserID();
                                                        String deviceid = userDeviceID(userSession);

                                                        RestService service = RestClient.getClient(userSession).create(RestService.class);
                                                        Call<User> call = service.userSendToken("Bearer "+userSession.getUserToken(),userid,newToken,deviceid);
                                                        call.enqueue(new Callback<User>() {
                                                            @Override
                                                            public void onResponse(Call<User> call, Response<User> response) {

                                                                //Toast.makeText()
                                                                if (response.isSuccessful()) {
                                                                    if (response.body() != null) {
                                                                        Toast.makeText(LoginActivity.this, "Logged in", Toast.LENGTH_SHORT).show();

                                                                        startSignIn(firebaser);
                                                                        // Replace account fragment with animation
                                                                        Intent intentHome;
                                                                        intentHome = new Intent(LoginActivity.this, MainActivity.class);
                                                                        startActivity(intentHome);
                                                                    }
                                                                } else {
                                                                    Toast.makeText(LoginActivity.this, "Login Failed, Check Login Details / Google Play Services!", Toast.LENGTH_SHORT).show();
                                                                }
                                                            }
                                                            @Override
                                                            public void onFailure(Call<User> call, Throwable t) {

                                                            }
                                                        });


                                                    }
                                                });


                                    } else {
                                        Toast.makeText(LoginActivity.this,"Empty response",Toast.LENGTH_LONG).show();
                                    }
                                }  else if (response.code() == 402){
                                    Toast.makeText(LoginActivity.this, "Load Error ", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(LoginActivity.this, "Unable to fetch data", Toast.LENGTH_SHORT).show();
                                }
                            }
                            @Override
                            public void onFailure(Call<User> call, Throwable t) {
                                hidepDialog();
                                Log.d("onFailure", t.toString());
                                Toast.makeText(LoginActivity.this, "Failure Communicating With The Server", Toast.LENGTH_SHORT).show();
                            }
                        });

                    } else {
                        Toast.makeText(LoginActivity.this,"Empty response",Toast.LENGTH_LONG).show();
                    }
                } else if (!response.isSuccessful()){
                    Toast.makeText(LoginActivity.this, "Invalid Username or Password ", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(LoginActivity.this, "Unable to Login", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
                Toast.makeText(LoginActivity.this, "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
            }
        });


    }


    public static String userDeviceID(Session session) {
        String uniqueUUID;
        if (session.getDeviceID() != null) {
                uniqueUUID = session.getDeviceID();
        } else {
            uniqueUUID = UUID.randomUUID().toString();
            session.setDeviceID(uniqueUUID);
        }
        return uniqueUUID;
    }


    public synchronized static String id(Context context) {
        if (uniqueID != null) {
            SharedPreferences sharedPrefs = context.getSharedPreferences(
                    PREF_UNIQUE_ID, Context.MODE_PRIVATE);
            uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null);
            if (uniqueID == null) {
                uniqueID = UUID.randomUUID().toString();
                Log.d("onUserUUID","uuid: "+UUID.randomUUID().toString());
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString(PREF_UNIQUE_ID, uniqueID);
                editor.commit();
            }
        }
        return uniqueID;
    }


    private void saveUserInfo(String response){
        userSession.setUserJSON(null);
        userSession.setUserJSON(response);
    }
    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void displayer(String titler, String messager){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(false);
        dialog.setTitle(titler);
        dialog.setMessage(messager);
        dialog.setPositiveButton("Ok", null);

        final AlertDialog alert = dialog.create();
        alert.show();
    }
}