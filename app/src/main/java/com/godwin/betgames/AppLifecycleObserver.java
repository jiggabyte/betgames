package com.godwin.betgames;

import android.os.Handler;
import android.util.Log;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

public class AppLifecycleObserver implements LifecycleObserver {

    MainActivity mainActivity;

    AppLifecycleObserver() {
       // this.mainActivity = new MainActivity();
    }

    public static final String TAG = AppLifecycleObserver.class.getName();

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void onEnterForeground() {
        //run the code we need
        Log.d("Sixtus ","Foreground");
        mainActivity = new MainActivity();
      //  mainActivity.setBackgroundNonActive();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onEnterBackground() {
        //run the code we need
        Log.d("Sixtus-X ","Background");
        mainActivity = new MainActivity();
      //  mainActivity.setBackgroundActive();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onEnterKillApp() {
        //run the code we need
        Log.d("Sixtus-Y ","App Killed");
        mainActivity = new MainActivity();
       // mainActivity.setSplashActive();
    }

}
