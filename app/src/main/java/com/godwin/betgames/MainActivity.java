package com.godwin.betgames;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.godwin.betgames.models.Nota;
import com.godwin.betgames.models.User;
import com.godwin.betgames.views.ClassicWhotActivity;
import com.godwin.betgames.views.FinishWhotActivity;
import com.godwin.betgames.views.LoginActivity;
import com.godwin.betgames.views.Preferences;
import com.godwin.betgames.views.ui.challenger.ChallengerFragment;
import com.godwin.betgames.views.ui.main.MainFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.ui.AppBarConfiguration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private FragmentManager fragmentManager;
    com.godwin.betgames.PreferenceHelper preferenceHelper;
    public com.godwin.betgames.Session session;
    private ProgressDialog pDialog;
    private Intent intent;
    Context context;
    private String[] parameter = new String[9];

    Handler handler;
    Runnable runnable;
    Runnable runnablec;

    long megAvailable;

    private String lifer;

    private static String uniqueID = null;
    private static final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";

    private boolean notabene = false;

    private AppBarConfiguration mAppBarConfiguration;
    private MenuItem account;


    ArrayList<String> displayData;

    ValueEventListener startxEventListener;
    ValueEventListener startzEventListener;
    ValueEventListener logoutEventListener;
    ValueEventListener startxChildEventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        session = new Session(MainActivity.this);
        requestWindowFeature(Window.FEATURE_NO_TITLE); // will hide the title
      //  getSupportActionBar().hide(); // hide the title bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_main);





        //   Toast.makeText(MainActivity.this, "GameStateX: "+session.getGameState(), Toast.LENGTH_SHORT).show();

        // MyApp app = new MyApp();
        //  app.onCreate();

        session.setBackgroundState(null);

        if(session.getReferer() != null){

            RestService service = RestClient.getClient(session).create(RestService.class);
            Call<User> call = service.userRef("Bearer " + session.getUserToken(), session.getReferer(), session.getUserID());
            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {

                    //Toast.makeText()
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            session.setReferer(null);
                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {

                }
            });

        }
        if(session.getFirstRun() != null && session.getFirstRun().equals("true")){

        } else {
            if(session.getUserID() == null){

            } else {
                session.setFirstRun("true");
                FirebaseMessaging.getInstance().getToken()
                        .addOnCompleteListener(new OnCompleteListener<String>() {
                            @Override
                            public void onComplete(@NonNull Task<String> task) {
                                if (!task.isSuccessful()) {
                                    Log.w("FCM", "Fetching FCM registration token failed", task.getException());
                                    return;
                                }

                                // Get new FCM registration token
                                String token = task.getResult();

                                // Log and toast
                                Log.d("FCM", token);

                                String newToken = task.getResult();
                                String userid = session.getUserID();
                                String deviceid = userDeviceID(session);

                                RestService service = RestClient.getClient(session).create(RestService.class);
                                Call<User> call = service.userSendToken("Bearer " + session.getUserToken(), userid, newToken, deviceid);
                                call.enqueue(new Callback<User>() {
                                    @Override
                                    public void onResponse(Call<User> call, Response<User> response) {

                                        //Toast.makeText()
                                        if (response.isSuccessful()) {
                                            if (response.body() != null) {
                                                Toast.makeText(MainActivity.this, "Setup Complete!", Toast.LENGTH_LONG).show();
                                            }
                                        } else {
                                            Toast.makeText(MainActivity.this, "Setup Incomplete!", Toast.LENGTH_LONG).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<User> call, Throwable t) {
                                        Toast.makeText(MainActivity.this, "Setup Error!", Toast.LENGTH_LONG).show();
                                    }
                                });


                            }
                        });
            }
        }

        session.setBackState("non-active-x");
        //  session.setGameState(null);
        session.setChallengeState(null);
        session.setAppState(null);


        // getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        createNotificationChannel();

        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter("Restart"));

        lifer = "create";

        // startService(new Intent(this, GameService.class));
        // stopService(new Intent(this, GameService.class));

        handler = new Handler();
        final int delay = 15000; //milliseconds

        if(!isGooglePlayServicesAvailable(MainActivity.this)) {
            displayer("Google Play Services","This Device seems not to have Google Play Services which is required for this game to function well!");
        }

        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long bytesAvailable;
        bytesAvailable = stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
        megAvailable = bytesAvailable / (1024 * 1024);
        if(megAvailable < 50){
            displayer("Storage Warning.","Your Device Internal Storage is getting low and might affect your game perfomance!");
        }


        pDialog = new ProgressDialog(MainActivity.this,  R.style.AppTheme_Dialog);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        // Get firebase database reference
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference gameDbRef = database.getReference("startx");

        if(session.getUserID() == null){


        } else {

            // Read from the database

            startxEventListener = gameDbRef.child(session.getUserID()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.
                    Nota nota = dataSnapshot.getValue(Nota.class);

                    Activity activity = MainActivity.this;
                    if(activity == null) return;

                    if("".equals(nota.getBacker())) {

                    } else {

                        if (nota.getNota() != null) {
                            String action = nota.getNota();

                            if (action.equals("displayConfirm")) {


                                getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

                                session.setPlay(null);
                                session.setReqTitle(null);
                                session.setReqStop(null);
                                session.setReqNota(null);
                                session.setUserBeta(null);
                                session.setUserAcc(null);
                                // session.setBetId(null);
                                String user_beta;
                                String user_acc;
                                String bet_id;
                                String bet_amt;
                                String bet_game;

                                user_beta = nota.getUser_beta();
                                user_acc = nota.getUser_acc();
                                bet_id = nota.getBet_id();
                                bet_amt = nota.getBet_amt();
                                bet_game = nota.getBet_game();

                                parameter[0] = nota.getUpdated_at();
                                parameter[1] = user_beta;
                                parameter[2] = user_acc;
                                parameter[3] = bet_id;
                                parameter[4] = bet_amt;
                                parameter[5] = bet_game;

                                displayData = new ArrayList<String>(Arrays.asList(parameter));

                                SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                Date now = new Date();
                                String strDate = sdfDate.format(now);

                                if (findDifference(parameter[0],strDate) >= 50) {

                                } else {
                                    if(session.getBackState() != null && session.getBackState().equals("active")){
                                        Preferences.setArrayPrefs("displayArrList", displayData, MainActivity.this);
                                    } else {
                                        // You can be pretty confident that the intent will not be null here.
                                        Intent intent = getIntent();

                                        // Get the extras (if there are any)
                                        Bundle extra = intent.getExtras();

                                        boolean checker = false;

                                        if (extra != null) {
                                            //  if ("active".equals(session.getGameState())) return;
                                            if (extra.containsKey("resetter")) {
                                                checker = true;

                                            } else {

                                            }
                                        }
                                        if(checker){

                                        } else {
                                            displayConfirm("Challenge Started.", "Your Bet Challenge has been started, Confirm Start?", parameter);
                                        }

                                    }
                                }

                            } else if (action.equals("displayStop")) {
                                getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                                session.setPlay(null);
                                session.setReqTitle(null);
                                session.setReqStop(null);
                                session.setReqNota(null);
                                session.setUserBeta(null);
                                session.setUserAcc(null);
                                // session.setBetId(null);
                                String user_beta;
                                String user_acc;
                                String bet_id;

                                user_beta = nota.getUser_beta();
                                user_acc = nota.getUser_acc();
                                bet_id = nota.getBet_id();
                                parameter[0] = nota.getUpdated_at();
                                parameter[1] = user_beta;
                                parameter[2] = user_acc;
                                parameter[3] = bet_id;

                                SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                Date now = new Date();
                                String strDate = sdfDate.format(now);

                                if (findDifference(parameter[0],strDate) >= 50) {

                                } else {

                                    displayStop("Request Cancelled.", "Your Accepted Challenge has been cancelled.");

                                }


                            } else {
                                //  Toast.makeText(MainActivity.this, "Start Error!", Toast.LENGTH_SHORT).show();
                            }




                        }

                    }


                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value

                    Log.w("FirebaseDB", "Failed to read value.", error.toException());
                }
            });
        }

        DatabaseReference gameDbRefz = database.getReference("startz");

        if(session.getUserID() == null){


        } else {

            // Read from the database


            startzEventListener = gameDbRefz.child(session.getUserID()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.
                    Nota nota = dataSnapshot.getValue(Nota.class);

                    Activity activity = MainActivity.this;
                    if(activity == null) return;

                    if(nota != null){
                        if("".equals(nota.getBacker())) {

                        } else {

                            if (nota.getNota() != null) {
                                String action = nota.getNota();

                                if (action.equals("startGame")) {
                                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                                    session.setPlay(null);
                                    session.setReqTitle(null);
                                    session.setReqStop(null);
                                    session.setReqNota(null);
                                    session.setUserBeta(null);
                                    session.setUserAcc(null);
                                    // session.setBetId(null);
                                    String user_beta;
                                    String user_acc;
                                    String bet_id;

                                    user_beta = nota.getUser_beta();
                                    user_acc = nota.getUser_acc();
                                    bet_id = nota.getBet_id();
                                    parameter[0] = "";
                                    parameter[1] = user_beta;
                                    parameter[2] = user_acc;
                                    parameter[3] = bet_id;
                                    startGame(parameter);

                                    //  Toast.makeText(MainActivity.this, "Start Details: "+session.getEndState()+" : "+nota.getId(), Toast.LENGTH_SHORT).show();



                                } else if (action.equals("Classic")) {

                                    //   Toast.makeText(MainActivity.this, "Start Details0: "+session.getEndState()+" : "+nota.getId(), Toast.LENGTH_SHORT).show();

                                    if(session.getDataState() != null && nota.getUpdated_at().equals(session.getDataState())){


                                    } else {
                                        if(session.getEndState() != 0 && nota.getId() == session.getEndState()){

                                        } else {
                                            if(!isFinishing())
                                            {
                                                hidepDialog();
                                            } else {

                                            }
                                            if(!isGooglePlayServicesAvailable(MainActivity.this) || megAvailable < 5) {
                                                displayer("Start Game Error.","This Device seems won't start the game as you don't have Google Play Services or your storage is below 5MB!");
                                            } else {

                                                Intent intentc = new Intent(MainActivity.this, ClassicWhotActivity.class);
                                                intentc.putExtra("stopper", "false");
                                                intentc.putExtra("id", nota.getId());
                                                intentc.putExtra("bet_id", nota.getBet_id());
                                                intentc.putExtra("player1", nota.getPlayer1());
                                                intentc.putExtra("player2", nota.getPlayer2());
                                                // intentc.putExtra("market", nota.getMarket_deck());
                                                intentc.putExtra("market_size", nota.getMarket_size());
                                                intentc.putExtra("one", nota.getPlayer1_deck());
                                                intentc.putExtra("two", nota.getPlayer2_deck());
                                                intentc.putExtra("play", nota.getPlay_deck());
                                                intentc.putExtra("turn", nota.getTurn());
                                                intentc.putExtra("game_type", nota.getGame_type());
                                                intentc.putExtra("timer", nota.getUpdated_at());

                                                hidepDialog();
                                                startActivity(intentc);
                                                MainActivity.this.finish();
                                            }
                                        }

                                    }



                                } else if (action.equals("Finish&Count")) {
                                    //   Toast.makeText(MainActivity.this, "Start Details1: "+session.getEndState()+" : "+nota.getId(), Toast.LENGTH_SHORT).show();

                                    if(session.getEndState() != 0 && nota.getId() == session.getEndState()){

                                    } else {
                                        if(!isFinishing())
                                        {
                                            hidepDialog();
                                        } else {

                                        }

                                        if(!isGooglePlayServicesAvailable(MainActivity.this) || megAvailable < 5) {
                                            displayer("Start Game Error.","This Device seems won't start the game as you don't have Google Play Services or your storage is below 5MB!");
                                        } else {
                                            Intent intentf = new Intent(MainActivity.this, FinishWhotActivity.class);
                                            intentf.putExtra("stopper", "false");
                                            intentf.putExtra("id", nota.getId());
                                            intentf.putExtra("bet_id", nota.getBet_id());
                                            intentf.putExtra("player1", nota.getPlayer1());
                                            intentf.putExtra("player2", nota.getPlayer2());
                                            // intentf.putExtra("market", nota.getMarket_deck());
                                            intentf.putExtra("market_size", nota.getMarket_size());
                                            intentf.putExtra("one", nota.getPlayer1_deck());
                                            intentf.putExtra("two", nota.getPlayer2_deck());
                                            intentf.putExtra("play", nota.getPlay_deck());
                                            intentf.putExtra("turn", nota.getTurn());
                                            intentf.putExtra("game_type", nota.getGame_type());

                                            startActivity(intentf);
                                            hidepDialog();
                                            MainActivity.this.finish();
                                        }
                                    }


                                } else {
                                    // Toast.makeText(MainActivity.this, "Start Error!", Toast.LENGTH_SHORT).show();
                                }

                            }

                        }

                    }




                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    Log.w("FirebaseDB", "Failed to read value.", error.toException());
                }
            });
        }


        DatabaseReference gameDbRefx = database.getReference("logout");

        if(session.getUserID() == null){


        } else {

            // Read from the database


            logoutEventListener = gameDbRefx.child(session.getUserID()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.
                    final Nota nota = dataSnapshot.getValue(Nota.class);


                    runnable = new Runnable() {
                        public void run() {
                            //do something
                        //    Toast.makeText(MainActivity.this, "Session_At"+session.getUpdated(), Toast.LENGTH_SHORT).show();
                        //    Toast.makeText(MainActivity.this, "Updated_At"+nota.getUpdated_at(), Toast.LENGTH_SHORT).show();

                              if (nota.getUpdated_at() != null && nota.getUpdated_at().equals(session.getUpdated())) {

                              } else {
                                  logoutByServer();

                              }


                        }
                    };

                    handler.postDelayed(runnable, delay);


                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    Log.d("FirebaseDB", "Failed to read value.", error.toException());
                }
            });
        }


        // You can be pretty confident that the intent will not be null here.
        Intent intent = getIntent();

        // Get the extras (if there are any)
        Bundle extra = intent.getExtras();

        if (extra != null) {
            if ("active".equals(session.getGameState())) return;
            if (extra.containsKey("usernome")) {
                final String usernome = intent.getStringExtra("usernome");
                String userchaller = intent.getStringExtra("userchaller");
                String betGame = intent.getStringExtra("betGame");
                String game_id = intent.getStringExtra("game_id");
                final String bet_id = intent.getStringExtra("bet_id");

                showpDialog();
                RestService service = RestClient.getClient(session).create(RestService.class);
                Call<User> call = service.userReplay("Bearer " + session.getUserToken(), userchaller, usernome, betGame, bet_id, game_id);
                call.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {

                        if (response.isSuccessful()) {
                            if (response.body() != null) {

                                if(response.body().getSuccessor().equals("success")) {

                                    // Toast.makeText(MainActivity.this, "Response: ", Toast.LENGTH_SHORT).show();
                                    Log.d( "Toaster", "Response: "+response.toString());

                                    // String jsonResponse = response.body().toString();
                                    // saveInfo(jsonResponse);
                                    session.setChallengeState(bet_id);
                                    Toast.makeText(MainActivity.this, "Replay Sent!", Toast.LENGTH_SHORT).show();
                                    Log.d("Tokeniser: ", response.body().getSuccessor());
                                    Log.d("onConfirm", "Success: " + response.body().getSuccessor());

                                    int delay = 60000; //milliseconds

                                    runnablec = new Runnable() {
                                        public void run() {
                                            if (session.getGameState() != null && "active".equals(session.getGameState())) {
                                                handler.removeCallbacks(runnablec);
                                                hidepDialog();
                                                session.setChallengeState(null);
                                            } else {
                                                //do something
                                                handler.removeCallbacks(runnablec);
                                                hidepDialog();
                                                session.setChallengeState(null);
                                            }

                                        }
                                    };

                                    handler.postDelayed(runnablec, delay);
                                } else {
                                    Log.d( "Toaster", "Response: "+response.body().getSuccessor());
                                    Toast.makeText(MainActivity.this, "Response: ", Toast.LENGTH_SHORT).show();
                                }


                            } else {
                                hidepDialog();
                                Toast.makeText(MainActivity.this, "Empty response", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            hidepDialog();
                            Toast.makeText(MainActivity.this, "Replay Not Sent!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, final Throwable t) {

                        // Get firebase database reference
                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference gameDbRef = database.getReference("startx");

                        if(session.getUserID() == null){


                        } else {

                            // Read from the database

                            startxChildEventListener = gameDbRef.child(usernome).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    // This method is called once with the initial value and again
                                    // whenever data at this location is updated.
                                    Nota nota = dataSnapshot.getValue(Nota.class);

                                    Activity activity = MainActivity.this;
                                    if(activity == null) return;

                                    if("".equals(nota.getBacker())) {

                                    } else {

                                        if (nota.getNota() != null) {
                                            String action = nota.getBet_id();

                                            if (action.equals(bet_id)) {
                                                showpDialog();

                                            } else {
                                                hidepDialog();
                                                Log.d("onFailure", t.toString());
                                                Toast.makeText(MainActivity.this, "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
                                            }


                                        }

                                    }


                                }

                                @Override
                                public void onCancelled(DatabaseError error) {
                                    // Failed to read value
                                    hidepDialog();
                                    Log.d("onFailure", t.toString());
                                    Toast.makeText(MainActivity.this, "Failure Communicating With Server", Toast.LENGTH_SHORT).show();

                                    Log.w("FirebaseDB", "Failed to read value.", error.toException());
                                }
                            });
                        }



                    }
                });



            } else if(extra.containsKey("resetter")){
                session.setGameState(null);

                session.setNotaRec(null);
                session.setNoteRec(null);
                session.setNotiRec(null);
                session.setNotoRec(null);
                session.setNotuRec(null);
                session.setNotyRec(null);
                session.setWinaRec(null);
                session.setLosaRec(null);
                session.setPlay("false");
                session.setReqNota(null);
                session.setUserBeta(null);
                session.setUserAcc(null);
                session.setBetId(null);

            }
        }



        if(session.getUserToken() != null){

            // Replace account fragment with animation
            fragmentManager = getSupportFragmentManager();
            fragmentManager
                    .beginTransaction().addToBackStack(null)
                    //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                    .replace(R.id.home_host, new MainFragment(),
                            Utils.Main_Fragment).commit();
        } else {
            // Replace login activity
            Intent intentLogin;
            intentLogin = new Intent(MainActivity.this, LoginActivity.class);
            intentLogin.putExtra("stopper","false");
            startActivity(intentLogin);
            MainActivity.this.finish();
        }


    }


    public void logoutByServer() {

        if(!isFinishing())
        {
            showpDialog();
        } else {

        }

        final String uuida = session.getUserID();

        RestService service = RestClient.getClient(session).create(RestService.class);
        retrofit2.Call<User> call = service.userLogout(session.getUserToken(),uuida);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(retrofit2.Call<User> call, Response<User> response) {
                hidepDialog();

                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Toast.makeText(MainActivity.this, "Logged out", Toast.LENGTH_SHORT).show();
                        Session session = new Session(MainActivity.this);

                        // logout fragment
                        session.setUserToken(null);
                        session.setUserID(null);
                        session.setUserJSON(null);

                        FirebaseAuth.getInstance().signOut();

                        // Replace login activity
                        Intent intentLogin;
                        intentLogin = new Intent(MainActivity.this, LoginActivity.class);
                        intentLogin.putExtra("stopper","false");
                        startActivity(intentLogin);
                        MainActivity.this.finish();

                        // Replace account fragment with animation
                        Intent intentLog;
                        intentLog = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(intentLog);
                        MainActivity.this.finish();
                    } else {
                        Toast.makeText(MainActivity.this,"Empty response",Toast.LENGTH_LONG).show();
                    }
                } else if (!response.isSuccessful()){
                    Toast.makeText(MainActivity.this, "Logout Error!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Unable to Logout", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });


    }





    // Function to print difference in
    // time start_date and end_date
    private long findDifference(@NonNull String start_date,String end_date) {

        long result = 0;
        // SimpleDateFormat converts the
        // string format to date object
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        // Try Block
        try {

            // parse method is used to parse
            // the text from a string to
            // produce the date
            Date d1 = sdf.parse(start_date);
            Date d2 = sdf.parse(end_date);

            // Calucalte time difference
            // in milliseconds
            long difference_In_Time
                    = d2.getTime() - d1.getTime();

            // Calucalte time difference in
            // seconds, minutes, hours, years,
            // and days
            long difference_In_Seconds
                    = (difference_In_Time
                    / 1000);
            // % 60;

            result = difference_In_Seconds;
        }

        // Catch the Exception
        catch (ParseException e) {
            e.printStackTrace();
        }

        return result;
    }


    @Override
    protected void onStart() {
        super.onStart();
        session.setBackState("non-active");

        lifer = "start";
    }


    @Override
    protected void onResume(){
        super.onResume();

        session.setBackState("non-active");
        LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(notaMsgReceiver,
                new IntentFilter("GameStartData"));

        RestService service = RestClient.getClient(session).create(RestService.class);
        Call<User> call = service.userSetOnline("Bearer " + session.getUserToken(), session.getUserID());
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {

                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });

        displayData = Preferences.getArrayPrefs("displayArrList",MainActivity.this);
        if(displayData.size() > 0){

            parameter[0] = displayData.get(0);
            parameter[1] = displayData.get(1);
            parameter[2] = displayData.get(2);
            parameter[3] = displayData.get(3);
            parameter[4] = displayData.get(4);
            parameter[5] = displayData.get(5);

            displayConfirm("Challenge Started.", "Your Bet Challenge has been started, Confirm Start?", parameter);

            displayData.clear();

            Preferences.setArrayPrefs("displayArrList",displayData,MainActivity.this);

        }


        lifer = "resume";

    }



    public static String userDeviceID(Session session) {
        String uniqueUUID;
        if (session.getDeviceID() != null) {
            uniqueUUID = session.getDeviceID();
        } else {
            uniqueUUID = UUID.randomUUID().toString();
            session.setDeviceID(uniqueUUID);
        }
        return uniqueUUID;
    }


    public synchronized static String id(Context context) {
        if (uniqueID != null) {
            SharedPreferences sharedPrefs = context.getSharedPreferences(
                    PREF_UNIQUE_ID, Context.MODE_PRIVATE);
            uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null);
            if (uniqueID == null) {
                uniqueID = UUID.randomUUID().toString();
                Log.d("onUserUUID","uuid: "+UUID.randomUUID().toString());
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString(PREF_UNIQUE_ID, uniqueID);
                editor.commit();
            }
        }
        return uniqueID;
    }


    @Override
    protected void onPause() {
        super.onPause();
        session.setBackState("active");
        lifer = "pause";

    }


    @Override
    protected void onStop() {
        super.onStop();

        session.setBackState("active");
        // Get firebase database reference
        if(session.getUserID() != null) {
            DatabaseReference userOnliner = FirebaseDatabase.getInstance().getReference("users/" + session.getUserID() + "/status");
            userOnliner.onDisconnect().setValue("offline");
        }
        // startService(new Intent(this, GameService.class));
        lifer = "stop";

    }


    @Override
    protected void onDestroy() {

        session.setBackState("active");
        handler.removeCallbacks(runnable);
        lifer = "destroy";
        super.onDestroy();
        //  stopService(new Intent(this, GameService.class));
        session.setBackgroundState(null);
        // session.setSplashState(null);

    }


    public void displayStart(String titler, String messager, final String[] parax){

        AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this,R.style.Theme_AppCompat_Light_Dialog);
        dialog.setCancelable(false);
        dialog.setTitle(titler);
        dialog.setMessage(messager);
        session.setPlay(null);
        session.setReqTitle(null);
        session.setReqStop(null);
        session.setReqNota(null);
        session.setUserBeta(null);
        session.setUserAcc(null);
        // session.setBetId(null);

        dialog.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                Log.d("ReceiverCancelled","Nota Clicked!"+parax[1]);

                RestService serviceStart = RestClient.getClient(session).create(RestService.class);
                Call<User> callStop = serviceStart.userStop("Bearer "+session.getUserToken(),parax[1],parax[1],parax[2],parax[3]);
                callStop.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {

                        //Toast.makeText()
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                String jsonResponse = response.body().getSuccessor();
                                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                                Log.d("onStop", "Success: "+response.body().getSuccessor());

                            } else {
                                Toast.makeText(MainActivity.this,"Empty response",Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "Unable to fetch data", Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<User> call, Throwable t) {

                        Log.d("onFailure", t.toString());
                        Toast.makeText(MainActivity.this, "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
                    }
                });
            }});
        dialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                Log.d("ReceiverAccept","Nota Clicked!"+parax[1]);

                RestService serviceStart = RestClient.getClient(session).create(RestService.class);
                Call<User> callStart = serviceStart.userStart("Bearer "+session.getUserToken(),parax[1],parax[2],parax[3]);
                callStart.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {

                        //Toast.makeText()
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                String jsonResponse = response.body().getSuccessor();
                                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                                Log.d("onStart", "Success: "+response.body().getSuccessor());


                            } else {
                                Toast.makeText(MainActivity.this,"Empty response",Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "Unable to fetch data", Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<User> call, Throwable t) {

                        Log.d("onFailure", t.toString());
                        Toast.makeText(MainActivity.this, "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        final AlertDialog alert = dialog.create();
        if(!isFinishing())
        {
            alert.show();
        } else {

        }

    }

    public boolean checkInternet(){

        try {
            URL url = new URL("http://www.google.com");
            URLConnection connection = url.openConnection();
            connection.connect();
            Toast.makeText(getApplicationContext(), "Internet is connected", Toast.LENGTH_SHORT).show();
            return true;
        } catch (MalformedURLException e) {
            Toast.makeText(getApplicationContext(), "Internet is not connected", Toast.LENGTH_SHORT).show();
            return false;
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), "Internet is not connected!", Toast.LENGTH_SHORT).show();
            return false;
        }

    }


    public void displayConfirm(String titler, String messager, final String[] parax){

        //  if(session.getChallengeState() != null && session.getChallengeState().equals("active")) return;

        session.setChallengeState("active");
        notabene = false;

        AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this, R.style.Theme_AppCompat_Light_Dialog);
        dialog.setCancelable(false);
        dialog.setTitle(titler);
        dialog.setMessage(messager + "\r\nOpponent: " + parax[2] + "\r\nBet Amount: ₦" + parax[4] + "\r\nBet Game: " + parax[5]);
        session.setPlay(null);
        session.setReqTitle(null);
        session.setReqStop(null);
        session.setReqNota(null);
        session.setUserBeta(null);
        session.setUserAcc(null);
        session.setBetId(parax[3]);

        dialog.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                Log.d("ReceiverCancelled", "Nota Clicked!" + parax[1]);

                RestService serviceStart = RestClient.getClient(session).create(RestService.class);
                Call<User> callStop = serviceStart.userStop("Bearer " + session.getUserToken(), parax[1], parax[1], parax[2], parax[3]);
                callStop.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {

                        //Toast.makeText()
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                session.setChallengeState(null);
                                session.setAppState("true");

                                session.setNotaRec(null);
                                session.setNoteRec(null);
                                session.setNotiRec(null);
                                session.setNotoRec(null);
                                session.setNotuRec(null);
                                session.setNotyRec(null);
                                session.setWinaRec(null);
                                session.setLosaRec(null);
                                session.setPlay("false");
                                session.setReqNota(null);
                                session.setUserBeta(null);
                                session.setUserAcc(null);
                                session.setBetId(null);

                                String jsonResponse = response.body().getSuccessor();
                                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                                ChallengerFragment challengerFrag = new ChallengerFragment();
                                if (challengerFrag.isAdded()) {
                                    challengerFrag.removeHandler();
                                }

                                Log.d("onStop", "Success: " + response.body().getSuccessor());


                            } else {
                                session.setChallengeState(null);
                                Toast.makeText(MainActivity.this, "Empty response", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            session.setChallengeState(null);
                            Toast.makeText(MainActivity.this, "Unable to fetch data", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        session.setChallengeState(null);
                        Log.d("onFailure", t.toString());
                        Toast.makeText(MainActivity.this, "Failure Communicating With Server", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        dialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                Log.d("ReceiverConfirm", "Nota Clicked!" + parax[2]);

                SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date now = new Date();
                String strDate = sdfDate.format(now);

                // Toast.makeText(MainActivity.this, "Time Check: " + findDifference(parax[0], strDate) + " first :" + parax[0] + " second: " + strDate, Toast.LENGTH_SHORT).show();
                if (findDifference(parax[0], strDate) >= 50) {
                    displayer("Game Challenge.", "The Game Challenge Invitation has Expired!");
                    session.setAppState(null);
                    session.setChallengeState(null);
                    session.setBetId(null);

                } else {
                    session.setChallengeState(null);
                    showpDialog();

                    RestService serviceConfirm = RestClient.getClient(session).create(RestService.class);
                    Call<User> call = serviceConfirm.userConfirm("Bearer " + session.getUserToken(), parax[1], parax[2], parax[3]);
                    call.enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {

                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    String jsonResponse = response.body().getSuccessor();

                                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                                    ChallengerFragment challengerFrag = new ChallengerFragment();
                                    if (challengerFrag.isAdded()) {
                                        challengerFrag.removeHandler();
                                    }

                                    Log.d("onConfirm", "Success: " + response.body().getSuccessor());

                                } else {
                                    session.setChallengeState(null);
                                    session.setAppState(null);
                                    session.setBetId(null);
                                    hidepDialog();
                                    Toast.makeText(MainActivity.this, "Empty response", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                session.setChallengeState(null);
                                session.setAppState(null);
                                session.setBetId(null);
                                hidepDialog();
                                Toast.makeText(MainActivity.this, "Unable to fetch data", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable t) {
                            session.setChallengeState(null);
                            session.setAppState(null);
                            session.setBetId(null);
                            hidepDialog();
                        }
                    });
                }


            }
        });
        final AlertDialog alert = dialog.create();
        if (!isFinishing()) {
            alert.show();
            playChallengeSound();
        } else {
            // Toast.makeText(this, "Display: Error here !!!!", Toast.LENGTH_SHORT).show();
        }



    }

    public void startGame(String[] parax){
        // session.setHomeState("active");
        session.setPlay(null);
        session.setReqTitle(null);
        session.setReqStop(null);
        session.setReqNota(null);
        session.setUserBeta(null);
        session.setUserAcc(null);
        // session.setBetId(null);

        if(!isFinishing())
        {
            showpDialog();
        } else {

        }

        RestService serviceOn = RestClient.getClient(session).create(RestService.class);
        Call<User> call = serviceOn.userOn("Bearer "+session.getUserToken(),session.getUserID(),parax[1],parax[2],parax[3]);
        call.enqueue(new Callback<User>() {

            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                session.setPlay(null);
                session.setReqTitle(null);
                session.setReqStop(null);
                session.setReqNota(null);
                session.setUserBeta(null);
                session.setUserAcc(null);
                // session.setBetId(null);

                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        String jsonResponse = response.body().getGamer().getGame_type();

                        Log.d("onBegin", "Success: "+response.body().getGamer().getGame_type());

                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                        ChallengerFragment challengerFrag = new ChallengerFragment();
                        if(challengerFrag.isAdded())
                        {
                            challengerFrag.removeHandler();
                        }


                    } else {
                        hidepDialog();
                        session.setChallengeState(null);
                        session.setAppState(null);
                        Toast.makeText(MainActivity.this,"Empty response",Toast.LENGTH_LONG).show();
                    }
                }  else {
                    hidepDialog();
                    session.setChallengeState(null);
                    session.setAppState(null);
                    Toast.makeText(MainActivity.this, "Load Game Error ", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                hidepDialog();
                session.setChallengeState(null);
                session.setAppState(null);
            }
        });
    }


    public void displayStop(String titler, String messager){

        session.setChallengeState(null);
        // session.setAppState(null);
        AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this, R.style.Theme_AppCompat_Light_Dialog);
        dialog.setCancelable(false);
        dialog.setTitle(titler);
        dialog.setMessage(messager);
        session.setPlay(null);
        session.setReqTitle(null);
        session.setReqStop(null);
        session.setReqNota(null);
        session.setUserBeta(null);
        session.setUserAcc(null);
        //  session.setBetId(null);



        dialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                Log.d("StopConfirm", "Yeta Clicked!");

                RestService service = RestClient.getClient(session).create(RestService.class);
                Call<User> call = service.userRemStop("Bearer " + session.getUserToken(), session.getUserID());
                call.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {

                        //Toast.makeText()
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                                ChallengerFragment challengerFrag = new ChallengerFragment();
                                if (challengerFrag.isAdded()) {
                                    challengerFrag.removeHandler();
                                }

                                // Replace account fragment with animation
                                fragmentManager = getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction
                                        .addToBackStack(null)
                                        //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                                        .replace(R.id.home_host, new ChallengerFragment(),
                                                Utils.Challenger_Fragment).commit();
                            }
                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        Toast.makeText(MainActivity.this, "Request Cancelled Failed!", Toast.LENGTH_SHORT).show();
                    }
                });


            }
        });

        final AlertDialog alert = dialog.create();
        if (!isFinishing()) {
            alert.show();
        } else {

        }


    }

    public void playChallengeSound(){
        MediaPlayer mediaPlayer;
        mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.challenge);
        mediaPlayer.start();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.release();
            }
        });
    }


    public boolean isGooglePlayServicesAvailable(Activity activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
        if(status != ConnectionResult.SUCCESS) {
            if(googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(activity, status, 2404).show();
            }
            return false;
        }
        return true;
    }


    @Override
    public void onBackPressed() {
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        if (backStackEntryCount == 1) {
            if(session.getUserToken() != null){

                // Replace account fragment with animation
                fragmentManager = getSupportFragmentManager();
                fragmentManager
                        .beginTransaction().addToBackStack(null)
                        //.setCustomAnimations(R.anim.right_enter, R.anim.left_out)
                        .replace(R.id.home_host, new MainFragment(),
                                Utils.Main_Fragment).commit();
            } else {
                // Replace login activity
                Intent intentLogin;
                intentLogin = new Intent(MainActivity.this, LoginActivity.class);
                intentLogin.putExtra("stopper","false");
                startActivity(intentLogin);
                MainActivity.this.finish();
            }
        } else {
            super.onBackPressed();
        }
    }




    /**
     * Handle onNewIntent() to inform the fragment manager that the
     * state is not saved.  If you are handling new intents and may be
     * making changes to the fragment state, you want to be sure to call
     * through to the super-class here first.  Otherwise, if your state
     * is saved but the activity is not stopped, you could get an
     * onNewIntent() call which happens before onResume() and trying to
     * perform fragment operations at that point will throw IllegalStateException
     * because the fragment manager thinks the state is still saved.
     *
     * @param intent
     */
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);


        if("clicky".equals(intent.getStringExtra("nota"))){
            //Intent intentx = getIntent();
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onClick(View view) {


    }

    private void createNotificationChannel() {
        String CHANNEL_ID = "BET_GAME_1";
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.default_notification_channel_id);
            String description = getString(R.string.notification_channel_info);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }



    public void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    public void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void displayer(String titler, String messager){
        AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this,R.style.Theme_AppCompat_Light_Dialog);
        dialog.setCancelable(false);
        dialog.setTitle(titler);
        dialog.setMessage(messager);
        dialog.setPositiveButton("Ok", null);

        final AlertDialog alert = dialog.create();
        alert.show();
    }

    public void setBackgroundNonActive(){
        // session.setBackgroundState("non-active");
    }

    public void setBackgroundActive(){
        //  session.setBackgroundState("active");
    }

    public void setSplashActive(){
        session.setBackgroundState("active");
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra("mode").equals("restart")){
                Intent intentx = getIntent();
                startActivity(intentx);
                finish();
            }

        }
    };

    private BroadcastReceiver notaMsgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra("nota").equals("displayConfirm")){

                notabene = true;

                //  session.setGameState(null);

                session.setNotaRec(null);
                session.setNoteRec(null);
                session.setNotiRec(null);
                session.setNotoRec(null);
                session.setNotuRec(null);
                session.setNotyRec(null);
                session.setWinaRec(null);
                session.setLosaRec(null);
                session.setPlay("false");
                session.setReqNota(null);
                session.setUserBeta(null);
                session.setUserAcc(null);
                session.setBetId(null);





                getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

                String user_beta;
                String user_acc;
                String bet_id;
                String bet_amt;

                user_beta = intent.getStringExtra("user_beta");
                user_acc = intent.getStringExtra("user_acc");
                bet_id = intent.getStringExtra("bet_id");
                bet_amt = intent.getStringExtra("bet_amt");

                parameter[0] = intent.getStringExtra("updated");
                parameter[1] = user_beta;
                parameter[2] = user_acc;
                parameter[3] = bet_id;
                parameter[4] = bet_amt;

                SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date now = new Date();
                String strDate = sdfDate.format(now);

                if (findDifference(parameter[0],strDate) >= 50) {
                    Toast.makeText(MainActivity.this, "Game Challenge Invitation is no more valid!", Toast.LENGTH_SHORT).show();
                } else {
                    //  displayConfirm("Challenge Started.", "Your Bet Challenge has been started, Confirm Start?", parameter);

                }

            }

        }
    };



}