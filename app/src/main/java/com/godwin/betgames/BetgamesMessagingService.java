package com.godwin.betgames;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.godwin.betgames.models.User;
import com.godwin.betgames.views.LoginActivity;

import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BetgamesMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    private LocalBroadcastManager broadcaster;
    Session session;
    Context context;
    String deviceid;

    @Override
    public void onCreate() {
        broadcaster = LocalBroadcastManager.getInstance(this);
        session = new Session(this);
        context = getApplicationContext();
        deviceid = session.getDeviceID();
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        Log.e("NEW_TOKEN",s);
        Intent intent = new Intent("Restart");
        intent.putExtra("mode","restart");
        broadcaster.sendBroadcast(intent);

    }

    private static int count = 0;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        //Displaying data in log
        //It is optional
        //Calling method to generate notification

       // String click_action = remoteMessage.getNotification().getClickAction();

        // showNotification(remoteMessage.getData());
        if(session.getBackState() != null && session.getBackState().equals("non-active")){

        } else if(session.getBackState() != null && session.getBackState().equals("non-active-x")) {

        } else {
           // useData(remoteMessage.getData());
          // showNotification(remoteMessage.getData());
        }

      //  showData(remoteMessage.getData());


    }

    //This method is only generating push notification
    private void showData(Map<String, String> row) {

        if("started".equals(row.get("backer"))){

            Intent intent = new Intent("GameStartData");
            intent.putExtra("stopper","false");
            intent.putExtra("resumer","resumer");
            intent.putExtra("nota","displayConfirm");
            intent.putExtra("userBeta",row.get("user_beta"));
            intent.putExtra("userAcc",row.get("user_acc"));
            intent.putExtra("betId",row.get("betId"));
            intent.putExtra("updated",row.get("updated_at"));

            broadcaster.sendBroadcast(intent);
        }


    }

    //This method is only generating push notification
    private void useData(Map<String, String> row) {

        if("started".equals(row.get("backer"))){ //messageTitle.equals("Bet Game Started") ||

            Intent intent = new Intent("GameStartData");
            intent.putExtra("stopper","false");
            intent.putExtra("nota","displayConfirm");
            intent.putExtra("userBeta",row.get("user_beta"));
            intent.putExtra("userAcc",row.get("user_acc"));
            intent.putExtra("betId",row.get("betId"));
            intent.putExtra("updated",row.get("updated_at"));
            /**
            Intent intentx = new Intent(this,NotaReceiver.class).setAction("GameStartDataRest");
            intentx.putExtra("stopper","false");
            intentx.putExtra("nota","displayConfirm");
            intentx.putExtra("userBeta",row.get("user_beta"));
            intentx.putExtra("userAcc",row.get("user_acc"));
            intentx.putExtra("betId",row.get("bet_id"));

            session.setPlay("true");
            session.setReqTitle("Bet Game Started");
            session.setReqStop("false");
            session.setReqNota("displayConfirm");
            session.setUserBeta(row.get("user_beta"));
            session.setUserAcc(row.get("user_acc"));
            session.setBetId(row.get("bet_id"));
            */
            broadcaster.sendBroadcast(intent);
        } else if("confirmed".equals(row.get("backer"))){ // messageTitle.equals("Bet Game Confirmed") ||

            Intent intent = new Intent("GameStartData");
            intent.putExtra("stopper","false");
            intent.putExtra("nota","startGame");
            intent.putExtra("userBeta",row.get("user_beta"));
            intent.putExtra("userAcc",row.get("user_acc"));
            intent.putExtra("betId",row.get("betId"));
            intent.putExtra("updated",row.get("updated_at"));
            /**
            Intent intentx = new Intent(this,NotaReceiver.class).setAction("GameStartDataRest");
            intentx.putExtra("stopper","false");
            intentx.putExtra("nota","startGame");
            intentx.putExtra("userBeta",row.get("user_beta"));
            intentx.putExtra("userAcc",row.get("user_acc"));
            intentx.putExtra("betId",row.get("bet_id"));

            session.setPlay("true");
            session.setReqTitle("Bet Game Confirmed");
            session.setReqStop("false");
            session.setReqNota("startGame");
            session.setUserBeta(row.get("user_beta"));
            session.setUserAcc(row.get("user_acc"));
            session.setBetId(row.get("bet_id"));
            */
            broadcaster.sendBroadcast(intent);

        } else if("cancelled".equals(row.get("backer"))){  //messageTitle.equals("Bet Challenge Accepted") ||

            Intent intent = new Intent("GameStartData");
            intent.putExtra("stopper","false");
            intent.putExtra("nota","displayStop");
            intent.putExtra("userBeta",row.get("user_beta"));
            intent.putExtra("userAcc",row.get("user_acc"));
            intent.putExtra("betId",row.get("betId"));
            intent.putExtra("updated",row.get("updated_at"));
            /**
            Intent intentx = new Intent(this,NotaReceiver.class).setAction("GameStartDataRest");
            intentx.putExtra("stopper","false");
            intentx.putExtra("nota","displayStop");
            intentx.putExtra("userBeta",row.get("user_beta"));
            intentx.putExtra("userAcc",row.get("user_acc"));
            intentx.putExtra("betId",row.get("bet_id"));

            session.setPlay("true");
            session.setReqTitle("Request Cancelled");
            session.setReqStop("false");
            session.setReqNota("displayStop");
            session.setUserBeta(row.get("user_beta"));
            session.setUserAcc(row.get("user_acc"));
            session.setBetId(row.get("bet_id"));
            */
            broadcaster.sendBroadcast(intent);
        } else if("initiate".equals(row.get("backer"))){ //messageTitle.equals("Bet Game Ongoing")
            if(Objects.equals(row.get("game_type"), "Finish&Count")){

                Intent intent = new Intent("GameStartData");
               // Intent intent = new Intent(this, FinishWhotActivity.class);
                intent.putExtra("stopper","false");
                intent.putExtra("id",row.get("id"));
                intent.putExtra("player1",row.get("player1"));
                intent.putExtra("player2",row.get("player2"));
                intent.putExtra("market",row.get("market_deck"));
                intent.putExtra("market_size",row.get("market_size"));
                intent.putExtra("one",row.get("player1_deck"));
                intent.putExtra("two",row.get("player2_deck"));
                intent.putExtra("play",row.get("play_deck"));
                intent.putExtra("turn",row.get("turn"));
                intent.putExtra("game_type",row.get("game_type"));
                intent.putExtra("nota","Finish&Count");
                intent.putExtra("updated",row.get("updated_at"));
                /**
                Intent intentx = new Intent(this,NotaReceiver.class).setAction("GameStartDataRest");
                intentx.putExtra("stopper","false");
                intentx.putExtra("id",row.get("id"));
                intentx.putExtra("player1",row.get("player1"));
                intentx.putExtra("player2",row.get("player2"));
                intentx.putExtra("market",row.get("market_deck"));
                intentx.putExtra("market_size",row.get("market_size"));
                intentx.putExtra("one",row.get("player1_deck"));
                intentx.putExtra("two",row.get("player2_deck"));
                intentx.putExtra("play",row.get("play_deck"));
                intentx.putExtra("turn",row.get("turn"));
                intentx.putExtra("game_type",row.get("game_type"));
                intentx.putExtra("nota","Finish&Count");
                */
                broadcaster.sendBroadcast(intent);
            } else if(Objects.equals(row.get("game_type"), "Classic")){

                Intent intent = new Intent("GameStartData");
               // Intent intent = new Intent(this, ClassicWhotActivity.class);
                intent.putExtra("stopper","false");
                intent.putExtra("id",row.get("id"));
                intent.putExtra("player1",row.get("player1"));
                intent.putExtra("player2",row.get("player2"));
                intent.putExtra("market",row.get("market_deck"));
                intent.putExtra("market_size",row.get("market_size"));
                intent.putExtra("one",row.get("player1_deck"));
                intent.putExtra("two",row.get("player2_deck"));
                intent.putExtra("play",row.get("play_deck"));
                intent.putExtra("turn",row.get("turn"));
                intent.putExtra("game_type",row.get("game_type"));
                intent.putExtra("nota","Classic");
                intent.putExtra("updated",row.get("updated_at"));
                /**
                Intent intentx = new Intent(this,NotaReceiver.class).setAction("GameStartDataRest");
                intentx.putExtra("stopper","false");
                intentx.putExtra("id",row.get("id"));
                intentx.putExtra("player1",row.get("player1"));
                intentx.putExtra("player2",row.get("player2"));
                intentx.putExtra("market",row.get("market_deck"));
                intentx.putExtra("market_size",row.get("market_size"));
                intentx.putExtra("one",row.get("player1_deck"));
                intentx.putExtra("two",row.get("player2_deck"));
                intentx.putExtra("play",row.get("play_deck"));
                intentx.putExtra("turn",row.get("turn"));
                intentx.putExtra("game_type",row.get("game_type"));
                intentx.putExtra("nota","Classic");
                */
                broadcaster.sendBroadcast(intent);
            }

        } else if("move".equals(row.get("backer"))){ //messageTitle.equals("Game Move")
        /**
            Intent intent = new Intent("GamePlayData");
            // You can also include some extra data.
            intent.putExtra("Nota", row.get("nota"));
            intent.putExtra("Note", row.get("note"));
            intent.putExtra("Noti", row.get("noti"));
            intent.putExtra("Noto", row.get("noto"));
            intent.putExtra("Notu", row.get("notu"));
            intent.putExtra("Noty", row.get("noty"));
            intent.putExtra("Wina", row.get("winner"));
            intent.putExtra("Losa", row.get("loser"));
            broadcaster.sendBroadcast(intent);
        */
        } else if("game-over".equals(row.get("backer"))){ //messageTitle.equals("Game Over")

            Intent intent = new Intent("GamePlayData");
            // You can also include some extra data.
            intent.putExtra("Wina", row.get("winner"));
            intent.putExtra("Losa", row.get("loser"));
            intent.putExtra("updated",row.get("updated_at"));
            broadcaster.sendBroadcast(intent);

        }


    }




    private void showNotification(Map<String, String> row) {

        Intent intent = new Intent(this,MainActivity.class);
        intent.putExtra("stopper","false");
        intent.putExtra("nota","clicky");

        String CHANNEL_ID = getString(R.string.default_notification_channel_id);
        String channelId = getString(R.string.default_notification_channel_id);
       // Intent fullScreenIntent = new Intent(this, MainActivity.class);
        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(context, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(context, CHANNEL_ID)
                        .setSmallIcon(R.drawable.backofcarder)
                        .setContentTitle("BetGames Challenge")
                        .setContentText("You have a Challenge Notification!")
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setAutoCancel(true)
                        // Use a full-screen intent only for the highest-priority alerts where you
                        // have an associated activity that you would like to launch after the user
                        // interacts with the notification. Also, if your app targets Android 10
                        // or higher, you need to request the USE_FULL_SCREEN_INTENT permission in
                        // order for the platform to invoke this notification.
                        .setFullScreenIntent(fullScreenPendingIntent, true);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "BetGamer_1",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        Notification incomingCallNotification = notificationBuilder.build();

        notificationManager.notify(1/* ID of notification */, notificationBuilder.build());

        //startForeground(1,incomingCallNotification);


    }



}